<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Usuarios</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Usuarios
            </h1>

        </section>

        <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- SELECT2 EXAMPLE -->
        <form id="formUsuario" action="usuario/registrarUsuario" method="post" autocomplete="off">
            <div class="card card-default">
                <div class="card-header">
                
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre*</label>
                                <input type="text" class="form-control" id="txtNombreUsuario" name="txtNombreUsuario" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Apellido paterno*</label>
                                <input type="text" class="form-control" id="txtApellidoPaterno" name="txtApellidoPaterno" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Apellido materno*</label>
                                <input type="text" class="form-control" id="txtApellidoMaterno" name="txtApellidoMaterno" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Login*</label>
                                <input type="text" class="form-control" id="txtLogin" name="txtLogin">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Contraseña*</label>
                                <input type="password" class="form-control" id="txtPass" name="txtPass">
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Perfil*</label>
                                <select class="form-control" id="cmbPerfil" name="cmbPerfil">
                                </select>
                            </div>
                        </div>
                        <!-- /.form-group -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Sucursal*</label>
                                <select class="form-control" id="cmbSucursal" name="cmbSucursal">
                                </select>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Puesto*</label>
                                <select class="form-control" id="cmbPuesto" name="cmbPuesto">
                                </select>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="txtcveUsuario" name="txtcveUsuario">
                    <input type="hidden" id="txtAccion" name="txtAccion" value="A">
                </div>
                <!-- /.row -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer">
                <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
                <button type="button" class="btn btn-primary" id="btnLimpiar">Limpiar</button>
                <button class="btn btn-primary" id="btnCancelar" onclick="cancelar();" style="visibility: hidden;">Cancelar</button>
            </div>

            
        </form>
      </div>
      <!-- /.container-fluid -->
    </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <div id="msgAlert"></div>

                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridUsuarios" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Nombre completo</th>
                            <th>Login &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Sucursal &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Puesto</th>
                            <th>Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>Estatus</th>
                            <th>Status</th>
                        </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->
<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">
    $(document).ready(function () {
        //Cargamos combo Perfil
        $.ajax({
            url      : 'perfil/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson.arrayDatos[0].cve_perfil);
                
                var select = $("#cmbPerfil");
                select.find('option').remove();
                
                $(myJson.arrayDatos).each( function(key, val){
                    select.append('<option value="' + val.cve_perfil + '">' + val.nombre_perfil + '</option>');
                });

                document.getElementById("cmbPerfil").selectedIndex = "0";
               
            }
        });

        //Cargamos combo sucursal
        $.ajax({
            url      : 'sucursal/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson.arrayDatos[0].cve_perfil);
                
                var select = $("#cmbSucursal");
                select.find('option').remove();

                $(myJson.arrayDatos).each( function(key, val){
                    select.append('<option value="' + val.cve_sucursal + '">' + val.nombre_sucursal + '</option>');
                });

                document.getElementById("cmbSucursal").selectedIndex = "0";
               
            }
        });

        //Cargamos combo puesto
        $.ajax({
            url      : 'puesto/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson.arrayDatos[0].cve_perfil);
                
                var select = $("#cmbPuesto");
                select.find('option').remove();

                
                $(myJson.arrayDatos).each( function(key, val){
                    select.append('<option value="' + val.cve_puesto + '">' + val.nombre_puesto + '</option>');
                });

                document.getElementById("cmbPuesto").selectedIndex = "0";
               
            }
        });

        tableUsuarios = $('#gridUsuarios').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                { "targets": 4, "width": "8%", "className": "text-center" },
                { "targets": 5, "width": "8%", "className": "text-center" },
                { "targets" : [6], "visible": false},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });

        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarTablaUsuario();


    });


    //Cargamos usuarios a la tabla
    function cargarTablaUsuario()
    {
        $.ajax({
            url      : 'usuario/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tableUsuarios.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {

                        if (parseInt(val.estatus_usuario) == 1)
                        {
                            title = 'Usuario activo';
                            icon = 'fa fa-circle';
                            color_icon = "color: #4ad129;"
                            accion = "bloquearUsuario('" + val.cve_usuario + "','0')";
                        }
                        else
                        {
                            title = 'Usuario bloqueado';
                            icon = 'fa fa-circle';
                            color_icon = "color: #f00;"
                            accion = "bloquearUsuario('" + val.cve_usuario + "','1')";
                        }

                        var btn_editar = "<i class='fa fa-edit' style='font-size:18px; cursor: pointer;' title='Editar usuario' onclick=\"mostrarUsuario('" + val.cve_usuario + "')\"></i>";
                        var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";
                    
                        tableUsuarios.row.add([
                            val.nombreCompleto,
                            val.login_usuario,
                            val.nombre_sucursal != null ? val.nombre_sucursal : "SUPER ADMINISTRADOR",
                            val.nombre_puesto,
                            btn_editar,
                            btn_status,
                            val.nombre_perfil
                        ]).draw();
                    })

                }
                else
                {
                    tableUsuarios = $('#gridUsuarios').DataTable();
                    
                }

            }
        });
    }

    $( "#cmbPerfil" ).change(function() {
        if (this.value == 1)
        {
            $("#cmbSucursal").prop('disabled', true);
            document.getElementById("cmbSucursal").selectedIndex = "0";
            document.getElementById("cmbPuesto").selectedIndex = "1";
        }
        else
        {
            $("#cmbSucursal").prop('disabled', false);
            document.getElementById("cmbPuesto").selectedIndex = "0";
        }
    });


    $('#formUsuario').on('submit',function(e){
        e.preventDefault();

        if ( $('#txtNombreUsuario').val()  == "" )
        {
            msgAlert("Favor de ingresar el nombre del usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#txtApellidoPaterno').val() == "" )
        {
            msgAlert("Favor de ingresar el apellido paterno del usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#txtApellidoMaterno').val() == "" )
        {
            msgAlert("Favor de ingresar el apellido materno del usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#txtLogin').val() == "" )
        {
            msgAlert("Favor de ingresar el login del usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#txtAccion').val() == "A" &&  $('#txtPass').val() == "" )
        {
            msgAlert("Favor de ingresar la contraseña de usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#cmbPerfil').val() == -1 )
        {
            msgAlert("Favor de seleccionar un perfil de usuario.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#cmbSucursal').val() == -1 && $('#cmbPerfil').val() != 1)
        {
            msgAlert("Favor de seleccionar una sucursal.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else if ( $('#cmbPuesto').val() == -1 )
        {
            msgAlert("Favor de seleccionar un puesto.","warning");
            setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);
        }
        else
        {
            //Añadimos el action al form para hacerlo dinámico
            //$("#formUsuario").attr('action', 'usuario/registrarUsuario');
            $("#btnGuardar").prop('disabled', true);

            $.ajax({
                url      : $(this).attr('action'),
                data     : $(this).serialize(),
                type: "POST",
                success: function(datos){

                    var myJson = JSON.parse(datos);
                    
                    if(myJson.status == "success")
                    {
                        //Reinicializamos tabla
                        cargarTablaUsuario();

                        msgAlert(myJson.msg ,"success");
                        setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                        //Limpiamos
                        $('#txtNombreUsuario').val('');
                        $('#txtApellidoPaterno').val('');
                        $('#txtApellidoMaterno').val('');
                        $('#txtLogin').val('');
                        $('#txtPass').val('');
                        document.getElementById("cmbPerfil").selectedIndex = "0";
                        document.getElementById("cmbSucursal").selectedIndex = "0";
                        document.getElementById("cmbPuesto").selectedIndex = "0";
                        $('#txtcveUsuario').val('');

                        $("#cmbSucursal").prop('disabled', false);
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');
                        $("#btnCancelar").css("visibility", "hidden");

                        $('#txtNombreUsuario').focus();

                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert(myJson.msg ,"danger");
                    }
                }
            }); 

        }

    });

    function mostrarUsuario(cve_usuario)
    {
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'usuario/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_usuario: cve_usuario 

            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson.arrayDatos);

                $('#txtNombreUsuario').val(myJson.arrayDatos[0].nombre_usuario);
                $('#txtApellidoPaterno').val(myJson.arrayDatos[0].apellidop_usuario);
                $('#txtApellidoMaterno').val(myJson.arrayDatos[0].apellidom_usuario);
                $('#txtLogin').val(myJson.arrayDatos[0].login_usuario);
                //document.getElementById("cmbPerfil").selectedIndex = myJson.arrayDatos[0].cveperfil_usuario;
                //document.getElementById("cmbSucursal").selectedIndex = myJson.arrayDatos[0].cvesucursal_usuario;
                //document.getElementById("cmbPuesto").selectedIndex = myJson.arrayDatos[0].cvepuesto_usuario;
                $('#cmbSucursal').val(myJson.arrayDatos[0].cvesucursal_usuario);
                $('#cmbPerfil').val(myJson.arrayDatos[0].cveperfil_usuario);
                $('#cmbPuesto').val(myJson.arrayDatos[0].cvepuesto_usuario);
                $('#txtcveUsuario').val(myJson.arrayDatos[0].cve_usuario);

                if ($('#cmbPerfil').val() == 1) $("#cmbSucursal").prop('disabled', true);
                else $("#cmbSucursal").prop('disabled', false);
                

                $('#txtAccion').val('M');
                $("#btnGuardar").html('Actualizar usuario');
                $("#btnCancelar").css("visibility", "visible");

            }
        });
    }

    function bloquearUsuario(cve_usuario,bloqueo)
    {

        if (bloqueo == 0)
        {
            var msg = "Esta seguro de bloquear este Usuario?";
            var ban = 2;
        }else{
            var msg = "Esta seguro de desbloquear este Usuario?";
            var ban = 3;
        }

        bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Si'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result == true){

                    $.ajax({
                        url      : 'usuario/bloquearUsuario',
                        type     : "POST",
                        data     : { 

                                ban: ban, 
                                cve_usuario: cve_usuario 

                        },
                        beforeSend: function() {
                            // setting a timeout

                        },
                        success  : function(datos) {

                            var myJson = JSON.parse(datos);
                    
                            if(myJson.status == "success")
                            {

                                //Reinicializamos tabla
                                cargarTablaUsuario();

                                msgAlert(myJson.msg ,"info");
                                setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                            }

                        }
                    });

                }else{
                    //No se hace nada...
                }
            }
        });

    }


    $('#btnLimpiar').click(function (e) {

        $('#txtNombreUsuario').val('');
        $('#txtApellidoPaterno').val('');
        $('#txtApellidoMaterno').val('');
        $('#txtLogin').val('');
        $('#txtPass').val('');
        document.getElementById("cmbPerfil").selectedIndex = "0";
        document.getElementById("cmbSucursal").selectedIndex = "0";
        document.getElementById("cmbPuesto").selectedIndex = "0";
        $('#txtcveUsuario').val('');

        $("#cmbSucursal").prop('disabled', false);
        $("#btnGuardar").prop('disabled', false);
        $("#btnGuardar").html('Guardar');
        $("#btnCancelar").css("visibility", "hidden");

        $('#txtNombreUsuario').focus();
    });



    function cancelar()
    {
        location.reload();
    }


    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>

</body>
</html>
