<?php
//print_r($datos);
?>
<div id="msgAlert2"></div>

<form id="formProducto" action="producto/guardarProducto" method="post">

    <div class="box-body">
        <div class="row">

            <div class="form-group col-md-12">
                <label>Nombre producto*</label>
                <input type="text" class="form-control" id="nombre_producto" name="nombre_producto" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

        </div>

        <div class="row">

            <div class="form-group col-md-12">
                <label>Precio mayoreo</label>
                <input type="number" step="0.01" class="form-control" id="preciomayoreo_producto" name="preciomayoreo_producto" onKeyPress="return soloNumeros(event);">
            </div>

        </div>

        <div class="row">
            
        <div class="form-group col-md-12">
                <label>Precio menudeo</label>
                <input type="number" step="0.01" min="0" class="form-control" id="preciomenudeo_producto" name="preciomenudeo_producto" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

           

        </div>

        <div class="row">
            
           

        </div>

    </div>

    

    <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
    </div>

    <input type="hidden" id="cve_producto" name="cve_producto">
</form>

<script type="text/javascript">


    $('#formProducto').on('submit',function(e){
        e.preventDefault();

        if ( $('#nombre_producto').val()  == "" )
        {
            msgAlert2("Favor de ingresar el nombre del producto.","warning");
         setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#preciomenudeo_producto').val() == "" )
        {
            msgAlert2("Favor de ingresar el Precio Menudeo.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#preciomayoreo_producto').val() == "" )
        {
            msgAlert2("Favor de ingresar el Precio Mayoreo.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else
        {

            $("#btnGuardar").prop('disabled', true);
            
            $.ajax({
                url      : $(this).attr('action'),
                data     : $(this).serialize(),
                type: "POST",
                success: function(datos){

                    var myJson = JSON.parse(datos);
                    
                    if(myJson.status == "success")
                    {
                        $('#modal_formProducto').modal('hide');

                        //var table = $('#gridProducto').DataTable();
                                    
                        //table.clear();
                        //table.destroy();
                        
                        //Reinicializamos tabla
                        cargarTablaProducto();

                        msgAlert(myJson.msg ,"success");
                        setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                        //$('#msgAlert').css("display", "none");
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');

                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                    }
                }
            }); 
        }
    });

    function msgAlert2(msg,tipo)
    {
        $('#msgAlert2').css("display", "block");
        $("#msgAlert2").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>