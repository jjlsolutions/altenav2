<?php
  if($_SESSION['cveperfil_usuario'] != 1){
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}
require_once ('../public/librerias/tcpdf/examples/lang/eng.php');
require_once ('../public/librerias/tcpdf/tcpdf.php');

include_once ("../Model/ReportesModelo.class.php");
class pdfBP01 extends TCPDF {


            
    public function Header() {
        // Primer cuadro del encabezado
        
        
    }

     public function Footer() {
          //Franja superior gris oscuro
         

      
    }
 
    public function informacion()
    {
      $total_cantidad_venta_global = 0;
      $total_global = 0;
      for($mes_numero=1;$mes_numero<=12;$mes_numero++){
        $model = new ReportesModelo();   
        $response = $model->consultarVentasMesMostrador($mes_numero);

        $tam =count($response);
        $total_cantidad_venta = 0;
        $total = 0;
        if ($tam!=0) {
          $this->cell(185,7,"REPORTE MENSUAL MOSTRADOR",1,1,"C",$this->SetFillColor(255, 255, 255),"",0,true,"T","C", $this->setFont('arial','B',10));
          
          $this->Multicell(40,7,"MES",1,'C', 1,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
          $this->Multicell(65,7,"NOMBRE DEL PRODUCTO",1,'C', 1,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
          $this->Multicell(40,7,"CANTIDAD TOTAL",1,'C', 1,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
          $this->setcellmargins(0,0,0,0);
          $this->Multicell(40,7,"TOTAL DE VENTA",1,'C', 1,1,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
        }

        for($j=0;$j<=$tam;$j++){

          if ($tam==0) {
          }

          else if ($j<$tam){
            $this->SetCellPaddings(2,2,2,2);
            $this->Multicell(40,7,$response[$j]['mes'],1,'L', 0,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','',8));
            $this->Multicell(65,7,$response[$j]['nombre_producto'],1,'L', 0,0,'', '',1,0,0,0,7,'M');
            $this->Multicell(40,7,$response[$j]['cantidad_venta'],1,'C', 0,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','',8));
            $this->Multicell(40,7,$response[$j]['total'],1,'C', 0,1,'', '',1,0,0,0,7,'M');

              $total_cantidad_venta = $total_cantidad_venta + $response[$j]['cantidad_venta'];
              $total = $total + $response[$j]['total'];

              //Bucle para poner filas en blanco  cuando son menos de 7 registros
            
              if($this->GetY() > 245){
                $this->addPage();
              }
          }
        }
        if ($tam!=0) {
          $this->SetCellMargins(0,0,0,0);
          $this->Multicell(105,7,"Total",1,'C', 1,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
          $this->Multicell(40,7,"$ ".$total_cantidad_venta,1,'C', 0,0,'', '',1,0,0,0,7,'M',$this->setFont('arial','B',10));
          $this->Multicell(40,7,"$ ".$total ,1,'C', 1,1,'', '',1,0,0,0,7,'M',$this->setFont('arial','B',10));
          $this->Ln(5);
          $this->addPage();
        }
        $total_cantidad_venta_global = $total_cantidad_venta_global + $total_cantidad_venta;
        $total_global = $total_global +$total;
      }

      $this->SetCellMargins(0,0,0,0);
      $this->cell(185,7,"TOTAL VENTA GLOBAL MOSTRADOR",1,1,"C",$this->SetFillColor(255, 255, 255),"",0,true,"T","C", $this->setFont('arial','B',10));
      $this->Multicell(105,7,"Total global",1,'C', 1,0,'', '',1,0,0,0,7,'M', $this->setFont('arial','B',8));
      $this->Multicell(40,7,"$ ".$total_cantidad_venta_global,1,'C', 0,0,'', '',1,0,0,0,7,'M',$this->setFont('arial','B',10));
      $this->Multicell(40,7,"$ ".$total_global ,1,'C', 1,1,'', '',1,0,0,0,7,'M',$this->setFont('arial','B',10));
      $this->Ln(5);
    }
  
}
$medidas=array(279,216);
$pdf = new pdfBP01('P','mm',$medidas,true,'UTF-8',false);

  $pdf->SetTitle('ALTEÑA');
  $pdf->SetMargins(15,15, 15);
  $pdf->SetAutoPageBreak(true, 10);
  $pdf->setHeaderMargin(15);
  $pdf->setFooterMargin(35);

  $pdf->addPage();
  $pdf->informacion();
// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('REPORTE_MENSUAL_MOSTRADOR'.date("m_d_y").date("H_i_s").'.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+

?>