<?php 
	$permisos_array = array();
  foreach ($_SESSION["permiso_perfil"] as $permiso)
  {
    foreach ($permiso as $valor2)
    {
      $permisos_array[] = $valor2['cve_permiso'];
    }
  }

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo NOMBRE_SITIO; ?> | Domicilio</title>
    <?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
    <style>
      .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
          }
          .custom-control-label::before {
            position: absolute;
            top: 0.34rem;
            left: -1.5rem;
            display: block;
            width: 1.4rem;
            height: 1.4rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
          }
          .custom-control-label::after {
            position: absolute;
            top: 0.50rem;
            left: -1.3rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: 100% / 100% 100% no-repeat;
          }
          .custom-checkbox .custom-control-label::before{
            border-radius: 1.0rem;
          }
        .panel-body .btn:not(.btn-block) { width:45%;}
        .btn-app {
          border-radius: 40px;
          border: none;
          margin: .5rem;
          box-shadow: 0 3px 5px rgba(0,0,0,.4);
        }
    </style>
</head>
<body id="idScrool" class="hold-transition sidebar-mini">
  <div class="wrapper">
    
      <?php 
      include RUTA_APP . 'vistas/includes/header.php';

      include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
      ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section  class="content-header">
        <div class="container-fluid">
          <div class="row">
            <div class="form-group col-md-12">
                <div id="msgAlert"></div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-12">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="home">Home</a></li>
                <li class="breadcrumb-item active">Domicilio</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <section class="content"> 
      <div id="pedidoAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" data-toggle="collapse" style="color: black;" href="#divPedido" aria-expanded="false">
              <div id="divPedidoTexto">Inventario</div>
              </a>
            </h4>
          </div>
          <div id="divPedido" class="collapse show" data-parent="#pedidoAcordeon" style="">
          <div class="card-body">
              <hr>
              <form id="formUsuario" action="venta/guardarProducto" method="post">
                <div class="panel-body" id="botones">
                    
                </div>
              </form>
            </div>
            <div class="card-body" id="divgridHistorial">
                <table id="gridHistorial" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Comentario</th>
                            <th>Sucursal</th>
                            <th>Fecha</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
      </div>
      </section>
      
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
    <div class="clearfix">
        <div class="progress">
          <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-2">
              <a id="back-to-top" href="#" class="btn btn-outline-primary back-to-top" role="button" aria-label="Scroll to top">
              <i class="fas fa-chevron-up"></i>
              </a>
          </div>    
      </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <div class="modal fade" id="modal_formCantidad">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="nombre_producto"></h4>
          
        </div>
        <div class="modal-body">
            <input type="hidden" id="cve_producto" name="cve_producto">
            <input type="hidden" id="stock" name="stock">
            <input type="hidden" id="preciomayoreo_producto" name="preciomayoreo_producto">
            <input type="hidden" id="preciomenudeo_producto" name="preciomenudeo_producto">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Cantidad*</label>
                    <input type="number" class="form-control form-control-lg" style="border: 0;box-shadow: none;font-size: 38pt;text-align: center;" id="cantidad_producto" name="cantidad_producto">
                </div>
            </div>
            
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-primary" onclick="pasarSpan()" style="width:40%;">OK</button>
          <button type="button" class="btn btn-warning" onclick="limpiarCantidad()" style="width:40%;">LIMPIAR</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal_formCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formSucursal"> 

            </div>
        </div>
    </div>
</div>

  <!-- ./wrapper -->
      <?php 
      include RUTA_APP . 'vistas/includes/script.php';
      ?>
<script type="text/javascript">

  $(document).ready(function () {
  
    cargarBotonesProducto();

    $('#cantidad_sabor').keypress(function(e) {
      if (e.which == 13) {
        pasarSpan();
      } 
    });

    var bar = $('.progress-bar');
		bar.width('0%');

    <?php if(in_array(17, $permisos_array)){ ?>
      cargarTablaHistorialStock();
      $("#divgridHistorial").show();
    <?php }else{ ?>
        $("#divgridHistorial").hide();
    <?php } ?>
    
  });

  function cargarTablaHistorialStock()
    {
      $.ajax({
          url      : 'producto/consultarHistorialStock',
          type     : "POST",
          data    : { ban: 1 },
          beforeSend: function() {
              // setting a timeout

          },
          success  : function(datos) {

            var myJson = JSON.parse(datos);

            var table = $('#gridHistorial').DataTable();
                                    
            table.clear();
            table.destroy();

            $('#gridHistorial').DataTable({
                data: myJson.arrayDatos,
                order: [[1, 'desc']],
                columns: [
                    { data: 'comentario' },
                    { data: 'nombre_sucursal' },
                    { data: 'fechaadd_historialstock' }
                ]
            });
          }
      });
  }
  
  function cargarBotonesProducto(){
    $.ajax({
        url      : 'Producto/consultar',
        type     : "POST",
        data    : { 
            ban: 3
        },
        success  : function(datos) {

            var myJson = JSON.parse(datos);

            botones = $("#botones");
            $("#botones").empty();

            if(myJson.arrayDatos.length > 0)
            {
                $(myJson.arrayDatos).each( function(key, val)
                {
                  if(cveperfil == 1){
                    nombresucursal = '';
                    explode = val.nombre_sucursal.split(" ");
                    for(i=0;i<explode.length;i++){
                      inicial = explode[i];
                        nombresucursal +=  inicial.substr(0,1);
                    }
                    nombre_producto = val.nombre_producto + " (" + nombresucursal+")";
                  }else{
                    nombre_producto = val.nombre_producto;
                  }
                  <?php if((in_array(12, $permisos_array))){ ?>
                      botones.append('<a class="btn btn-app bg-success" id="producto_' + val.cve_producto + '" onClick="cantidad(' + val.cve_producto + ',\'' + val.nombre_producto + '\',' + val.preciomayoreo_producto + ',' + val.preciomenudeo_producto+ ',' + val.stock + ')"><b>' + nombre_producto + '</b></a>');
                      $("#producto_"+val.cve_producto).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+val.cve_producto+'"><b>'+val.stock+'</b></span>');
                    <?php }else{ ?>
                      botones.append('<a class="btn btn-app bg-success" id="producto_' + val.cve_producto + '"><b>' + nombre_producto + '</b></a>');
                      $("#producto_"+val.cve_producto).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+val.cve_producto+'"><b>'+val.stock+'</b></span>');
                    <?php } ?>
                });
            }
        }
    });
}


function cantidad(cve_producto, nombre_producto, preciomayoreo_producto, preciomenudeo_producto,stock){
    $('#cve_producto').val(cve_producto);
    $('#preciomenudeo_producto').val(preciomenudeo_producto);
    $('#preciomayoreo_producto').val(preciomayoreo_producto);
    $('#cantidad_producto').val("");
    $('#nombre_producto').text(nombre_producto);
    $('#stock').val(stock);
    $('#modal_formCantidad').modal({
        keyboard: false,
        backdrop: 'static'
    });
    
    $('#modal_formCantidad').on('shown.bs.modal', function () {
      $('#cantidad_producto').focus();
    });
}

function pasarSpan(){
    var total = 0;
    cve_producto = $('#cve_producto').val();
    stock = $('#stock').val();
    cantidad_producto = $("#cantidad_producto").val();
    spanCantidad = $('#spanCantidad_'+cve_producto).text();

    if(spanCantidad == ''){
      spanCantidad = 0;
    }
      if(cantidad_producto == ''){
        cantidad_producto = 0;
      }
      total = 0;
      total = parseInt(cantidad_producto) + parseInt(stock);
      if(cantidad_producto != 0){
        $.ajax({
          url      : 'producto/actualizarStockProducto',
          type     : "POST",
          data    : { 
            cve_producto: cve_producto,
            cantidad_producto : cantidad_producto 
          },
          success  : function(datos) {
            var myJson = JSON.parse(datos);
            var stock = myJson["stock"];
            $("#producto_"+cve_producto).find("span").remove();//remove span elements    
            //$("#producto_"+cve_producto).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+cve_producto+'"><b>'+cantidad_producto+'</b></span>');
            $("#producto_"+cve_producto).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+cve_producto+'"><b>'+stock+'</b></span>');
            cargarTablaHistorialStock();
            $("#cantidad_producto").val("");
            $('#modal_formCantidad').modal('hide');
          }
        });
      }else{
        $("#cantidad_producto").val("");
        $('#modal_formCantidad').modal('hide');
      } 
}


function limpiarVendedor(){
    $("#CMBVENDEDOR").val("");
    $("#nombreCompleto").text("");
    $("#nombresucursal").text("");
    $("#loginusuario").text("");
    $("#nombreperfil").text("");
    $('#divTarjetaVendedor').hide();

    nombreVendedorTexto = $("#divVendedorTexto").text();

          divVendedorTexto = $("#divVendedorTexto");
          $("#divVendedorTexto").empty();
          divVendedorTexto.append(nombreVendedorTexto);
  }

  function guardar(){
    var totalGlobal = 0;
    var cantidadGlobal = 0;
    var arrayProductosIndividual = new Array();
    var arrayProductosTodos = [];

    CantidadGlobal = $("#CantidadGlobal").text();
    precio = 1;//menudeo
    if($("#CheckboxMayoreo").prop("checked")){
      precio = 2;//mayoreo
    }
    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);

        TotalProducto = $("#spanTotal_"+myArr[1]).text();
        myArr2 = TotalProducto.split("$");
        totalGlobal = parseFloat(totalGlobal) + parseFloat(myArr2[1]); 
        
        precio_venta =  $("#spanMenudeo_"+myArr[1]).text();
        if(precio == 2){
          precio_venta = $("#spanMayoreo_"+myArr[1]).text();
        }
        var producto = {cvesp_venta:myArr[1], cantidad_venta:parseInt(cantidadProducto), precio_venta:parseFloat(precio_venta)}
        arrayProductosTodos.push(producto);
      }
    });

    var val = $('#CMBCLIENTES').val() ? $('#CMBCLIENTES').val() : '';
    
    var valueCombo = $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") : "";

    var cvecliente = valueCombo.cvecliente ? valueCombo.cvecliente : '';
  
    if (cvecliente == '')
    {
      msgAlert("Favor de ingresar un cliente.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if(cantidadGlobal  == 0){
      msgAlert("Favor de ingresar al menos un producto.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if($('#observaciones_venta').val()  == ''){
      msgAlert("Favor de escribir una observación.","warning");
      scrollHastaArriba(100);
          return;
    }
    else{
      tipoprecio_venta = 1;//menudeo
      if($("#CheckboxMayoreo").prop("checked")){
        tipoprecio_venta = 2;//mayoreo
      }
      precioespecial_venta = '0';//precio especial no
      if($("#CheckboxPrecioEspecial").prop("checked")){
        precioespecial_venta = ((parseFloat($("#txtAuxTotalTotalGlobal").val()) * 15)/100);//precio especial si
      }
      cobroenvio_venta = '0';
      if(totalGlobal > 0){
        if($("#CheckboxEnvio").prop("checked")){
          cobroenvio_venta = 20;
        }
      }

      var listaProductos = JSON.stringify(arrayProductosTodos);
      $("#btnCobrar").attr("disabled", true);
      $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          var bar = $('.progress-bar');

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              percentComplete = parseInt(percentComplete * 100);
              //console.log(percentComplete);

              var percentVal = percentComplete+'%';
              bar.width(percentVal);

            }
          }, false);
          return xhr;
        },
        url      : 'venta/guardarVenta',
        type     : "POST",
        datatype: 'JSON',
        data    : { 
          listaProductos: listaProductos,
          total_venta: totalGlobal, 
          tipo_venta: 2,
          tipoprecio_venta: tipoprecio_venta,
          cvecliente_venta: cvecliente,
          cobroenvio_venta : cobroenvio_venta,
          observaciones : $('#observaciones_venta').val(),
          precioespecial_venta : precioespecial_venta
        },
        success  : function(datos) {
          var myJson = JSON.parse(datos);
          var cve_venta = myJson["cve_venta"];
          
          $.ajax({
            url      : 'venta/obtenerFolio',
            type     : "POST",
            data    : { 
              ban: 1,
              cve_venta: cve_venta 
            },
            success  : function(datos) {
              var myJson = JSON.parse(datos);

              $(myJson.arrayDatos).each( function(key, val){
                $("#CantidadGlobal").empty();
                $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
                $("#CantidadGlobal").append(" 0 ");

                $("#TotalGlobal").empty();
                $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
                $("#TotalGlobal").append(" $0 ");

                $("#divClienteTexto").empty();
                $("#divClienteTexto").append("Cliente");

                $("#divPedidoTexto").empty();
                $("#divPedidoTexto").append("Pedido");

                $("#divObservacionesTexto").empty();
                $("#divObservacionesTexto").append("Observaciones");

                $("#observaciones_venta").val("");

                $("#txtTotalTotalGlobal").empty();
                $("#txtTotalTotalGlobal").append("0");
                cargarBotonesProducto();
                $('#folio_venta').text(val.folio_venta);
                labeltotalGlobal = val.total_venta;
                if(val.cobroenvio_venta > 0){
                  labeltotalGlobal = parseFloat(val.total_venta) + parseFloat(val.cobroenvio_venta);
                  $('#cobroenvio_venta').text("Si");
                }else{
                  $('#cobroenvio_venta').text("No");
                }
                if(val.precioespecial_venta > 0){
                  labeltotalGlobal = labeltotalGlobal - parseFloat(val.precioespecial_venta);
                  $('#precioespecial_venta').text("Si");
                }else{
                  $('#precioespecial_venta').text("No");
                }
                $('#total_venta').text(labeltotalGlobal);
                if(val.tipoprecio_venta == 1){
                  $('#tipoprecio_venta').text("Menudeo");
                }
                if(val.tipoprecio_venta == 2){
                  $('#tipoprecio_venta').text("Mayoreo");
                }
                $('#nombrecliente_venta').text(val.nombre_cliente);
                

                
                $('#fechaadd_venta').text(val.fechaadd_venta);
                $("#CMBCLIENTES").val("");
                $("#nombrecliente").text("");
                $("#direccioncliente").text("");
                $("#celularcliente").text("");
                $("#comentariocliente").text("");
                $('#divTarjetaCliente').hide();
                cargarBotonesProducto();
                var bar = $('.progress-bar');
					      bar.width('0%');
                $("#btnCobrar").attr("disabled", false);
                $('#modalInfoVenta').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
              });
            }
          });
        }
      });

    }
}

function limpiarCantidad(){
  $("#cantidad_producto").val("");
  $('#txtTotalGlobal').text("Total : $0");
  $('#cantidad_producto').focus();
}

function msgAlert(msg,tipo)
{
    $('#msgAlert').css("display", "block");
    $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
}
</script>
</body>
</html>
