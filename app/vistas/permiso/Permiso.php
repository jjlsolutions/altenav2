<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo NOMBRE_SITIO; ?> | Permisos</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Permisos
            </h1>

        </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <!-- /.card-header -->
              <div class="card-body">
                <table id="gridPerfiles" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                        <th>Nombre&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Descripcion&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                        <th>Editar&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- modales -->
<div class="modal fade" id="modal_formPerfiles" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Permisos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formPerfil"> 

            </div>
        </div>
    </div>
</div>

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">

    $(document).ready(function () {

        tablePerfil = $('#gridPerfiles').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "20%","className": "text-center","targets": 0},
                {"width": "70%","className": "text-center","targets": 1},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });

        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarTablaPerfil();

    });


    function cargarTablaPerfil()
    {
        $.ajax({
            url      : 'permiso/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tablePerfil.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {

                       
                        if(val.estatus_perfil == 1){
                            var btn_editar = "<i class='fa fa-edit' style='font-size:18px; cursor: pointer;' title='Editar Perfil' onclick=\"mostrarPerfil('" + val.cve_perfil + "')\"></i>";
                        
                            tablePerfil.row.add([
                                val.nombre_perfil,
                                val.descripcion_perfil,
                                btn_editar
                            ]).draw();
                        }
                        
                    })

                }
                else
                {
                    tablePerfil = $('#gridPerfiles').DataTable();
                    
                }

            }
        });
    }

    function mostrarPerfil(cve_perfil)
    {
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'permiso/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_perfil: cve_perfil 

            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson);

                $('#modal_formPerfiles').modal({
                    backdrop: 'static',
                    keyboard: false
                });

                $("#muestra_formPerfil").html('Cargando...');

                $.ajax({
                    url: 'permiso/formPermiso',
                    type     : "POST",
                    data     : { 

                            cve_perfil: cve_perfil 

                    },
                    success: function(datos){

                        $("#muestra_formPerfil").html(datos);

                        $('#txtNombrePerfil').val(myJson.arrayDatos[0].nombre_perfil);
                        $('#txtDescipcionPerfil').val(myJson.arrayDatos[0].descripcion_perfil);
                        $('#txtcvePerfil').val(myJson.arrayDatos[0].cve_perfil);

                        $("#btnGuardar").html('Actualizar permisos');
                    }
                });

                

                //$('#txtAccion').val('M');
                
                //$("#btnCancelar").css("visibility", "visible");

            }
        });
    }

    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>

</body>
</html>
