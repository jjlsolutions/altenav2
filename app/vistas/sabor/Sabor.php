<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

    $permisos_array = array();
    foreach ($_SESSION["permiso_perfil"] as $permiso)
    {
        foreach ($permiso as $valor2)
        {
        $permisos_array[] = $valor2['cve_permiso'];
        }
    }
	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Sabores</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sabores
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary" id="btnMostraModalSabor">Nuevo sabor</button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridSabor" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre sabor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Clave sabor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Producto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Editar</th>
                                <th>Estatus</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal_formSabor">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Sabor</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="cve_sabor" name="cve_sabor">
            <div class="row">
                <div class="form-group col-md-12">
                    <div id="msgAlert2"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Nombre sabor*</label>
                    <input type="text" class="form-control" id="nombre_sabor" name="nombre_sabor" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                </div>
                <div class="form-group col-md-4">
                    <label>Nombre clave*</label>
                    <input type="text" class="form-control" id="clave_sabor" name="clave_sabor" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                </div>
                <div class="form-group col-md-4">
                    <label>Producto*</label>
                    <select id="cveproducto_sabor" name="cveproducto_sabor" class="form-control ns_">
                        
                    </select>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">
    if(cveperfil == 1){
        $("#btnMostraModalSabor").hide();
    }else{
        <?php if(in_array(5, $permisos_array)){ ?>
        $("#btnMostraModalSabor").show();
        <?php }else{ ?>
            $("#btnMostraModalSabor").hide();
        <?php } ?>
    }
    $(document).ready(function () {
        tableSabor = $('#gridSabor').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "10%","className": "text-center","targets": 3},
                {"width": "10%","className": "text-center","targets": 4},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });
        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarProducto();
        cargarTablaSabor();
    });

    function cargarTablaSabor()
    {
        $.ajax({
            url      : 'Sabor/consultar',
            type     : "POST",
            data    : { 
                ban: 1 
            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tableSabor.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {

                        if (parseInt(val.estatus_sabor) == 1)
                        {
                            title = 'Sabor activo';
                            icon = 'fa fa-circle';
                            color_icon = "color: #4ad129;"
                            accion = "bloquearSabor ('" + val.cve_sabor  + "','0')";
                        }
                        else
                        {
                            title = 'Sabor bloqueado';
                            icon = 'fa fa-circle';
                            color_icon = "color: #f00;"
                            accion = "bloquearSabor ('" + val.cve_sabor  + "','1')";
                        }
                        <?php if(in_array(6, $permisos_array)){ ?>
                            var btn_editar = "<i class='fa fa-edit' style='font-size:18px; cursor: pointer;' title='Editar sabor' onclick=\"mostrarSabor('" + val.cve_sabor  + "')\"></i>";
                        <?php }else{ ?>
                            var btn_editar = "";
                        <?php } ?>
                        
                        <?php if(in_array(7, $permisos_array)){ ?>
                            var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";
                        <?php }else{ ?>
                            var btn_status = "";
                        <?php } ?>
                        

                        tableSabor.row.add([
                            val.nombre_sabor ,
                            val.clave_sabor,
                            val.nombre_producto ,
                            btn_editar,
                            btn_status,
                        ]).draw();
                    })

                }
                else
                {
                    tableSabor = $('#gridSabor').DataTable();
                    
                }

            }
        });
    }

    $('#btnMostraModalSabor').click(function (e) {
        $('#modal_formSabor').modal({
            keyboard: false
        });
        $('#cve_sabor').val('');
        $('#nombre_sabor').val('');
        $('#clave_sabor').val('');
        document.getElementById("cveproducto_sabor").selectedIndex = "0";
        $("#btnGuardar").html('Guardar');
        return false;
    });

    function cargarProducto(){
        $.ajax({
            url      : 'Producto/consultar',
            type     : "POST",
            data    : { 
                ban: 1
            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                select = $("#cveproducto_sabor");
                select.attr('disabled',false);
                select.find('option').remove();
                select.append('<option value="-1">-- Selecciona --</option>');

                if(myJson.arrayDatos.length > 0)
                {
                    $(myJson.arrayDatos).each( function(key, val)
                    {
                        select.append('<option value="' + val.cve_producto + '">' + val.nombre_producto + '</option>');
                    })

                }

            }
        });
    }

    $('#btnGuardar').click(function (e) {
        if ( $('#nombre_sabor').val()  == "" )
        {
            msgAlert2("Favor de ingresar el nombre del sabor.","warning");
        }
        if ( $('#clave_sabor').val()  == "" )
        {
            msgAlert2("Favor de ingresar la clave del sabor.","warning");
        }
        else if ( $('#cveproducto_sabor').val()  == "-1" )
        {
            msgAlert2("Favor de ingresar producto","warning");
        }
        else
        {
            $("#btnGuardar").prop('disabled', true);
            
            $.ajax({
                url      : 'Sabor/guardarSabor',
                data     : {
                    cve_sabor  : $('#cve_sabor').val() != '' ? $('#cve_sabor').val() : '0',
                    nombre_sabor  : $('#nombre_sabor').val() != '' ? $('#nombre_sabor').val() : '',
                    clave_sabor  : $('#clave_sabor').val() != '' ? $('#clave_sabor').val() : '',
                    cveproducto_sabor : $('#cveproducto_sabor').val() != '-1' ? $('#cveproducto_sabor').val() : '-1'
                },
                type: "POST",
                success: function(datos){
                    var myJson = JSON.parse(datos);
                    if(myJson.status == "success")
                    {
                        $('#modal_formSabor').modal('hide');
                        $('#cve_sabor').val('');
                        //Reinicializamos tabla
                        cargarTablaSabor();
                        msgAlert(myJson.msg ,"success");
                        //$('#msgAlert').css("display", "none");
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');
                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                        
                    }
                }
            }); 
        }
        return false;
    });

    function msgAlert2(msg,tipo)
    {
        $('#msgAlert2').css("display", "block");
        $("#msgAlert2").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
        setTimeout(function() { $("#msgAlert2").fadeOut(1500); },1500);
    }

    function mostrarSabor(cve_sabor )
    {
        $('#msgAlert').css("display", "none");
        
        $.ajax({
            url      : 'Sabor/consultar',
            type     : "POST",
            data     : { 
                    ban: 2, 
                    cve_sabor : cve_sabor  
            },
            beforeSend: function() {
                // setting a timeout
            },
            success  : function(datos) {
                var myJson = JSON.parse(datos);
                //console.log(myJson);
                $('#modal_formSabor').modal({
                    keyboard: false
                });
                $('#cve_sabor').val(myJson.arrayDatos[0].cve_sabor );
                $('#nombre_sabor').val(myJson.arrayDatos[0].nombre_sabor );
                $('#clave_sabor').val(myJson.arrayDatos[0].clave_sabor );
                $("#cveproducto_sabor").val(myJson.arrayDatos[0].cveproducto_sabor).change();
                $("#btnGuardar").html('Actualizar sabor');

            }
        });
    }

    function bloquearSabor (cve_sabor ,bloqueo)
    {
        if (bloqueo == 0)
        {
            var msg = "Esta seguro de bloquear este sabor?";
            var ban = 2;
        }else{
            var msg = "Esta seguro de desbloquear este sabor?";
            var ban = 3;
        }

        bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Si'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result == true){

                    $.ajax({
                        url      : 'Sabor/bloquearSabor ',
                        type     : "POST",
                        data     : { 

                                ban: ban, 
                                cve_sabor : cve_sabor  

                        },
                        beforeSend: function() {
                            // setting a timeout

                        },
                        success  : function(datos) {

                            var myJson = JSON.parse(datos);
                    
                            if(myJson.status == "success")
                            {

                                //var table = $('#gridSabor').DataTable();
                                        
                                //table.clear();
                                //table.destroy();

                                //Reinicializamos tabla
                                cargarTablaSabor();

                                msgAlert(myJson.msg ,"info");

                            }

                        }
                    });

                }else{
                    //No se hace nada...
                }
            }
        });

    }

    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
        setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
    }

</script>

</body>
</html>
