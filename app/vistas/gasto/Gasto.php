<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Gastos</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
   
   <style>
		#portapdf { width: auto; height: auto; border: 1px solid #484848; margin: 0 auto; }
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Gastos
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <button class="btn btn-primary" id="btnMostraModalGasto">Nuevo gasto</button>
                        <div class="float-right">
                            <span class="fa fa-plus-square  fa-2x" style='color: #007bff;' id="btnMostraModalCaGasto"  data-toggle="tooltip" data-placement="top" title="Nuevo catálogo de gasto"  aria-hidden="true" onclick="javascript:ModalNuevoRegistroCatalogo();" ></span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Fecha:</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <i class="far fa-calendar-alt"></i>
                                    </span>
                                    </div>
                                    <input type="text" class="form-control float-right" id="cmbFecha" name="cmbFecha" readonly>
                                    
                                </div>
                                
                                <!-- /.input group -->
                            </div>
                        </div>
                        
                    </div>
                    <div class="card-header">
                        
                        <div class="float-right">
                        <button class="btn btn-primary" id="btnBuscarGasto" onclick="buscarGasto();">Buscar</button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridGasto" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre gasto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Descripcion&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Sucursal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Total&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Ver</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<div class="modal fade" id="modal_formGasto">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Gasto</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="cve_gasto" name="cve_gasto">
            <div class="row">
                <div class="form-group col-md-12">
                    <div id="msgAlert2"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-4">
                    <label>Gasto*</label>
                    <select id="cvecagasto_gasto" name="cvecagasto_gasto" class="form-control ns_">
                        
                    </select>
                </div>
                <div class="form-group col-md-4">
                    <label>Descripción*</label>
                    <input type="text" class="form-control" id="descripcion_gasto" name="descripcion_gasto" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                </div>
                <div class="form-group col-md-4">
                    <label>Total*</label>
                    <input type="number" class="form-control" id="total_gasto" name="total_gasto">
                </div>
                <div id="divAdjunto">
                    <div class="form-group col-md-12">
                    <label for="txtArchivo">Documento digitalizado<span id="asttxtArchivo">*</span>:</label>
                            <input class="ns_" style="margin-bottom: 15px;float:left; margin:10px 0 0 0; word-wrap: break-word; width:100%;" id="txtArchivo" name="txtArchivo" type="file" accept=".pdf,.jpg,.jpeg,.gif,.png,.PDF,.JPG,.JPEG,.GIF,.PNG,.doc,.docx" onchange="checkfile(this,'.pdf,.jpg,.jpeg,.gif,.png,.PDF,.JPG,.JPEG,.GIF,.PNG,.doc,.docx')">
                            <div hidden='true' class='erroresTxt' id='errortxtArchivo'>Este campo es obligatorio.</div>
                    </div>
                </div>
                <div id="divImagen">
                    
                        <div id="portapdf" style="height: 400px;"></div>
                    
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modal_formCaGasto">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Catálogo de Gastos</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="form-group col-md-12">
                    <div id="msgAlert2"></div>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Nombre*</label>
                    <input type="text" class="form-control" id="nombre_cagasto" name="nombre_cagasto" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                </div>
            </div>
        </div>
        <div class="modal-footer justify-content-between">
                <button type="submit" class="btn btn-primary" id="btnGuardarCaGasto">Guardar</button>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">
    if(cveperfil == 1){
        $("#btnMostraModalGasto").hide();
        $("#btnMostraModalCaGasto").hide();
    }
    $(document).ready(function () {
        tableGasto = $('#gridGasto').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "10%","className": "text-center","targets": 3},
                {"width": "10%","className": "text-center","targets": 4},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });
        $('#cmbFecha').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarCaGasto();
        cargarTablaGasto();
    });

    function cargarTablaGasto()
    {
        $.ajax({
            url      : 'Gasto/consultar',
            type     : "POST",
            data    : { 
                ban: 1,
                fecha_venta: $("#cmbFecha").val() 
            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tableGasto.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {
                        title = 'Gasto activo';
                        icon = 'fa fa-minus-circle';
                        color_icon = "";
                        accion = "bloquearGasto ('" + val.cve_degasto  + "','"+val.imagen_degasto+"')";
                        

                        var btn_editar = "<i class='fa fa-eye' style='font-size:18px; cursor: pointer;' title='Ver gasto' onclick=\"mostrarGasto('" + val.cve_degasto  + "')\"></i>";
                        var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";

                        tableGasto.row.add([
                            val.nombre_gasto ,
                            val.descripcion_degasto,
                            val.nombre_sucursal,
                            val.total_degasto ,
                            btn_editar,
                            btn_status,
                        ]).draw();
                    })

                }
                else
                {
                    tableGasto = $('#gridGasto').DataTable();
                    
                }

            }
        });
    }

    $('#btnMostraModalGasto').click(function (e) {
        $('#modal_formGasto').modal({
            keyboard: false
        });
        $('#cve_gasto').val('');
        $('#descripcion_gasto').val('');
        $('#total_gasto').val('');
        document.getElementById("cvecagasto_gasto").selectedIndex = "0";
        $("#btnGuardar").show();
        $("#divAdjunto").show();
        $("#divImagen").hide();
        $("#btnGuardar").html('Guardar');
        return false;
    });

    function buscarGasto(){
        cargarTablaGasto();
    }

    function cargarCaGasto(){
        $.ajax({
            url      : 'Gasto/consultarCaGasto',
            type     : "POST",
            data    : { 
                ban: 1
            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                select = $("#cvecagasto_gasto");
                select.attr('disabled',false);
                select.find('option').remove();
                select.append('<option value="-1">-- Selecciona --</option>');

                if(myJson.arrayDatos.length > 0)
                {
                    $(myJson.arrayDatos).each( function(key, val)
                    {
                        select.append('<option value="' + val.cve_gasto + '">' + val.nombre_gasto + '</option>');
                    })

                }

            }
        });
    }

    function checkfile(sender,parametros)
    {
        var validExts = parametros.split(",");
        var fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

        if (validExts.indexOf(fileExt) < 0) {
            $('#msjError').html("<div class=\"alert alert-danger\" id=\"divAlertSuccess\">Favor de seleccionar un archivo de alguna de las siguientes extensiones <strong>"+parametros+"</strong></div>");
            $('#ventanaModal').modal('show');
            sender.value="";
            }
    }
    
    $('#btnGuardar').click(function (e) {
        if ( $('#descripcion_gasto').val()  == "" )
        {
            msgAlert2("Favor de ingresar alguna descripción.","warning");
        }
        if ( $('#total_gasto').val()  == "" )
        {
            msgAlert2("Favor de ingresar el total del gasto.","warning");
        }
        else if ( $('#cvecagasto_gasto').val()  == "-1" )
        {
            msgAlert2("Favor de ingresar gasto","warning");
        }
        else
        {
            $("#btnGuardar").prop('disabled', true);
            
            var file_data = $('#txtArchivo').prop('files')[0];

            var form_data = new FormData();
            form_data.append('file', file_data);
            form_data.append('cve_degasto', $('#cve_gasto').val() != '' ? $('#cve_gasto').val() : '0');
            form_data.append('descripcion_degasto', $('#descripcion_gasto').val() != '' ? $('#descripcion_gasto').val() : '');
            form_data.append('total_degasto', $('#total_gasto').val() != '' ? $('#total_gasto').val() : '');
            form_data.append('cvecagasto_degasto', $('#cvecagasto_gasto').val() != '-1' ? $('#cvecagasto_gasto').val() : '-1');
            $.ajax({
                url      : 'Gasto/guardarGasto',
                type	: "POST",
                enctype	: 'multipart/form-data',
                data	: form_data,
                processData: false,
                contentType: false,
                success: function(datos){
                    var myJson = JSON.parse(datos);
                    if(myJson.status == "success")
                    {
                        $('#modal_formGasto').modal('hide');
                        $('#cve_gasto').val('');
                        //Reinicializamos tabla
                        cargarTablaGasto();
                        msgAlert(myJson.msg ,"success");
                        //$('#msgAlert').css("display", "none");
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');
                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                        
                    }
                }
            }); 
        }
        return false;
    });

    $('#btnGuardarCaGasto').click(function (e) {
        if ( $('#nombre_cagasto').val()  == "" )
        {
            msgAlert2("Favor de ingresar algun nombre de gasto","warning");
        }
        else
        {
            $.ajax({
                url      : 'Gasto/guardarCaGasto',
                data     : {
                    ban : 1,
                    cve_cagasto : 0,
                    nombre_cagasto  : $('#nombre_cagasto').val() != '' ? $('#nombre_cagasto').val() : ''
                },
                type: "POST",
                success: function(datos){
                    var myJson = JSON.parse(datos);
                    if(myJson.status == "success")
                    {
                        $('#modal_formCaGasto').modal('hide');
                        //Reinicializamos tabla
                        cargarCaGasto();
                        msgAlert(myJson.msg ,"success");
                    }
                    else
                    {
                        $("#btnGuardarCaGasto").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                        
                    }
                }
            });
        }
        return false;
    });

    function msgAlert2(msg,tipo)
    {
        $('#msgAlert2').css("display", "block");
        $("#msgAlert2").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
        setTimeout(function() { $("#msgAlert2").fadeOut(1500); },1500);
    }

    function mostrarGasto(cve_degasto )
    {
        $('#msgAlert').css("display", "none");
        
        $.ajax({
            url      : 'Gasto/consultar',
            type     : "POST",
            data     : { 
                    ban: 2, 
                    cve_degasto : cve_degasto  
            },
            beforeSend: function() {
                // setting a timeout
            },
            success  : function(datos) {
                var myJson = JSON.parse(datos);
                //console.log(myJson);
                var ruta_archivo = 'public/img/gasto/'+myJson.arrayDatos[0].imagen_degasto;
                $("#portapdf").html('<object data="'+ruta_archivo+'" type="" width="100%" height="100%"></object>');
                $('#modal_formGasto').modal({
                    keyboard: false
                });
                $('#cve_gasto').val(myJson.arrayDatos[0].cve_degasto );
                $('#descripcion_gasto').val(myJson.arrayDatos[0].descripcion_degasto );
                $('#total_gasto').val(myJson.arrayDatos[0].total_degasto );
                $("#cvecagasto_gasto").val(myJson.arrayDatos[0].cvegasto_degasto).change();
                $("#btnGuardar").hide();
                $("#divAdjunto").hide();
                $("#divImagen").show();

            }
        });
    }

    function bloquearGasto (cve_degasto,imagen_degasto)
    {
            var msg = "Esta seguro de eliminar este gasto?";

        bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Si'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result == true){

                    $.ajax({
                        url      : 'Gasto/eliminarGasto ',
                        type     : "POST",
                        data     : { 
                            cve_degasto : cve_degasto,
                            imagen_degasto : imagen_degasto
                        },
                        success  : function(datos) {

                            var myJson = JSON.parse(datos);
                    
                            if(myJson.status == "success")
                            {

                                //var table = $('#gridGasto').DataTable();
                                        
                                //table.clear();
                                //table.destroy();

                                //Reinicializamos tabla
                                cargarTablaGasto();

                                msgAlert(myJson.msg ,"info");

                            }

                        }
                    });

                }else{
                    //No se hace nada...
                }
            }
        });

    }

    function ModalNuevoRegistroCatalogo()
    {
        $('#modal_formCaGasto').modal({
            keyboard: false
        });
        $('#nombre_cagasto').val('');
        return false;
    }

    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
        setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
    }

</script>

</body>
</html>
