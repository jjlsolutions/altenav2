<?php 
if($_SESSION['cveperfil_usuario'] != 1){
    session_start();
    session_unset();
    session_destroy(); 
    header("Location: login");
    exit;
}
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Concentrad  del día</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
   
   <style>
		#portapdf { width: auto; height: auto; border: 1px solid #484848; margin: 0 auto; }
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Concentrado cajero del día
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            
            <div class="clearfix" style="margin-top: 25px;">
                <div id="progress" class="progress">
                    <div class="progress-bar progress-bar-success"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Fecha:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <span class="input-group-text">
                                <i class="far fa-calendar-alt"></i>
                            </span>
                            </div>
                            <input type="text" class="form-control float-right" id="cmbFecha" name="cmbFecha" readonly>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Sucursal*</label>
                        <select class="form-control" id="cmbSucursal" name="cmbSucursal" onchange="cargarTablaVendedor(event);">
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                    <label for="CMBVENDEDOR">Buscar vendedor</label>
                    <datalist id="cmbVendedorListMod">
                    <!--option value="0" selected="selected"> -- Seleccione -- </option-->
                    </datalist>
                    <div class="input-group input-group-sm">
                        <input list="cmbVendedorListMod" onclick="limpiarVendedor()" id="CMBVENDEDOR" name="CMBVENDEDOR" type="text" class="form-control" placeholder=" -- Escriba -- " onkeyup="javascript:this.value=this.value.toUpperCase();" onchange="seleccionVendedor(event);" autocomplete="off">
                    </div>
                </div>
                <div class="col-md-2 float-right">
                    <div class="form-group float-right">
                        <label>&nbsp;</label>
                        <div class="input-group float-right">
                            <div class="input-group-prepend float-right">
                            <div class="float-right" id="btnBuscarVentas">
                                <button type="submit" class="btn btn-primary" id="btnBuscarVentas" onclick="consultarConcentradocajero();">Buscar</button>
                            </div>
                            </div>
                        </div>
                        <!-- /.input group -->
                    </div>
                </div>
            </div>
            
            <div class="container-fluid">
                
            <div class="row" id="divtotalVendedor">
                
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- /.modal -->

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">
  
    $(document).ready(function () {
        consultarSucursal();
        $('#cmbFecha').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });
        
        consultarConcentradocajero();
        cargarTablaVendedor();
        
    });

    function consultarConcentradocajero(){
        
        divtotalVendedor = $("#divtotalVendedor");
        $("#divtotalVendedor").empty();

        var val = $('#CMBVENDEDOR').val() ? $('#CMBVENDEDOR').val() : '';
        var valueCombo = $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") : "";
        var cveusuario = valueCombo.cveusuario ? valueCombo.cveusuario : '';
        $.ajax({
            xhr: function() {
            var xhr = new window.XMLHttpRequest();
            var bar = $('.progress-bar');

            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                //console.log(percentComplete);

                var percentVal = percentComplete+'%';
                bar.width(percentVal);

                }
            }, false);
            return xhr;
            },
            url      : 'concentradocajero/consultar',
            type     : "POST",
            data    : { 
                ban: 4,
                fecha_concentradocajero: $("#cmbFecha").val(),
                sucursal_concentradocajero : $('#cmbSucursal').val() != '-1' ? $('#cmbSucursal').val() : '-1',
                cveusuario_concentradocajero : cveusuario != '' ? cveusuario : '0'
            },
            success  : function(datos) {
                var myJson = JSON.parse(datos);
                var cadenaTotales = '';
                if(myJson.arrayDatos.length > 0)
                {      
                    $(myJson.arrayDatos).each( function(key, val)
                    { 
                        cadenaTotales+='<div class="col-md-3">'+
                                            '<div class="card card-primary card-outline">'+
                                                '<div class="card-body box-profile">'+
                                                    '<h3 class="profile-username text-center">'+val.nombrecompleto_venta+'</h3>'+
                                                    '<p class="text-muted text-center">'+val.nombre_sucursal+'</p>'+
                                                    '<p class="text-muted text-center">'+val.nombre_perfil+'</p>'+
                                                    '<ul class="list-group list-group-unbordered mb-3">'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Total venta</b> <a class="float-right">&nbsp;</a><a class="float-right">$'+val.total_venta+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Total caja</b> <a class="float-right">&nbsp;</a><a class="float-right">$'+val.totalcaja_venta+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Total precio especial</b> <a class="float-right">&nbsp;</a><a class="float-right">$'+val.precioespecial_venta+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Cantidad mostrador</b> <a class="float-right">&nbsp;</a><a class="float-right">'+val.cantidad_venta_mostrador+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Cantidad domicilio</b> <a class="float-right">&nbsp;</a><a class="float-right">'+val.cantidad_venta_domicilio+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Menudeo mostrador</b><br> <a class="float-right">Total: $'+(val.total_mostrador_menudeo != null ? val.total_mostrador_menudeo : '0')+'</a><br><a class="float-right">Cant.: '+val.cantidad_mostrador_menudeo+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Menudeo domicilio</b><br> <a class="float-right">Total: $'+(val.total_domicilio_menudeo != null ? val.total_domicilio_menudeo : '0')+'</a> <br><a class="float-right">Cant.: '+val.cantidad_domicilio_menudeo+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Mayoreo mostrador</b><br> <a class="float-right">Total: $'+(val.total_mostrador_mayoreo != null ? val.total_mostrador_mayoreo : '0')+'</a> <br><a class="float-right">Cant.: '+val.cantidad_mostrador_mayoreo+'</a>'+
                                                        '</li>'+
                                                        '<li class="list-group-item">'+
                                                            '<b>Mayoreo domicilio</b><br> <a class="float-right">Total: $'+(val.total_domicilio_mayoreo != null ? val.total_domicilio_mayoreo : '0')+'</a> <br><a class="float-right">Cant.: '+val.cantidad_domicilio_mayoreo+'</a>'+
                                                        '</li>'+
                                                    '</ul>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>';
                    });
                   
                    divtotalVendedor.append(cadenaTotales);
                    var bar = $('.progress-bar');
                    bar.width('0%');
                }
                else
                {                    
                    
                    var bar = $('.progress-bar');
                    bar.width('0%');                  
                }

            }
        });
    }

    function consultarSucursal(){
        //Cargamos combo sucursal
        $.ajax({
            url      : 'sucursal/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                //console.log(myJson.arrayDatos[0].cve_perfil);
                
                var select = $("#cmbSucursal");
                select.find('option').remove();

                $(myJson.arrayDatos).each( function(key, val){
                    select.append('<option value="' + val.cve_sucursal + '">' + val.nombre_sucursal + '</option>');
                });

               
            }
        });
    }

    function cargarTablaVendedor()
    {
        $.ajax({
            url      : 'Usuario/consultarCaConcentrado',
            type     : "POST",
            data    : { 
                ban: 1,
                sucursal_concentradocajero : $('#cmbSucursal').val() != null ? $('#cmbSucursal').val() : '1',
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                if(myJson.arrayDatos.length > 0)
                {
                    var select2 = $("#cmbVendedorListMod");
                    select2.find('option').remove();
                    
                    $(myJson.arrayDatos).each( function(key, val)
                    {
                    value = '{"cveusuario":"'+val.cve_usuario+'","nombreCompleto":"'+val.nombreCompleto.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'","nombresucursal":"'+val.nombre_sucursal+'","loginusuario":"'+val.login_usuario+'","nombreperfil":"'+val.nombre_perfil.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'"}';
                    select2.append("<option data-value='"+value+"' value='"+val.nombreCompleto.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+"'>");
                    });

                    $('#CMBVENDEDOR').val("-- Escriba --");

                }

                

            }
        });
    }

    function seleccionVendedor(){
        var val = $('#CMBVENDEDOR').val() ? $('#CMBVENDEDOR').val() : '';
        if(val != '')
        {
        if(val.indexOf("\"") !== -1){
            var valueCombo = $("#cmbVendedorListMod").find("option[value='"+val+"']").data("value") ? $("#cmbVendedorListMod").find("option[value='"+val+"']").data("value") : "";
        }
        else{
        var valueCombo = $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") : "";
        }

        var cveusuario = valueCombo.cveusuario ? valueCombo.cveusuario : '';
        var nombreCompleto = valueCombo.nombreCompleto ? valueCombo.nombreCompleto : '';
        var nombresucursal = valueCombo.nombresucursal ? valueCombo.nombresucursal : '';
        var loginusuario = valueCombo.loginusuario ? valueCombo.loginusuario : '';
        var nombreperfil = valueCombo.nombreperfil ? valueCombo.nombreperfil : '';
        
        var bool = false;

            if(cveusuario > 0){
            
        
            }
            else{
            
            $("#CMBVENDEDOR").val("");
            

            }

        

        }
        else{
        alert("Favor de ingresar el nombre del contacto.");
        
        }
    }

    function limpiarVendedor(){
        $("#CMBVENDEDOR").val("");
    }

</script>

</body>
</html>
