<?php 
session_start();
if (isset($_SESSION["cve_usuario"]))
{
	session_write_close();
	header("Location: home");
	exit;
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  	<title><?php echo NOMBRE_SITIO; ?> | Login</title>
  	<?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
</head>
<style>
		.container {
			background-color: #fff;
			border-radius: 10px;
			box-shadow: 0 14px 28px rgba(0,0,0,0.25), 
					0 10px 10px rgba(0,0,0,0.22);
			position: relative;
			overflow: hidden;
			max-width: 100%;
		}
		.login-page {
			background: #7476ff;
		}
	</style>
<body class="hold-transition login-page">

<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary container">
    <div class="card-header text-center">
	<div class="login-logo">
  		<img src="img/LogoAltena.jpg" width="130" height="130" class="img-circle">
  	</div>
    </div>
    <div class="card-body">

      <form action="login/verificarUsuario" id="login" method="post">
        <div class="input-group mb-3">
          <input type="text" id="txt_usr" name="txt_usr" class="form-control" placeholder="Usuario">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" id="txt_pass" name="txt_pass" class="form-control" placeholder="Password" id="btnEntrar">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
</div>
<!-- /.login-box -->
<?php 
	include RUTA_APP . 'vistas/includes/script.php';
?>
<script type="text/javascript">

	$('#login').on('submit',function(e){
	    e.preventDefault();

	    var usr = $('#txt_usr').val(); 
		var pass = $('#txt_pass').val();

	    if (usr == "")
		{
			msgError("Favor de agregar tu usuario.","warning");
		}
		else if (pass == "")
		{
			msgError("Favor de agregar tu contraseña.","warning");
		}
		else
		{
			$("#btnEntrar").prop('disabled', true);

		    $.ajax({
		        type     : "POST",
		        url      : $(this).attr('action'),
		        data     : $(this).serialize(),
		        beforeSend: function() {
			        // setting a timeout
			        msgError("Iniciando...","info");
			        //setTimeout(function() { }, 1500);
			    },
		        success  : function(datos) {
		           	var json = eval("(" + datos + ")");

					if (json.sesion == 1){
						//msgError("Sesion iniciada..!","success");
						location.href='home';
					}
					else
					{
						msgError(json.msg,"danger");
						$("#btnEntrar").prop('disabled', false);
					}
		        }
		    });
		}

	});

	function msgError(msg,tipo)
	{
		$('#msgError').css("display", "block");
		$("#msgError").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
	}

</script>

</body>
</html>