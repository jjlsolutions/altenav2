<?php 

  if($_SESSION['cveperfil_usuario'] == 1){
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

  $permisos_array = array();
  foreach ($_SESSION["permiso_perfil"] as $permiso)
  {
    foreach ($permiso as $valor2)
    {
      $permisos_array[] = $valor2['cve_permiso'];
    }
  }

  $fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo NOMBRE_SITIO; ?> | Domicilio</title>
    <?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
    <style>
        .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
          }
          .custom-control-label::before {
            position: absolute;
            top: 0.34rem;
            left: -1.5rem;
            display: block;
            width: 1.4rem;
            height: 1.4rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
          }
          .custom-control-label::after {
            position: absolute;
            top: 0.50rem;
            left: -1.3rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: 100% / 100% 100% no-repeat;
          }
          .custom-checkbox .custom-control-label::before{
            border-radius: 1.0rem;
          }
          .btn-app {
            border-radius: 40px;
            border: none;
            margin: .5rem;
            box-shadow: 0 3px 5px rgba(0,0,0,.4);
            padding: 12px 2px;
          }
    </style>
</head>
<body id="idScrool" class="hold-transition sidebar-mini">
  <div class="wrapper">
    
      <?php 
      include RUTA_APP . 'vistas/includes/header.php';

      include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
      ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section  class="content-header">
        <div class="container-fluid">
          <div class="row">
            <div class="form-group col-md-12">
                <div id="msgAlert"></div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-12">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="home">Home</a></li>
                <li class="breadcrumb-item active">Domicilio</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <section class="content"> 
      <div id="clienteAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" style="color: black;" data-toggle="collapse" href="#divCliente" aria-expanded="false">
              <div id="divClienteTexto">Cliente</div> 
              </a>
            </h4>
          </div>
          <div id="divCliente" class="collapse" data-parent="#clienteAcordeon">
            <div class="card-body">
              <div class="row">
              <label for="CMBCLIENTES">Buscar contactos</label>
                <datalist id="cmbClientesListMod">
                  <!--option value="0" selected="selected"> -- Seleccione -- </option-->
                </datalist>
                <div class="input-group input-group-sm">
                <input list="cmbClientesListMod" onclick="limpiarCliente()" id="CMBCLIENTES" name="CMBCLIENTES" type="text" class="form-control" placeholder=" -- Escriba -- " onkeyup="javascript:this.value=this.value.toUpperCase();" onchange="seleccionCliente(event);" autocomplete="off">
                  <span class="input-group-append" onclick="nuevoCliente()" id="agregarNuevoCliente" data-toggle="tooltip" data-placement="button" title="Agregar nuevo cliente">
                  <i class="fas fa-plus-circle" style="cursor: pointer; color: #007bff; font-size: 27px; margin-left:12px;"></i>
                  </span>
                </div>
              </div>
              <br>
              <div class="callout callout-info" id="divTarjetaCliente" style="display:none;">
              
                <div class="row invoice-info">
                  <div class="col-sm-4 invoice-col">
                    <address>
                    <label><strong> Cliente: </strong></label>
                    <h6 id="nombrecliente"></h6>
                    <label><strong> Dirección: </strong></label>
                    <h6 id="direccioncliente"> </h6>
                    <label><strong> Celular: </strong></label>
                    <h6 id="celularcliente"> </h6>
                    </address>
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    <address>
                      <label><strong> Comentario: </strong></label>
                      <h6 id="comentariocliente"> </h6>
                    </address>
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <section class="content"> 
      <div id="pedidoAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" data-toggle="collapse" style="color: black;" href="#divPedido" aria-expanded="false">
              <div id="divPedidoTexto">Pedido</div>
              </a>
            </h4>
          </div>
          <div id="divPedido" class="collapse" data-parent="#pedidoAcordeon" style="">
            <div class="card-body">
              <div class="row">
                <div class="col-12"> 
                  <div class="custom-control custom-checkbox float-right"> 
                      <input class="custom-control-input" type="checkbox" id="CheckboxMayoreo" onclick="ChecaMayoreo()" checked="">
                      <label for="CheckboxMayoreo" class="custom-control-label">Mayoreo</label>
                  </div>
                </div>
              </div>
              <hr>
                <div class="row">
                  <div class="col-12 col-sm-12">
                      <div class="card card-info card-tabs">
                          <div class="card-header p-0 pt-1">
                              <ul class="nav nav-tabs" id="pestanas" role="tablist">
                              
                              </ul>
                          </div>
                          <div class="card-body">
                            <form id="formUsuario" action="domicilio/guardarPedido" method="post">
                              <div class="tab-content panel-body" id="pestanasContent">
                            
                              </div>
                            </form>
                          </div>
                          <!-- /.card -->
                      </div>
                  </div>
                </div> 
            </div>
        </div>
      </div>
      </section>
      <section class="content"> 
      <div id="observacionesAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" data-toggle="collapse" style="color: black;" href="#divobservaciones" aria-expanded="false">
              <div id="divObservacionesTexto">Observaciones</div>
              </a>
            </h4>
          </div>
          <div id="divobservaciones" class="collapse" data-parent="#observacionesAcordeon" style="">
          <div class="card-body">
              <div class="form-group">
                <textarea class="form-control" rows="3" id="observaciones_venta" placeholder="Escribe las observaciones ..." onkeyup="ObservacionesCheck()"></textarea>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>


      <!-- Main content -->
      <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col-12"> 
              <div class="custom-control custom-checkbox float-right"> 
                <span class="mr-2" id="CantidadGlobal" style="font-size: 150%;">
                <i class="fas fa-circle text-primary"></i> 0
                </span>

                <span class="mr-5" id="TotalGlobal" style="font-size: 150%;">
                <i class="fas fa-circle text-warning"></i> $0
                </span>
                <input class="custom-control-input" type="checkbox" id="CheckboxEnvio" onclick="ChecaEnvio()" checked="" >
                <label for="CheckboxEnvio" class="custom-control-label">Cobrar envío&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
              </div>
            </div>
          </div>
          <div class="row" id="divPrecioEspecial">
            <div class="col-10"> 
              <div class="custom-control custom-checkbox float-right"> 
                <input class="custom-control-input" type="checkbox" id="CheckboxPrecioEspecial" onclick="ChecaPrecioEspecial()" >
                <label for="CheckboxPrecioEspecial" class="custom-control-label">Precio especial</label>
                    
              </div>
            </div>
            <div class="col-2">
                <input type="text" class="form-control-sm" id="inputPrecioEspecial" placeholder="% Precio especial" maxlength="3" onkeyup="comprueba(this)" min="1" pattern="^[0-9]+">
                </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
          
          </div>
          <div class="col-6">
            <div class="float-right" style="font-size: 150%;">Total: $<label id="txtTotalTotalGlobal" >0</label></div><br>
            <input id="txtAuxTotalTotalGlobal" name="txtAuxTotalTotalGlobal" type="hidden" value="0">
          </div>
        </div>
      </section>
      <!-- /.content -->
      
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
    <div class="clearfix">
        <div class="progress">
          <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-10">
          <button type="button" class="btn btn-block btn-primary btn-lg" id="btnCobrar" onclick="guardar()">REGISTRAR PEDIDO</button>
          </div>
          <div class="col-2">
              <a id="back-to-top" href="#" class="btn btn-outline-primary back-to-top" role="button" aria-label="Scroll to top">
              <i class="fas fa-chevron-up"></i>
              </a>
          </div>    
      </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <div class="modal fade" id="modal_formCantidad">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="nombre_sabor"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <input type="hidden" id="cve_sabor" name="cve_sabor">
            <input type="hidden" id="idPestana" name="idPestana">
            <input type="hidden" id="preciomayoreo_sabor" name="preciomayoreo_sabor">
            <input type="hidden" id="preciomenudeo_sabor" name="preciomenudeo_sabor">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Cantidad*</label>
                    <input type="number" min="0" class="form-control form-control-lg" style="border: 0;box-shadow: none;font-size: 38pt;text-align: center;" id="cantidad_sabor" name="cantidad_sabor" onkeyup='javascript:this.value=this.value.toUpperCase();sacaTotalProducto(this.value)'>
                </div>
            </div>
            <div class="row">
              <div class="col-6">
              
              </div>
              <div class="col-6">
                <div class="float-right"><label id="txtTotalGlobal">Total*</label></div><br>
              </div>
            </div>
            
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-primary" onclick="pasarSpan()" style="width:40%;">OK</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>

  <div class="modal fade" id="modal_formCliente" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Cliente</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formSucursal"> 

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalInfoVenta">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información de venta</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-md-6">
              <h7>Folio: </h7>
              <label id="folio_venta"></label>
          </div>
          <div class="form-group col-md-6">
              <h7>Total: </h7>
              <label id="total_venta"></label>
          </div>
          <div class="form-group col-md-6">
              <h7>Cobro envio: </h7>
              <label id="cobroenvio_venta"></label>
          </div>
          <div class="form-group col-md-6">
              <h7>Precio especial: </h7>
              <label id="precioespecial_venta"></label>
          </div>
          <div class="form-group col-md-6">
            <h7>Tipo: </h7>
            <label id="tipoprecio_venta"></label>
          </div>
          <div class="form-group col-md-12">
            <h7>Cliente: </h7>
            <label id="nombrecliente_venta"></label>
          </div>
          <div class="form-group col-md-6">
            <h7>fecha: </h7>
            <label id="fechaadd_venta"></label>
          </div>
      </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
  <!-- ./wrapper -->
      <?php 
      include RUTA_APP . 'vistas/includes/script.php';
      ?>
<script type="text/javascript">

  $(document).ready(function () {
    if(cveperfil == 1){
        $("#agregarNuevoCliente").hide();
    }
    cargarTablaCliente();
    cargarBotonesProducto();

    $('#cantidad_sabor').keypress(function(e) {
      if (e.which == 13) {
        pasarSpan();
      } 
    });

    var bar = $('.progress-bar');
		bar.width('0%');

    <?php if(in_array(18, $permisos_array)){ ?>
      $("#divPrecioEspecial").show();
    <?php }else{ ?>
        $("#divPrecioEspecial").hide();
    <?php } ?>
  });
  
  function cargarBotonesProducto(){
      $.ajax({
          url      : 'Producto/consultar',
          type     : "POST",
          data    : { 
              ban: 3
          },
          success  : function(datos) {

              var myJson = JSON.parse(datos);

                          
              pestanas = $("#pestanas");
              $("#pestanas").empty();
              
              pestanasContent = $("#pestanasContent");
              $("#pestanasContent").empty();


              if(myJson.arrayDatos.length > 0)
              {
                  $(myJson.arrayDatos).each( function(key, val)
                  {
                    if(val.stock > 0){
                      pestanas.append('<li class="nav-item"> <a class="nav-link " id="pestana_' + val.cve_producto + '" data-toggle="pill" href="#contenido1_' + val.cve_producto + '" role="tab" aria-controls="custom-tabs-one-home" aria-selected="true"><div id="pestanaIcono_' + val.cve_producto + '">' + val.nombre_producto + '</div></a></li>');
                      pestanasContent.append('<div class="tab-pane fade" id="contenido1_' + val.cve_producto + '" role="tabpanel" aria-labelledby="custom-tabs-one-home-tab"><div class="row" id="contenido_' + val.cve_producto + '"></div></div>');
                      
                      cargarBotonesSabores(val.cve_producto);
                    }

                  });

              }

              

          }
      });
  }

  function cargarBotonesSabores(cve_producto){
      $.ajax({
          url      : 'Sabor/consultar',
          type     : "POST",
          data    : { 
              ban: 3,
              cve_sabor : cve_producto
          },
          success  : function(datos) {

              var myJson = JSON.parse(datos);

              botonesSabores = $("#contenido_"+cve_producto);
              $("#contenido_"+cve_producto).empty();
              
              if(myJson.arrayDatos.length > 0)
              {
                $(myJson.arrayDatos).each( function(key, val)
                {
                  
                  botonesSabores.append('<div class="col-4"><a class="btn btn-app bg-success btn-block" id="sabor_' + val.cve_sabor + '" onClick="cantidad(' + cve_producto + ','+ val.cve_sabor + ',\'' + val.nombre_sabor + '\',' + val.preciomayoreo_producto + ',' + val.preciomenudeo_producto + ')"><b style="font-size: 14px;">' + val.nombre_sabor + '</b></a></div>');
                  
                });
               
              }
              

              

          }
      });
  }

  function comprueba(valor){
   
  if(valor.value > 0){
    
   
  }else{
    $("#inputPrecioEspecial").val("");
  }
}

  function cantidad(cve_producto, cve_sabor, nombre_sabor, preciomayoreo_sabor, preciomenudeo_sabor){
    
      $('#idPestana').val(cve_producto);
      $('#cve_sabor').val(cve_sabor);
      $('#preciomayoreo_sabor').val(preciomayoreo_sabor);
      $('#preciomenudeo_sabor').val(preciomenudeo_sabor);
      $('#txtTotalGlobal').text('Total : $0');
      $('#cantidad_sabor').val("");
      $('#nombre_sabor').text(nombre_sabor);
      
      $('#modal_formCantidad').modal({
          keyboard: false,
      });
      $('#modal_formCantidad').on('shown.bs.modal', function () {
        $('#cantidad_sabor').focus();
      });
  }

  function sacaTotalProducto(valor){
    var totalProCuenta = 0; 
    var preciomayoreo_sabor = $('#preciomayoreo_sabor').val();
    var preciomenudeo_sabor = $('#preciomenudeo_sabor').val();
    var precio;
    if($("#CheckboxMayoreo").prop("checked")){
      precio = preciomayoreo_sabor;
    }
    else{
      precio = preciomenudeo_sabor;
    }
    totalProCuenta = precio * valor;
    $('#txtTotalGlobal').text("Total : $"+totalProCuenta);
    
  }

  function pasarSpan(){
    idPestana = $("#idPestana").val();
    nombreProducto = $("#pestanaIcono_"+idPestana).text();
    nombrePedidoTexto = $("#divPedidoTexto").text();
    

    pestanaIcono = $("#pestanaIcono_"+idPestana);
    $("#pestanaIcono_"+idPestana).empty();
    pestanaIcono.append(nombreProducto+' <i class="fas fa-check"></i>');

    divPedidoTexto = $("#divPedidoTexto");
    $("#divPedidoTexto").empty();
    divPedidoTexto.append(nombrePedidoTexto+' <i class="fas fa-check"></i>');

   


    cve_sabor = $('#cve_sabor').val();
    preciomayoreo_sabor = $('#preciomayoreo_sabor').val();
    preciomenudeo_sabor = $('#preciomenudeo_sabor').val();

    var precio;
    if($("#CheckboxMayoreo").prop("checked")){
      precio = preciomayoreo_sabor;
    }
    else{
      precio = preciomenudeo_sabor;
    }
    
    cantidad_sabor = $("#cantidad_sabor").val();

    total = cantidad_sabor * precio;
    if(total == '0'){
      $("#sabor_"+cve_sabor).find("span").remove();//remove span elements    
    }
    else{
      $("#sabor_"+cve_sabor).find("span").remove();//remove span elements    
      $("#sabor_"+cve_sabor).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+cve_sabor+'"><b>'+cantidad_sabor+'</b></span>');
      $("#sabor_"+cve_sabor).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+cve_sabor+'"><b>$'+total+'</b></span>');

      $("#sabor_"+cve_sabor).append('<span id="spanMayoreo_'+cve_sabor+'">'+preciomayoreo_sabor+'</span>');
      $("#sabor_"+cve_sabor).append('<span id="spanMenudeo_'+cve_sabor+'">'+preciomenudeo_sabor+'</span>');
      $("#spanMayoreo_"+cve_sabor).hide();
      $("#spanMenudeo_"+cve_sabor).hide();
    
    }
    var entro = 0;
    $("#formUsuario").find('#contenido_'+idPestana+' a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");
      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
        if(parseInt(cantidadProducto) >= 0){
          entro = 1;
        }
      

    });
    if(entro == 0){
      pestanaIcono = $("#pestanaIcono_"+idPestana);
      $("#pestanaIcono_"+idPestana).empty();
      pestanaIcono.append(nombreProducto);
    }
    

    sacaTotalGlobal();
    $("#cantidad_sabor").val("");
    $('#modal_formCantidad').modal('hide');
      
  }

  function ObservacionesCheck(){
    nombreObservacionesTexto = $("#divObservacionesTexto").text();
    var Observaciones = $("#observaciones_venta").val();
    if(Observaciones == ""){

      divObservacionesTexto = $("#divObservacionesTexto");
      $("#divObservacionesTexto").empty();
      divObservacionesTexto.append(nombreObservacionesTexto);
    }
    else{
      divObservacionesTexto = $("#divObservacionesTexto");
      $("#divObservacionesTexto").empty();
      divObservacionesTexto.append(nombreObservacionesTexto+' <i class="fas fa-check"></i>');

    }

  }

  function sacaTotalGlobal(){
    var totalGlobal = 0;
    var cantidadGlobal = 0;
      $("#formUsuario").find('a').each(function() {
        var elemento= this;
        myArr = elemento.id.split("_");

        cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
        if(parseInt(cantidadProducto) > 0){
          cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);
        }

        TotalProducto = $("#spanTotal_"+myArr[1]).text();
        myArr2 = TotalProducto.split("$");
        if(myArr2[1] >= 0){
          totalGlobal = parseFloat(totalGlobal) + parseFloat(myArr2[1]); 
        }
      });
      $("#CantidadGlobal").empty();

      $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
      $("#CantidadGlobal").append(" "+cantidadGlobal.toString()+" ");

      if(cantidadGlobal <= 0){
        divPedidoTexto = $("#divPedidoTexto");
        $("#divPedidoTexto").empty();
        divPedidoTexto.append(nombrePedidoTexto);
      }

      $("#TotalGlobal").empty();
      
      $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
      $("#TotalGlobal").append(" $"+totalGlobal.toString()+" ");

      if(totalGlobal > 0){
        $("#txtAuxTotalTotalGlobal").val(totalGlobal.toString());
        if($("#CheckboxEnvio").prop("checked")){
          totalGlobal = totalGlobal + 20;
        }
      }
      
      $("#txtTotalTotalGlobal").empty();
      $("#txtTotalTotalGlobal").append(totalGlobal.toString());
      ChecaPrecioEspecial();

  }

  function ChecaMayoreo(){
    var totalGlobal = 0;
    var cantidadGlobal = 0;
    nombrePedidoTexto = $("#divPedidoTexto").text();
      $("#formUsuario").find('a').each(function() {
        var elemento= this;
        myArr = elemento.id.split("_");

        cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
        
        if(parseInt(cantidadProducto) > 0){
          cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);
        }

        TotalProducto = $("#spanTotal_"+myArr[1]).text();
        myArr2 = TotalProducto.split("$");
        if(myArr2[1] >= 0){
          spanMayoreo = $("#spanMayoreo_"+myArr[1]).text();
          spanMenudeo = $("#spanMenudeo_"+myArr[1]).text();
          if($("#CheckboxMayoreo").prop("checked")){
            total = spanMayoreo * cantidadProducto;
          }
          else{
            total = spanMenudeo * cantidadProducto;
          }
          
            $("#sabor_"+myArr[1]).find("span").remove();//remove span elements    
            $("#sabor_"+myArr[1]).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+myArr[1]+'"><b>'+cantidadProducto+'</b></span>');
            $("#sabor_"+myArr[1]).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+myArr[1]+'"><b>$'+total+'</b></span>');

            $("#sabor_"+myArr[1]).append('<span id="spanMayoreo_'+myArr[1]+'">'+spanMayoreo+'</span>');
            $("#sabor_"+myArr[1]).append('<span id="spanMenudeo_'+myArr[1]+'">'+spanMenudeo+'</span>');
            $("#spanMayoreo_"+myArr[1]).hide();
            $("#spanMenudeo_"+myArr[1]).hide();
            totalGlobal = parseFloat(totalGlobal) + parseFloat(total);
        }
      });

      $("#CantidadGlobal").empty();

      $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
      $("#CantidadGlobal").append(" "+cantidadGlobal.toString()+" ");

      if(cantidadGlobal <= 0){
        divPedidoTexto = $("#divPedidoTexto");
        $("#divPedidoTexto").empty();
        divPedidoTexto.append(nombrePedidoTexto);
      }

      $("#TotalGlobal").empty();
      
      $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
      $("#TotalGlobal").append(" $"+totalGlobal.toString()+" ");
      if(totalGlobal > 0){
        $("#txtAuxTotalTotalGlobal").val(totalGlobal.toString());
        if($("#CheckboxEnvio").prop("checked")){
          totalGlobal = totalGlobal + 20;
        }
      }
      
      $("#txtTotalTotalGlobal").empty();
      $("#txtTotalTotalGlobal").append(totalGlobal.toString());
      ChecaPrecioEspecial();
   
  }

  function cargarTablaCliente()
  {
    $.ajax({
          url      : 'Cliente/consultar',
          type     : "POST",
          data    : { 
              ban: 1
          },
          success  : function(datos) {

              var myJson = JSON.parse(datos);

              if(myJson.arrayDatos.length > 0)
              {
                var select2 = $("#cmbClientesListMod");
                select2.find('option').remove();
                
                $(myJson.arrayDatos).each( function(key, val)
                {
                  value = '{"cvecliente":"'+val.cve_cliente+'","nombrecliente":"'+val.nombre_cliente.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'","direccioncliente":"'+val.direccion_cliente.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'","celularcliente":"'+val.celular_cliente+'","comentariocliente":"'+val.comentario_cliente.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'"}';
                  select2.append("<option data-value='"+value+"' value='"+val.nombre_cliente.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+" | "+val.celular_cliente.replace(/'/g, "&#x27;")+"'>");
                });

                $('#CMBCLIENTES').val("-- Escriba --");

              }

              

          }
      });
  }

  function limpiarCliente(){
    $("#CMBCLIENTES").val("");
    $("#nombrecliente").text("");
    $("#direccioncliente").text("");
    $("#celularcliente").text("");
    $("#comentariocliente").text("");
    $('#divTarjetaCliente').hide();

    nombreClienteTexto = $("#divClienteTexto").text();

          divClienteTexto = $("#divClienteTexto");
          $("#divClienteTexto").empty();
          divClienteTexto.append(nombreClienteTexto);
  }
  function seleccionCliente(){
    var val = $('#CMBCLIENTES').val() ? $('#CMBCLIENTES').val() : '';
    if(val != '')
    {
      if(val.indexOf("\"") !== -1){
        var valueCombo = $("#cmbClientesListMod").find("option[value='"+val+"']").data("value") ? $("#cmbClientesListMod").find("option[value='"+val+"']").data("value") : "";
      }
      else{
      var valueCombo = $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") : "";
      }

      var cvecliente = valueCombo.cvecliente ? valueCombo.cvecliente : '';
      var nombrecliente = valueCombo.nombrecliente ? valueCombo.nombrecliente : '';
      var direccioncliente = valueCombo.direccioncliente ? valueCombo.direccioncliente : '';
      var celularcliente = valueCombo.celularcliente ? valueCombo.celularcliente : '';
      var comentariocliente = valueCombo.comentariocliente ? valueCombo.comentariocliente : '';
      
      var bool = false;

        if(cvecliente > 0){
          
          $("#nombrecliente").text(nombrecliente);

          $("#direccioncliente").text(direccioncliente);

          $("#celularcliente").text(celularcliente);

          $("#comentariocliente").text(comentariocliente);
          $('#divTarjetaCliente').show();

          nombreClienteTexto = $("#divClienteTexto").text();

          divClienteTexto = $("#divClienteTexto");
          $("#divClienteTexto").empty();
          divClienteTexto.append(nombreClienteTexto+' <i class="fas fa-check"></i>');
        }
        else{
          
          $("#CMBCLIENTES").val("");
          $("#nombrecliente").text("");
          $("#direccioncliente").text("");
          $("#celularcliente").text("");
          $("#comentariocliente").text("");
          $('#divTarjetaCliente').hide();

          nombreClienteTexto = $("#divClienteTexto").text();

          divClienteTexto = $("#divClienteTexto");
          $("#divClienteTexto").empty();
          divClienteTexto.append(nombreClienteTexto);

        }

      

    }
    else{
      alert("Favor de ingresar el nombre del contacto.");
      
    }
  }

  function ChecaEnvio(){
    if($("#txtTotalTotalGlobal").text() != '0'){
      if($("#CheckboxEnvio").prop("checked")){
        totalGlobal = parseFloat($("#txtTotalTotalGlobal").text()) + 20;
      }
      else{
        totalGlobal = parseFloat($("#txtTotalTotalGlobal").text()) - 20;
      }
      $("#txtTotalTotalGlobal").empty();
      $("#txtTotalTotalGlobal").append(totalGlobal.toString());
    }
  }

  function ChecaPrecioEspecial(){
    
    if($("#txtTotalTotalGlobal").text() != '0'){
      if($("#CheckboxPrecioEspecial").prop("checked")){
        if($("#inputPrecioEspecial").val() != ''){
          $("#inputPrecioEspecial").prop('disabled', true);
          ahorro = (parseFloat($("#txtAuxTotalTotalGlobal").val()) * parseFloat($("#inputPrecioEspecial").val()))/100;
          totalGlobal = parseFloat($("#txtTotalTotalGlobal").text()) - ahorro;
        }else{          
          $('#CheckboxPrecioEspecial').prop('checked', false);
          $("#inputPrecioEspecial").prop('disabled', false);
          msgAlert("Favor de escribir el porcentaje.","warning");
        }
      }
      else{
        $("#inputPrecioEspecial").prop('disabled', false);
        $("#inputPrecioEspecial").val('');
        ahorro = 0;
        if($("#CheckboxEnvio").prop("checked")){
          totalGlobal = parseFloat($("#txtAuxTotalTotalGlobal").val()) + 20;
        }
        else{
          totalGlobal = parseFloat($("#txtAuxTotalTotalGlobal").val());
        }
      }
      
      $("#txtTotalTotalGlobal").empty();
      $("#txtTotalTotalGlobal").append(totalGlobal.toString());
    }else{
      $('#CheckboxPrecioEspecial').prop('checked', false);
      msgAlert("Favor de agregar productos.","warning");
    }
  }

  function nuevoCliente(){
    $('#modal_formCliente').modal({
        keyboard: false,
        backdrop: 'static'
    });

    $("#muestra_formSucursal").html('Cargando...');

    $.ajax({
        url: 'cliente/formCliente',
        success: function(datos){

            $("#muestra_formSucursal").html(datos);

        }
    });
  }

  function guardar(){
    var totalGlobal = 0;
    var cantidadGlobal = 0;
    var arrayProductosIndividual = new Array();
    var arrayProductosTodos = [];

    CantidadGlobal = $("#CantidadGlobal").text();
    precio = 1;//menudeo
    if($("#CheckboxMayoreo").prop("checked")){
      precio = 2;//mayoreo
    }
    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);

        TotalProducto = $("#spanTotal_"+myArr[1]).text();
        myArr2 = TotalProducto.split("$");
        totalGlobal = parseFloat(totalGlobal) + parseFloat(myArr2[1]); 
        
        precio_venta =  $("#spanMenudeo_"+myArr[1]).text();
        if(precio == 2){
          precio_venta = $("#spanMayoreo_"+myArr[1]).text();
        }
        var producto = {cvesp_venta:myArr[1], cantidad_venta:parseInt(cantidadProducto), precio_venta:parseFloat(precio_venta)}
        arrayProductosTodos.push(producto);
      }
    });

    var val = $('#CMBCLIENTES').val() ? $('#CMBCLIENTES').val() : '';
    
    var valueCombo = $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbClientesListMod").find("option[value=\""+val+"\"]").data("value") : "";

    var cvecliente = valueCombo.cvecliente ? valueCombo.cvecliente : '';
  
    if (cvecliente == '')
    {
      msgAlert("Favor de ingresar un cliente.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if(cantidadGlobal  == 0){
      msgAlert("Favor de ingresar al menos un producto.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if($('#observaciones_venta').val()  == ''){
      msgAlert("Favor de escribir una observación.","warning");
      scrollHastaArriba(100);
          return;
    }
    else{
      tipoprecio_venta = 1;//menudeo
      if($("#CheckboxMayoreo").prop("checked")){
        tipoprecio_venta = 2;//mayoreo
      }
      precioespecial_venta = '0';//precio especial no
      if($("#CheckboxPrecioEspecial").prop("checked")){
        precioespecial_venta = ((parseFloat($("#txtAuxTotalTotalGlobal").val()) * parseFloat($("#inputPrecioEspecial").val()))/100);//precio especial si
      }
      cobroenvio_venta = '0';
      if(totalGlobal > 0){
        if($("#CheckboxEnvio").prop("checked")){
          cobroenvio_venta = 20;
        }
      }

      var listaProductos = JSON.stringify(arrayProductosTodos);
      $("#btnCobrar").attr("disabled", true);
      $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          var bar = $('.progress-bar');

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              percentComplete = parseInt(percentComplete * 100);
              //console.log(percentComplete);

              var percentVal = percentComplete+'%';
              bar.width(percentVal);

            }
          }, false);
          return xhr;
        },
        url      : 'venta/guardarVenta',
        type     : "POST",
        datatype: 'JSON',
        data    : { 
          listaProductos: listaProductos,
          total_venta: totalGlobal, 
          tipo_venta: 2,
          tipoprecio_venta: tipoprecio_venta,
          cvecliente_venta: cvecliente,
          cobroenvio_venta : cobroenvio_venta,
          observaciones : $('#observaciones_venta').val(),
          precioespecial_venta : precioespecial_venta
        },
        success  : function(datos) {
          var myJson = JSON.parse(datos);
          var cve_venta = myJson["cve_venta"];
          
          $.ajax({
            url      : 'venta/obtenerFolio',
            type     : "POST",
            data    : { 
              ban: 1,
              cve_venta: cve_venta 
            },
            success  : function(datos) {
              var myJson = JSON.parse(datos);

              $(myJson.arrayDatos).each( function(key, val){
                $("#CantidadGlobal").empty();
                $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
                $("#CantidadGlobal").append(" 0 ");

                $("#TotalGlobal").empty();
                $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
                $("#TotalGlobal").append(" $0 ");

                $("#divClienteTexto").empty();
                $("#divClienteTexto").append("Cliente");

                $("#divPedidoTexto").empty();
                $("#divPedidoTexto").append("Pedido");

                $("#divObservacionesTexto").empty();
                $("#divObservacionesTexto").append("Observaciones");

                $("#observaciones_venta").val("");

                $("#txtTotalTotalGlobal").empty();
                $("#txtTotalTotalGlobal").append("0");
                cargarBotonesProducto();
                $('#folio_venta').text(val.folio_venta);
                labeltotalGlobal = val.total_venta;
                if(val.cobroenvio_venta > 0){
                  labeltotalGlobal = parseFloat(val.total_venta) + parseFloat(val.cobroenvio_venta);
                  $('#cobroenvio_venta').text("Si");
                }else{
                  $('#cobroenvio_venta').text("No");
                }
                if(val.precioespecial_venta > 0){
                  labeltotalGlobal = labeltotalGlobal - parseFloat(val.precioespecial_venta);
                  $('#precioespecial_venta').text("Si");
                }else{
                  $('#precioespecial_venta').text("No");
                }
                $('#total_venta').text(labeltotalGlobal);
                if(val.tipoprecio_venta == 1){
                  $('#tipoprecio_venta').text("Menudeo");
                }
                if(val.tipoprecio_venta == 2){
                  $('#tipoprecio_venta').text("Mayoreo");
                }
                $('#nombrecliente_venta').text(val.nombre_cliente);
                

                
                $('#fechaadd_venta').text(val.fechaadd_venta);
                $("#CMBCLIENTES").val("");
                $("#nombrecliente").text("");
                $("#direccioncliente").text("");
                $("#celularcliente").text("");
                $("#comentariocliente").text("");
                $('#divTarjetaCliente').hide();
                cargarBotonesProducto();
                var bar = $('.progress-bar');
					      bar.width('0%');
                $("#btnCobrar").attr("disabled", false);
                $('#modalInfoVenta').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
              });
            }
          });
        }
      });

    }
}

function msgAlert(msg,tipo)
{
    $('#msgAlert').css("display", "block");
    $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
}
</script>
</body>
</html>
