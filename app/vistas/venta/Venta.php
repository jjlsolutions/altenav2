<?php 

  if($_SESSION['cveperfil_usuario'] == 1){
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo NOMBRE_SITIO; ?> | Venta</title>
    <?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
    <style>
      .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
          }
          .custom-control-label::before {
            position: absolute;
            top: 0.34rem;
            left: -1.5rem;
            display: block;
            width: 1.4rem;
            height: 1.4rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
          }
          .custom-control-label::after {
            position: absolute;
            top: 0.50rem;
            left: -1.3rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: 100% / 100% 100% no-repeat;
          }
          .custom-checkbox .custom-control-label::before{
            border-radius: 1.0rem;
          }
        .panel-body .btn:not(.btn-block) { width:45%;}
        .btn-app {
          border-radius: 40px;
          border: none;
          margin: .5rem;
          box-shadow: 0 3px 5px rgba(0,0,0,.4);
        }
    </style>
</head>
<body id="idScrool" class="hold-transition sidebar-mini">
<div class="wrapper">
  
    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
      <div class="row">
          <div class="form-group col-md-12">
              <div id="msgAlert"></div>
          </div>
      </div>
        <div class="row mb-2">
          <div class="col-sm-12">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="home">Home</a></li>
              <li class="breadcrumb-item active">Ventas</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
        <form id="formUsuario" action="venta/guardarProducto" method="post">
          <div class="row">
            <div class="col-12"> 
              <div class="custom-control custom-checkbox float-right"> 
                  <input class="custom-control-input" type="checkbox" id="CheckboxMayoreo" onclick="sacaCantidadGlobal()" >
                  <label for="CheckboxMayoreo" class="custom-control-label">Mayoreo</label>
              </div>
            </div>
          </div>
          <div class="panel-body" id="botones">
              
          </div>
        </form>
        <div class="container-fluid">
          <div class="row">
            <div class="col-10">
              <div class="d-flex flex-row justify-content-end">
                  <span class="mr-2" id="CantidadGlobal" style="font-size: 150%;">
                    <i class="fas fa-circle text-primary"></i> 0
                  </span>

                  <span id="TotalGlobal" style="font-size: 150%;">
                   <i class="fas fa-circle text-warning"></i> $0
                  </span>
                </div>
              </div>
            <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        
        
    </section>
    <!-- /.content -->
    
  </div>
  <!-- /.content-wrapper -->
  
  <footer class="main-footer">
    <div class="clearfix" style="margin-top: 15px;">
      <div id="progress" class="progress">
        <div class="progress-bar progress-bar-success"></div>
      </div>
    </div>
    <hr>
    <button type="button" class="btn btn-block btn-primary btn-lg" id="btnCobrar" onclick="guardar()">COBRAR</button>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<div class="modal fade" id="modal_formCantidad">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="nombre_producto"></h4>
        
      </div>
      <div class="modal-body">
          <input type="hidden" id="cve_producto" name="cve_producto">
          <input type="hidden" id="preciomayoreo_producto" name="preciomayoreo_producto">
          <input type="hidden" id="preciomenudeo_producto" name="preciomenudeo_producto">
          <div class="row">
              <div class="form-group col-md-12">
                  <label>Cantidad*</label>
                  <input type="number" min="0" class="form-control form-control-lg" style="border: 0;box-shadow: none;font-size: 38pt;text-align: center;" id="cantidad_producto" name="cantidad_producto" onkeyup='javascript:this.value=this.value.toUpperCase();sacaTotalProducto(this.value)'>
              </div>
          </div>
          <div class="row">
            <div class="col-6">
            
            </div>
            <div class="col-6">
              <div class="float-right"><label id="txtTotalGlobal">Total*</label></div><br>
            </div>
          </div>
          
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-primary" onclick="pasarSpan()" style="width:40%;">OK</button>
        <button type="button" class="btn btn-warning" onclick="limpiarCantidad()" style="width:40%;">LIMPIAR</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modal_producto">
  <div class="modal-dialog">
    <div class="modal-content">
    <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Stock</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
      <div class="modal-body">
         
          <div class="row">
            <div class="col-12">
              <label>No hay suficiente producto</label>
            </div>
          </div>
          
      </div>
      <div class="modal-footer justify-content-between">
      <button type="button" class="btn btn-primary"  data-dismiss="modal">Cerrar</button>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modalInfoVenta">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información de venta</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-md-6">
              <h7>Folio: </h7>
              <label id="folio_venta"></label>
          </div>
          <div class="form-group col-md-6">
              <h7>Total: </h7>
              <label id="total_venta">Total: </label>
          </div>
          <div class="form-group col-md-6">
            <h7>Tipo: </h7>
            <label id="tipoprecio_venta"></label>
          </div>
          <div class="form-group col-md-6">
            <h7>fecha: </h7>
            <label id="fechaadd_venta"></label>
          </div>
      </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- ./wrapper -->
    <?php 
    include RUTA_APP . 'vistas/includes/script.php';
    ?>
<script type="text/javascript">
var aux_cantidadBolis = 0;
$(document).ready(function () {
    cargarBotonesProducto();

    $('#cantidad_producto').keypress(function(e) {
      if (e.which == 13) {
        pasarSpan();
      } 
    });
    var bar = $('.progress-bar');
		bar.width('0%');
});

function guardar(){
  var totalGlobal = 0;
  var cantidadGlobal = 0;
  var arrayProductosTodos = [];

  CantidadGlobal = parseInt($("#CantidadGlobal").text()) - aux_cantidadBolis;
  
  precio = 1;//menudeo
  if(CantidadGlobal >= 10 || aux_cantidadBolis >= 30 || $("#CheckboxMayoreo").prop("checked")){
    precio = 2;//mayoreo
  }

    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");
      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);

        TotalProducto = $("#spanTotal_"+myArr[1]).text();
        myArr2 = TotalProducto.split("$");
        totalGlobal = parseFloat(totalGlobal) + parseFloat(myArr2[1]); 
        
        precio_venta =  $("#spanMenudeo_"+myArr[1]).text();
        if(precio == 2){
          precio_venta = $("#spanMayoreo_"+myArr[1]).text();
        }
        var producto = {cvesp_venta:myArr[1], cantidad_venta:parseInt(cantidadProducto), precio_venta:parseFloat(precio_venta), cve_producto:parseFloat(cve_producto)}
        arrayProductosTodos.push(producto);
      }
    });

    if (cantidadGlobal  == 0)
    {
        msgAlert("Favor de ingresar al menos un producto.","warning");
        scrollHastaArriba(100);
          return;
    }
    else{
      tipoprecio_venta = 1;//menudeo
      if(precio == 2){
        tipoprecio_venta = 2;//mayoreo
      }

      var listaProductos = JSON.stringify(arrayProductosTodos);
      $("#btnCobrar").attr("disabled", true);
      $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          var bar = $('.progress-bar');

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              percentComplete = parseInt(percentComplete * 100);
              //console.log(percentComplete);

              var percentVal = percentComplete+'%';
              bar.width(percentVal);

            }
          }, false);
          return xhr;
        },
        url      : 'venta/guardarVenta',
        type     : "POST",
        datatype: 'JSON',
        data    : { 
          listaProductos: listaProductos,
          total_venta: totalGlobal, 
          tipo_venta: 1,
          tipoprecio_venta: tipoprecio_venta,
          cvecliente_venta: 0,
          cobroenvio_venta : '0',
          observaciones : '',
          precioespecial_venta : 0
        },
        success  : function(datos) {
          var myJson = JSON.parse(datos);
          var cve_venta = myJson["cve_venta"];
          
          $.ajax({
            url      : 'venta/obtenerFolio',
            type     : "POST",
            data    : { 
              ban: 1,
              cve_venta: cve_venta 
            },
            success  : function(datos) {
              var myJson = JSON.parse(datos);

              $(myJson.arrayDatos).each( function(key, val){
                $("#CantidadGlobal").empty();
                $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
                $("#CantidadGlobal").append(" 0 ");

                $("#TotalGlobal").empty();
                $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
                $("#TotalGlobal").append(" $0 ");
                cargarBotonesProducto();
                $('#folio_venta').text(val.folio_venta);
                $('#total_venta').text(val.total_venta);
                if(val.tipoprecio_venta == 1){
                  $('#tipoprecio_venta').text("Menudeo");
                }
                if(val.tipoprecio_venta == 2){
                  $('#tipoprecio_venta').text("Mayoreo");
                }
                
                $('#fechaadd_venta').text(val.fechaadd_venta);
                
                var bar = $('.progress-bar');
					      bar.width('0%');
                $("#btnCobrar").attr("disabled", false);
                $('#modalInfoVenta').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
              });
              
              $("#CheckboxMayoreo").prop('checked',false);
              aux_cantidadBolis = 0;
            }
          });
        }
      });

    }
}

function limpiarCantidad(){
  $("#cantidad_producto").val("");
  $('#txtTotalGlobal').text("Total : $0");
  $('#cantidad_producto').focus();
}
function cargarBotonesProducto(){
    $.ajax({
        url      : 'Producto/consultar',
        type     : "POST",
        data    : { 
            ban: 3
        },
        success  : function(datos) {

            var myJson = JSON.parse(datos);

            botones = $("#botones");
            $("#botones").empty();

            if(myJson.arrayDatos.length > 0)
            {
                $(myJson.arrayDatos).each( function(key, val)
                {
                  if(val.stock > 0){
                    botones.append('<a class="btn btn-app bg-success" id="producto_' + val.cve_producto + '" onClick="cantidad(' + val.cve_producto + ',\'' + val.nombre_producto + '\',' + val.preciomayoreo_producto + ',' + val.preciomenudeo_producto + ')"><b>' + val.nombre_producto + '</b></a>');
                  }
                    
                });
            }
        }
    });
}

function cantidad(cve_producto, nombre_producto, preciomayoreo_producto, preciomenudeo_producto){
    $('#cve_producto').val(cve_producto);
    $('#preciomenudeo_producto').val(preciomenudeo_producto);
    $('#preciomayoreo_producto').val(preciomayoreo_producto);
    $('#txtTotalGlobal').text('Total : $0');
    $('#cantidad_producto').val("");
    $('#nombre_producto').text(nombre_producto);
    
    $('#modal_formCantidad').modal({
        keyboard: false,
        backdrop: 'static'
    });
    
    $('#modal_formCantidad').on('shown.bs.modal', function () {
      $('#cantidad_producto').focus();
    });
}

function sacaTotalProducto(valor){
  var totalProCuenta = 0; 
  var cve_producto = $('#cve_producto').val();
  var preciomenudeo_producto = $('#preciomenudeo_producto').val();
  var preciomayoreo_producto = $('#preciomayoreo_producto').val();
  var nombre_producto = $('#nombre_producto').text();

  cantidad_producto = $("#cantidad_producto").val();
  CantidadGlobal = $("#CantidadGlobal").text();
  spanCantidad = $('#spanCantidad_'+cve_producto).text();

  if(spanCantidad == ''){
    spanCantidad = 0;
  }
  if(nombre_producto == 'BOLIS SELLADOS' || nombre_producto == 'BOLI SELLADO'){
    cantidadTotal = (parseInt(CantidadGlobal) - spanCantidad);
    precio = preciomenudeo_producto;
    if(cantidadTotal >= 10 || cantidad_producto >= 30 || $("#CheckboxMayoreo").prop("checked")){
      precio = preciomayoreo_producto;
    }
  }else{
    cantidadTotal = ((parseInt(CantidadGlobal)- aux_cantidadBolis) + parseInt(cantidad_producto)) - spanCantidad;
    
    precio = preciomenudeo_producto;
    if(cantidadTotal >= 10 || aux_cantidadBolis >= 30 || $("#CheckboxMayoreo").prop("checked")){
      precio = preciomayoreo_producto;
    }
  }
  
  totalProCuenta = precio * valor;
  $('#txtTotalGlobal').text("Total : $"+totalProCuenta);
  
}

function pasarSpan(){

  if($("#cantidad_producto").val()){
    $.ajax({
        url      : 'Producto/consultarStock',
        type     : "POST",
        data    : { 
            cve_producto : $('#cve_producto').val()
        },
        success  : function(datos) {

            var myJsonStock = JSON.parse(datos);

          if(myJsonStock.stock > 0 &&  parseInt($("#cantidad_producto").val()) <= myJsonStock.stock ){
            var total = 0;
            cve_producto = $('#cve_producto').val();
            preciomenudeo_producto = $('#preciomenudeo_producto').val();
            preciomayoreo_producto = $('#preciomayoreo_producto').val();
            nombre_producto = $('#nombre_producto').text();
            
            cantidad_producto = $("#cantidad_producto").val();
            CantidadGlobal = $("#CantidadGlobal").val();
            spanCantidad = $('#spanCantidad_'+cve_producto).text();

            if(spanCantidad == ''){
              spanCantidad = 0;
            }
            if(nombre_producto == 'BOLIS SELLADOS' || nombre_producto == 'BOLI SELLADO'){
              if(cantidad_producto == ''){
                cantidad_producto = 0;
              }
              cantidadTotal = (CantidadGlobal - spanCantidad);

              precio = preciomenudeo_producto;
              if(cantidadTotal >= 10 || cantidad_producto >= 30 || $("#CheckboxMayoreo").prop("checked")){
                precio = preciomayoreo_producto;
              }
              aux_cantidadBolis = cantidad_producto;
            }else{
              if(cantidad_producto == ''){
                cantidad_producto = 0;
              }
              cantidadTotal = ((CantidadGlobal - aux_cantidadBolis) + cantidad_producto) - spanCantidad;
            
              precio = preciomenudeo_producto;
              if(cantidadTotal >= 10 || $("#CheckboxMayoreo").prop("checked")){
                precio = preciomayoreo_producto;
              }
            }
            
            total = parseFloat(cantidad_producto) * parseFloat(precio);
            if(total == 0){
              $("#producto_"+cve_producto).find("span").remove();//remove span elements    
            }
            else{
              $("#producto_"+cve_producto).find("span").remove();//remove span elements    
              $("#producto_"+cve_producto).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+cve_producto+'"><b>'+cantidad_producto+'</b></span>');
              $("#producto_"+cve_producto).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+cve_producto+'"><b>$'+total+'</b></span>');

              $("#producto_"+cve_producto).append('<span id="spanMenudeo_'+cve_producto+'">'+preciomenudeo_producto+'</span>');
              $("#producto_"+cve_producto).append('<span id="spanMayoreo_'+cve_producto+'">'+preciomayoreo_producto+'</span>');
              $("#spanMenudeo_"+cve_producto).hide();
              $("#spanMayoreo_"+cve_producto).hide();

            }
            
            sacaCantidadGlobal();
            $("#cantidad_producto").val("");
            $('#modal_formCantidad').modal('hide');
          }else{
            $('#modal_producto').modal('show');
          }
        }
    });
  }else{
    $("#cantidad_producto").val("");
    $('#modal_formCantidad').modal('hide');
  }
  
    
}

function consultarStock(){
  $.ajax({
      url      : 'Producto/consultar',
      type     : "POST",
      data    : { 
          ban: 3
      },
      success  : function(datos) {

          var myJson = JSON.parse(datos);

          botones = $("#botones");
          $("#botones").empty();

          if(myJson.arrayDatos.length > 0)
          {
              $(myJson.arrayDatos).each( function(key, val)
              {
                if(val.stock > 0){
                  botones.append('<a class="btn btn-app bg-success" id="producto_' + val.cve_producto + '" onClick="cantidad(' + val.cve_producto + ',\'' + val.nombre_producto + '\',' + val.preciomayoreo_producto + ',' + val.preciomenudeo_producto + ')"><b>' + val.nombre_producto + '</b></a>');
                }
                  
              });
          }
      }
  });
}

function sacaCantidadGlobal(){
  var cantidadGlobal = 0;
    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);
      }
    });
    $("#CantidadGlobal").empty();

    $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
    $("#CantidadGlobal").append(" "+cantidadGlobal.toString()+" ");

    recalculaTotalGlobal();
    
}

function recalculaTotalGlobal(){
  var totalGlobal = 0;
  var cantidadGlobal = 0;
  $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        spanMayoreo = $("#spanMayoreo_"+myArr[1]).text();
        spanMenudeo = $("#spanMenudeo_"+myArr[1]).text();
        CantidadGlobal = $("#CantidadGlobal").text();
        if($("#producto_"+myArr[1]).val() == 'BOLIS SELLADOS' || $("#producto_"+myArr[1]).val() == 'BOLI SELLADO'){
          aux_cantidadBolis = cantidad_producto;
        }
      }
    });
    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        spanMayoreo = $("#spanMayoreo_"+myArr[1]).text();
        spanMenudeo = $("#spanMenudeo_"+myArr[1]).text();
        CantidadGlobal = $("#CantidadGlobal").text();
        
        precio = spanMenudeo;
        if((CantidadGlobal - aux_cantidadBolis) >= 10 || aux_cantidadBolis >= 30 || $("#CheckboxMayoreo").prop("checked")){
          precio = spanMayoreo;
        }
        total = parseFloat(cantidadProducto) * parseFloat(precio);
     
        $("#producto_"+myArr[1]).find("span").remove();//remove span elements    
        $("#producto_"+myArr[1]).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+myArr[1]+'"><b>'+cantidadProducto+'</b></span>');
        $("#producto_"+myArr[1]).append('<span class="badge bg-warning" style="font-size: 130%; margin-left: 100%;margin-top: 40px;" id="spanTotal_'+myArr[1]+'"><b>$'+total+'</b></span>');
        
        $("#producto_"+myArr[1]).append('<span id="spanMenudeo_'+myArr[1]+'">'+spanMenudeo+'</span>');
        $("#producto_"+myArr[1]).append('<span id="spanMayoreo_'+myArr[1]+'">'+spanMayoreo+'</span>');
        $("#spanMenudeo_"+myArr[1]).hide();
        $("#spanMayoreo_"+myArr[1]).hide();

        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);
      }

      TotalProducto = $("#spanTotal_"+myArr[1]).text();
      myArr2 = TotalProducto.split("$");
      if(myArr2[1] >= 0){
        totalGlobal = parseFloat(totalGlobal) + parseFloat(myArr2[1]); 
      }
    });
    $("#CantidadGlobal").empty();

    $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
    $("#CantidadGlobal").append(" "+cantidadGlobal.toString()+" ");

    $("#TotalGlobal").empty();
     
     $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
    $("#TotalGlobal").append(" $"+totalGlobal.toString()+" ");
    
}

function msgAlert(msg,tipo)
{
    $('#msgAlert').css("display", "block");
    $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
}

</script>
</body>
</html>
