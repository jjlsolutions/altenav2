<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo NOMBRE_SITIO; ?> | Incidente</title>
    <?php 
        include RUTA_APP . 'vistas/includes/link.php';
    ?>
    <style>
      .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
          }
          .custom-control-label::before {
            position: absolute;
            top: 0.34rem;
            left: -1.5rem;
            display: block;
            width: 1.4rem;
            height: 1.4rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
          }
          .custom-control-label::after {
            position: absolute;
            top: 0.50rem;
            left: -1.3rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: 100% / 100% 100% no-repeat;
          }
          .custom-checkbox .custom-control-label::before{
            border-radius: 1.0rem;
          }
        .panel-body .btn:not(.btn-block) { width:45%;}
        .btn-app {
          border-radius: 40px;
          border: none;
          margin: .5rem;
          box-shadow: 0 3px 5px rgba(0,0,0,.4);
        }
    </style>
</head>
<body id="idScrool" class="hold-transition sidebar-mini">
  <div class="wrapper">
    
      <?php 
      include RUTA_APP . 'vistas/includes/header.php';

      include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
      ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section  class="content-header">
        <div class="container-fluid">
          <div class="row">
            <div class="form-group col-md-12">
                <div id="msgAlert"></div>
            </div>
          </div>
          <div class="row mb-2">
            <div class="col-sm-12">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="home">Home</a></li>
                <li class="breadcrumb-item active">Incidente</li>
              </ol>
            </div>
          </div>
        </div><!-- /.container-fluid -->
      </section>
      <section class="content"> 
      <div id="vendedorAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" style="color: black;" data-toggle="collapse" href="#divVendedor" aria-expanded="false">
              <div id="divVendedorTexto">Vendedor</div> 
              </a>
            </h4>
          </div>
          <div id="divVendedor" class="collapse" data-parent="#vendedorAcordeon">
            <div class="card-body">
              <div class="row">
              <label for="CMBVENDEDOR">Buscar vendedor</label>
                <datalist id="cmbVendedorListMod">
                  <!--option value="0" selected="selected"> -- Seleccione -- </option-->
                </datalist>
                <div class="input-group input-group-sm">
                  <input list="cmbVendedorListMod" onclick="limpiarVendedor()" id="CMBVENDEDOR" name="CMBVENDEDOR" type="text" class="form-control" placeholder=" -- Escriba -- " onkeyup="javascript:this.value=this.value.toUpperCase();" onchange="seleccionVendedor(event);" autocomplete="off">
                </div>
              </div>
              <br>
              <div class="callout callout-info" id="divTarjetaVendedor" style="display:none;">
              
                <div class="row invoice-info">
                  <div class="col-sm-4 invoice-col">
                    <address>
                    <label><strong> Vendedor: </strong></label>
                    <h6 id="nombreCompleto"></h6>
                    <label><strong> Sucursal: </strong></label>
                    <h6 id="nombresucursal"> </h6>
                    </address>
                  </div>
                  <!-- /.col -->
                  <div class="col-sm-4 invoice-col">
                    <address>
                      <label><strong> Perfil: </strong></label>
                      <h6 id="nombreperfil"> </h6>
                      <label><strong> Login: </strong></label>
                      <h6 id="loginusuario"> </h6>
                    </address>
                  </div>
                  <!-- /.col -->
                  
                  <!-- /.col -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
      <section class="content"> 
      <div id="pedidoAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" data-toggle="collapse" style="color: black;" href="#divProducto" aria-expanded="false">
              <div id="divProductoTexto">Producto</div>
              </a>
            </h4>
          </div>
          <div id="divProducto" class="collapse" data-parent="#pedidoAcordeon" style="">
            <div class="card-body">
              <hr>
              <form id="formUsuario" action="venta/guardarProducto" method="post">
                <div class="row">
                  <div class="col-12"> 
                    <div class="custom-control custom-checkbox float-right"> 
                        <input class="custom-control-input" type="checkbox" id="CheckboxMayoreo" onclick="sacaCantidadGlobal()" >
                        <label for="CheckboxMayoreo" class="custom-control-label">Mayoreo</label>
                    </div>
                  </div>
                </div>
                <div class="panel-body" id="botones">
                    
                </div>
              </form>
            </div>
        </div>
      </div>
      </section>
      <section class="content"> 
      <div id="observacionesAcordeon">
        <div class="card card-default">
          <div class="card-header">
            <h4 class="card-title w-100">
              <a class="d-block w-100 collapsed" data-toggle="collapse" style="color: black;" href="#divobservaciones" aria-expanded="false">
              <div id="divObservacionesTexto">Observaciones</div>
              </a>
            </h4>
          </div>
          <div id="divobservaciones" class="collapse" data-parent="#observacionesAcordeon" style="">
          <div class="card-body">
              <div class="form-group">
                <textarea class="form-control" rows="3" id="observaciones_venta" placeholder="Escribe las observaciones ..." onkeyup="ObservacionesCheck()"></textarea>
              </div>
              <div id="divAdjunto">
                    <div class="form-group col-md-12">
                    <label for="txtArchivo">Documento digitalizado<span id="asttxtArchivo">*</span>:</label>
                            <input class="ns_" style="margin-bottom: 15px;float:left; margin:10px 0 0 0; word-wrap: break-word; width:100%;" id="txtArchivo" name="txtArchivo" type="file" accept=".pdf,.jpg,.jpeg,.gif,.png,.PDF,.JPG,.JPEG,.GIF,.PNG,.doc,.docx" onchange="checkfile(this,'.pdf,.jpg,.jpeg,.gif,.png,.PDF,.JPG,.JPEG,.GIF,.PNG,.doc,.docx')">
                            <div hidden='true' class='erroresTxt' id='errortxtArchivo'>Este campo es obligatorio.</div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      </section>


      <div class="card-header">
      <button type="button" class="btn btn-primary" id="btnCobrar" onclick="guardar()">Registrar incidente</button>
          
      </div>
      
      <!-- Main content -->
      <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridGasto" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre vendedor&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Observaciones&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Sucursal&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Ver</th>
                                <th>Eliminar</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

      <!-- Main content -->
      <!-- /.content -->
      
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
    <div class="clearfix">
        <div class="progress">
          <div class="progress-bar bg-primary progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">
            
          </div>
        </div>
      </div>
      <hr>
      <div class="row">
          <div class="col-2">
              <a id="back-to-top" href="#" class="btn btn-outline-primary back-to-top" role="button" aria-label="Scroll to top">
              <i class="fas fa-chevron-up"></i>
              </a>
          </div>    
      </div>
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
  </div>
  <div class="modal fade" id="modal_formCantidad">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" id="nombre_producto"></h4>
          
        </div>
        <div class="modal-body">
            <input type="hidden" id="cve_producto" name="cve_producto">
            <input type="hidden" id="preciomayoreo_producto" name="preciomayoreo_producto">
            <input type="hidden" id="preciomenudeo_producto" name="preciomenudeo_producto">
            <div class="row">
                <div class="form-group col-md-12">
                    <label>Cantidad*</label>
                    <input type="number" min="0" class="form-control form-control-lg" style="border: 0;box-shadow: none;font-size: 38pt;text-align: center;" id="cantidad_producto" name="cantidad_producto" onkeyup='javascript:this.value=this.value.toUpperCase();'>
                </div>
            </div>
            
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-primary" onclick="pasarSpan()" style="width:40%;">OK</button>
          <button type="button" class="btn btn-warning" onclick="limpiarCantidad()" style="width:40%;">LIMPIAR</button>
        </div>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <div class="modal fade" id="modal_formGasto">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Detalle</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <div class="form-group col-md-12">
                    <label>Observaciones*</label>
                    <textarea class="form-control" id="observaciones_incidente" name="observaciones_incidente" onkeyup='javascript:this.value=this.value.toUpperCase();'></textarea>
                </div>
        <div class="card-body">
          <table id="gridGastoModal" class="table table-bordered table-hover">
              <thead>
                  <tr>
                      <th>Nombre producto&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                      <th>Cantidad&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                  </tr>
              </thead>
          </table>
        </div>
          <div id="divImagen">
              
                  <div id="portapdfModal" style="height: 400px;"></div>
              
          </div>
        </div>
        </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalInfoVenta">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Información de incidente</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-md-6">
              <h7>Se registro el incidente </h7>
              <label id="folio_venta"></label>
          </div>
      </div>
      </div>
    </div>
    <!-- /.modal-content -->
  </div>
  <!-- /.modal-dialog -->
</div>
  <!-- ./wrapper -->
      <?php 
      include RUTA_APP . 'vistas/includes/script.php';
      ?>
<script type="text/javascript">
var aux_cantidadBolis = 0;
  $(document).ready(function () {
    if(cveperfil == 1){
      $("#btnCobrar").attr("disabled", true);
    }
    tableGasto = $('#gridGasto').DataTable( {    
      "responsive": true,
      "searching" : true,
      "paging"    : true,
      "ordering"  : false,
      "info"      : true,
      "autoWidth": false,
      "columnDefs": [
          {"width": "10%","className": "text-center","targets": 3},
          {"width": "10%","className": "text-center","targets": 4},
      ],

      "bJQueryUI":true,"oLanguage": {
          "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
          "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
          "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
          "sInfoPostFix":    "",
          "sInfoThousands":  ",",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sLoadingRecords": "Cargando...",
          "sProcessing":     "Procesando...",
          "sSearch":         "Buscar:",
          "sZeroRecords":    "No se encontraron resultados",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": activar para Ordenar Ascendentemente",
              "sSortDescending": ": activar para Ordendar Descendentemente"
          }
      }
  });
  tableGastoModal = $('#gridGastoModal').DataTable( {    
    "responsive": true,
    "searching" : true,
    "paging"    : true,
    "ordering"  : false,
    "info"      : true,
    "autoWidth": false,
    "columnDefs": [
        {"width": "10%","className": "text-center","targets": 1}
    ],

    "bJQueryUI":true,"oLanguage": {
        "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
        "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
        "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
        "sInfoPostFix":    "",
        "sInfoThousands":  ",",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sLoadingRecords": "Cargando...",
        "sProcessing":     "Procesando...",
        "sSearch":         "Buscar:",
        "sZeroRecords":    "No se encontraron resultados",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": activar para Ordenar Ascendentemente",
            "sSortDescending": ": activar para Ordendar Descendentemente"
        }
    }
});
  cargarTablaGasto();
    cargarTablaVendedor();
    cargarBotonesProducto();

    $('#cantidad_sabor').keypress(function(e) {
      if (e.which == 13) {
        pasarSpan();
      } 
    });

    var bar = $('.progress-bar');
		bar.width('0%');
  });

  function cargarTablaGasto()
    {
        $.ajax({
            url      : 'Incidente/consultar',
            type     : "POST",
            data    : { 
                ban: 1 
            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {
   
                var myJson = JSON.parse(datos);

                tableGasto.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {
                      cancelar = '';
                        if (parseInt(val.estatus_incidente) == 1)
                        {
                            title = '&nbsp;&nbsp;&nbsp;Aprobar&nbsp;&nbsp;&nbsp;';
                            icon = 'badge badge-primary';
                            accion = "";
                            if(cveperfil == 1 || cveperfil == 2){
                              title = '&nbsp;&nbsp;&nbsp;Aprobar&nbsp;&nbsp;&nbsp;';
                              icon = 'badge badge-primary';
                              accion = "AprobarIncidente(2,'" + val.cve_incidente + "','" + 2 + "')";
                            }
                            titleC = 'Eliminar';
                            iconC = 'badge badge-danger';
                            accionC = "AprobarIncidente(3,'" + val.cve_incidente + "','" + 3 + "')";
                            cancelar = "<br><span class='"+iconC+"' style=\"cursor: pointer;font-size:14px;\" onclick=\"" + accionC + "\">"+titleC+"</span>"
                        }
                        else if(parseInt(val.estatus_incidente) == 2)
                        {
                            title = 'Aprobado';
                            icon = 'badge badge-success';
                            accion = "";
                            if(cveperfil == 1 || cveperfil == 2){
                                title = 'Aprobado';
                                icon = 'badge badge-success';
                                accion = "";
                            }
                            
                        }

                        var btn_editar = "<i class='fa fa-eye' style='font-size:18px; cursor: pointer;' title='Editar gasto' onclick=\"mostrarGasto('" + val.cve_incidente  + "')\"></i>";
                        //var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";
                        var btn_status = "<span class='"+icon+"' style=\"cursor: pointer;font-size:14px;margin-bottom: 5px;\" onclick=\"" + accion + "\">"+title+"</span>"+cancelar;
                        tableGasto.row.add([
                            val.nombre_usuario ,
                            val.observaciones_incidente,
                            val.nombre_sucursal,
                            btn_editar,
                            btn_status,
                        ]).draw();
                    })

                }
                else
                {
                    tableGasto = $('#gridGasto').DataTable();
                    
                }

            }
        });
    }
  
  function mostrarGasto(cve_incidente)
  {
      $('#msgAlert').css("display", "none");
      
      $.ajax({
          url      : 'incidente/consultar',
          type     : "POST",
          data     : { 
                  ban: 4, 
                  cve_incidente : cve_incidente  
          },
          beforeSend: function() {
              // setting a timeout
          },
          success  : function(datos) {
              var myJson = JSON.parse(datos);
              //console.log(myJson);
              var ruta_archivo = 'public/img/incidente/'+myJson.arrayDatos[0].imagen_incidente;
              $("#portapdfModal").html('<object data="'+ruta_archivo+'" type="" width="100%" height="100%"></object>');
              $('#observaciones_incidente').val(myJson.arrayDatos[0].observaciones_incidente );
                tableGastoModal.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {
                      tableGastoModal.row.add([
                            val.nombre_producto ,
                            val.cantidad_deincidente
                        ]).draw();
                    })

                }
                else
                {
                    tableGasto = $('#gridGastoModal').DataTable();
                    
                }

              $('#modal_formGasto').modal({
                  keyboard: false
              });
              

          }
      });
  }
  function cargarBotonesProducto(){
    $.ajax({
        url      : 'Producto/consultar',
        type     : "POST",
        data    : { 
            ban: 3
        },
        success  : function(datos) {

            var myJson = JSON.parse(datos);

            botones = $("#botones");
            $("#botones").empty();

            if(myJson.arrayDatos.length > 0)
            {
                $(myJson.arrayDatos).each( function(key, val)
                {
                  if(val.stock > 0){
                    botones.append('<a class="btn btn-app bg-success" id="producto_' + val.cve_producto + '" onClick="cantidad(' + val.cve_producto + ',\'' + val.nombre_producto + '\',' + val.preciomayoreo_producto + ',' + val.preciomenudeo_producto + ')"><b>' + val.nombre_producto + '</b></a>');
                  }
                });
            }
        }
    });
}

  function cantidad(cve_producto, nombre_producto, preciomayoreo_producto, preciomenudeo_producto){
    $('#cve_producto').val(cve_producto);
    $('#preciomenudeo_producto').val(preciomenudeo_producto);
    $('#preciomayoreo_producto').val(preciomayoreo_producto);
    $('#cantidad_producto').val("");
    $('#nombre_producto').text(nombre_producto);
    
    $('#modal_formCantidad').modal({
        keyboard: false,
        backdrop: 'static'
    });
    
    $('#modal_formCantidad').on('shown.bs.modal', function () {
      $('#cantidad_producto').focus();
    });
}

function pasarSpan(){

  $.ajax({
      url      : 'Producto/consultarStock',
      type     : "POST",
      data    : { 
          cve_producto : $('#cve_producto').val()
      },
      success  : function(datos) {

          var myJsonStock = JSON.parse(datos);

        if(myJsonStock.stock > 0 &&  parseInt($("#cantidad_producto").val()) <= myJsonStock.stock ){
          var total = 0;
          cve_producto = $('#cve_producto').val();
          
          cantidad_producto = $("#cantidad_producto").val();
          spanCantidad = $('#spanCantidad_'+cve_producto).text();

          if(spanCantidad == ''){
            spanCantidad = 0;
          }
            if(cantidad_producto == ''){
              cantidad_producto = 0;
            }
            
            
            $("#producto_"+cve_producto).find("span").remove();//remove span elements    
            $("#producto_"+cve_producto).append('<span class="badge bg-primary" style="font-size: 130%; margin-right: 95%;" id="spanCantidad_'+cve_producto+'"><b>'+cantidad_producto+'</b></span>');
            
            
          $("#cantidad_producto").val("");
          $('#modal_formCantidad').modal('hide');
        }else{
          alert("No hay sufiente producto.");
        }
      }
  });
    
    
}


  function ObservacionesCheck(){
    nombreObservacionesTexto = $("#divObservacionesTexto").text();
    var Observaciones = $("#observaciones_venta").val();
    if(Observaciones == ""){

      divObservacionesTexto = $("#divObservacionesTexto");
      $("#divObservacionesTexto").empty();
      divObservacionesTexto.append(nombreObservacionesTexto);
    }
    else{
      divObservacionesTexto = $("#divObservacionesTexto");
      $("#divObservacionesTexto").empty();
      divObservacionesTexto.append(nombreObservacionesTexto+' <i class="fas fa-check"></i>');

    }

  }

  function cargarTablaVendedor()
  {
    $.ajax({
          url      : 'Usuario/consultar',
          type     : "POST",
          data    : { 
              ban: 1
          },
          success  : function(datos) {

              var myJson = JSON.parse(datos);

              if(myJson.arrayDatos.length > 0)
              {
                var select2 = $("#cmbVendedorListMod");
                select2.find('option').remove();
                
                $(myJson.arrayDatos).each( function(key, val)
                {
                  value = '{"cveusuario":"'+val.cve_usuario+'","nombreCompleto":"'+val.nombreCompleto.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'","nombresucursal":"'+val.nombre_sucursal+'","loginusuario":"'+val.login_usuario+'","nombreperfil":"'+val.nombre_perfil.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+'"}';
                  select2.append("<option data-value='"+value+"' value='"+val.nombreCompleto.replace(/"/g, "\\&#x22;").replace(/'/g, "&#x27;")+"'>");
                });

                $('#CMBVENDEDOR').val("-- Escriba --");

              }

              

          }
      });
  }

  function AprobarIncidente(ban, cve_incidente, estatus_incidente)
  {
    
    
      //2 = aprobar, 3 = eliminar
      if(ban == 2){
        mensaje = '<b>¿Desea aprobar la incidencia?</b><br><p</p>';
      }
      if(ban == 3){
        mensaje = '<b>¿Desea eliminar la incidencia?</b><br><p</p>';
      }
      
    bootbox.dialog({ 
                message: mensaje,
                size: 'large',
                buttons: {
                    No: {
                        label: 'No',
                        className: 'btn-danger',
                        callback: function(){
                            
                        }
                    },
                    Si: {
                        label: 'Si',
                        className: 'btn-primary',
                        callback: function(){
                          $.ajax({
                            url      : 'incidente/consultar',
                            type     : "POST",
                            data     : { 
                                    ban: ban, 
                                    cve_incidente: cve_incidente,
                                    estatus_incidente: estatus_incidente
                            },
                            success  : function(datos) {
                              cargarTablaGasto();
                            }
                        });              
                        }
                    }
                }
            });

      
  }
  
  function limpiarVendedor(){
    $("#CMBVENDEDOR").val("");
    $("#nombreCompleto").text("");
    $("#nombresucursal").text("");
    $("#loginusuario").text("");
    $("#nombreperfil").text("");
    $('#divTarjetaVendedor').hide();

    nombreVendedorTexto = $("#divVendedorTexto").text();

          divVendedorTexto = $("#divVendedorTexto");
          $("#divVendedorTexto").empty();
          divVendedorTexto.append(nombreVendedorTexto);
  }
  function seleccionVendedor(){
    var val = $('#CMBVENDEDOR').val() ? $('#CMBVENDEDOR').val() : '';
    if(val != '')
    {
      if(val.indexOf("\"") !== -1){
        var valueCombo = $("#cmbVendedorListMod").find("option[value='"+val+"']").data("value") ? $("#cmbVendedorListMod").find("option[value='"+val+"']").data("value") : "";
      }
      else{
      var valueCombo = $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") : "";
      }

      var cveusuario = valueCombo.cveusuario ? valueCombo.cveusuario : '';
      var nombreCompleto = valueCombo.nombreCompleto ? valueCombo.nombreCompleto : '';
      var nombresucursal = valueCombo.nombresucursal ? valueCombo.nombresucursal : '';
      var loginusuario = valueCombo.loginusuario ? valueCombo.loginusuario : '';
      var nombreperfil = valueCombo.nombreperfil ? valueCombo.nombreperfil : '';
      
      var bool = false;

        if(cveusuario > 0){
          
          $("#nombreCompleto").text(nombreCompleto);

          $("#nombresucursal").text(nombresucursal);

          $("#loginusuario").text(loginusuario);

          $("#nombreperfil").text(nombreperfil);
          $('#divTarjetaVendedor').show();

          nombreVendedorTexto = $("#divVendedorTexto").text();

          divVendedorTexto = $("#divVendedorTexto");
          $("#divVendedorTexto").empty();
          divVendedorTexto.append(nombreVendedorTexto+' <i class="fas fa-check"></i>');
        }
        else{
          
          $("#CMBVENDEDOR").val("");
          $("#nombreCompleto").text("");
          $("#nombresucursal").text("");
          $("#loginusuario").text("");
          $("#nombreperfil").text("");
          $('#divTarjetaVendedor').hide();

          nombreVendedorTexto = $("#divVendedorTexto").text();

          divVendedorTexto = $("#divVendedorTexto");
          $("#divVendedorTexto").empty();
          divVendedorTexto.append(nombreVendedorTexto);

        }

      

    }
    else{
      alert("Favor de ingresar el nombre del contacto.");
      
    }
  }



  function guardar(){
    var cantidadGlobal = 0;
    var arrayProductosTodos = [];

    
    $("#formUsuario").find('a').each(function() {
      var elemento= this;
      myArr = elemento.id.split("_");

      cantidadProducto = $("#spanCantidad_"+myArr[1]).text();
      if(parseInt(cantidadProducto) >= 0){
        cantidadGlobal = parseInt(cantidadGlobal) + parseInt(cantidadProducto);

        var producto = {cvesp_venta:myArr[1], cantidad_venta:parseInt(cantidadProducto)}
        arrayProductosTodos.push(producto);
      }
    });

    var val = $('#CMBVENDEDOR').val() ? $('#CMBVENDEDOR').val() : '';

    var valueCombo = $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") ? $("#cmbVendedorListMod").find("option[value=\""+val+"\"]").data("value") : "";

    var cveusuario = valueCombo.cveusuario ? valueCombo.cveusuario : '';

    if (cveusuario == '')
    {
      msgAlert("Favor de ingresar un vendedor.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if(cantidadGlobal  == 0){
      msgAlert("Favor de ingresar al menos un producto.","warning");
      scrollHastaArriba(100);
          return;
    }
    else if($('#observaciones_venta').val()  == ''){
      msgAlert("Favor de escribir una observación.","warning");
      scrollHastaArriba(100);
          return;
    }
    else{
    

      var listaProductos = JSON.stringify(arrayProductosTodos);
      $("#btnCobrar").attr("disabled", true);

      var file_data = $('#txtArchivo').prop('files')[0];

      var form_data = new FormData();
      form_data.append('file', file_data);
      form_data.append('listaProductos', listaProductos);
      form_data.append('cvecliente_venta', cveusuario);
      form_data.append('observaciones', $('#observaciones_venta').val());
      
      $.ajax({
        xhr: function() {
          var xhr = new window.XMLHttpRequest();
          var bar = $('.progress-bar');

          xhr.upload.addEventListener("progress", function(evt) {
            if (evt.lengthComputable) {
              var percentComplete = evt.loaded / evt.total;
              percentComplete = parseInt(percentComplete * 100);
              //console.log(percentComplete);

              var percentVal = percentComplete+'%';
              bar.width(percentVal);

            }
          }, false);
          return xhr;
        },
        url      : 'incidente/guardarIncidente',
        type	: "POST",
        enctype	: 'multipart/form-data',
        data	: form_data,
        processData: false,
        contentType: false,
        success  : function(datos) {
          var myJson = JSON.parse(datos);
          //var cve_venta = myJson["cve_venta"];
          
          $("#CantidadGlobal").empty();
          $("#CantidadGlobal").append('<i class="fas fa-circle text-primary"></i>');
          $("#CantidadGlobal").append(" 0 ");

          $("#TotalGlobal").empty();
          $("#TotalGlobal").append('<i class="fas fa-circle text-warning"></i>');
          $("#TotalGlobal").append(" $0 ");

          $("#divVendedorTexto").empty();
          $("#divVendedorTexto").append("Vendedor");

          $("#divProductoTexto").empty();
          $("#divProductoTexto").append("Producto");

          $("#divObservacionesTexto").empty();
          $("#divObservacionesTexto").append("Observaciones");

          $("#observaciones_venta").val("");

          $("#txtTotalTotalGlobal").empty();
          $("#txtTotalTotalGlobal").append("0");
          cargarBotonesProducto();
          var bar = $('.progress-bar');
          bar.width('0%');
          $("#CMBVENDEDOR").val("");
          $("#nombreCompleto").text("");
          $("#nombresucursal").text("");
          $("#loginusuario").text("");
          $("#nombreperfil").text("");
          $('#divTarjetaVendedor').hide();
          $("#txtArchivo").val("");

          $('#modalInfoVenta').modal({
              keyboard: false,
              backdrop: 'static'
          });
          $("#btnCobrar").attr("disabled", false);
          cargarTablaGasto();
        }
      });

    }
}
function limpiarCantidad(){
  $("#cantidad_producto").val("");
  $('#txtTotalGlobal').text("Total : $0");
  $('#cantidad_producto').focus();
}

function checkfile(sender,parametros)
    {
        var validExts = parametros.split(",");
        var fileExt = sender.value;
        fileExt = fileExt.substring(fileExt.lastIndexOf('.'));

        if (validExts.indexOf(fileExt) < 0) {
            $('#msjError').html("<div class=\"alert alert-danger\" id=\"divAlertSuccess\">Favor de seleccionar un archivo de alguna de las siguientes extensiones <strong>"+parametros+"</strong></div>");
            $('#ventanaModal').modal('show');
            sender.value="";
            }
    }

function msgAlert(msg,tipo)
{
    $('#msgAlert').css("display", "block");
    $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    setTimeout(function() { $("#msgAlert").fadeOut(1500); },1500);
}
</script>
</body>
</html>
