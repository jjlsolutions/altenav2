<?php
//print_r($datos);
?>
<div id="msgAlert2"></div>

<form id="formCliente" action="cliente/guardarCliente" method="post">

    <div class="box-body">
        <div class="row">

            <div class="form-group col-md-6">
                <label>Nombre del cliente*</label>
                <input type="text" class="form-control" id="nombre_cliente" name="nombre_cliente" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

            <div class="form-group col-md-6">
                <label>Telefono</label>
                <input type="text" class="form-control" id="celular_cliente" name="celular_cliente" maxlength="10" onKeyPress="return soloNumeros(event);">
            </div>

        </div>

        <div class="row">

            <div class="form-group col-md-6">
                <label>Dirección*</label>
                <input type="text" class="form-control" id="direccion_cliente" name="direccion_cliente" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

            <div class="form-group col-md-6">
                <label>Comentario*</label>
                <input type="text" class="form-control" id="comentario_cliente" name="comentario_cliente" onkeyup='javascript:this.value=this.value.toUpperCase();'>
            </div>

        </div>

    </div>

    

    <div class="box-footer">
        <button type="submit" class="btn btn-primary" id="btnGuardar">Guardar</button>
    </div>

    <input type="hidden" id="cve_cliente" name="cve_cliente">
</form>

<script type="text/javascript">


    $('#formCliente').on('submit',function(e){
        e.preventDefault();

        if ( $('#nombre_cliente').val()  == "" )
        {
            msgAlert2("Favor de ingresar el nombre del cliente.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#direccion_cliente').val() == "" )
        {
            msgAlert2("Favor de ingresar la dirección del cliente.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else if ( $('#comentario_cliente').val() == "" )
        {
            msgAlert2("Favor de ingresar la colonia del cliente.","warning");
            setTimeout(function() { $("#msgAlert2").fadeOut(1500); },3000);
        }
        else
        {

            $("#btnGuardar").prop('disabled', true);
            
            $.ajax({
                url      : $(this).attr('action'),
                data     : $(this).serialize(),
                type: "POST",
                success: function(datos){

                    var myJson = JSON.parse(datos);
                    
                    if(myJson.status == "success")
                    {
                        $('#modal_formCliente').modal('hide');

                        //var table = $('#gridCliente').DataTable();
                                    
                        //table.clear();
                        //table.destroy();
                        
                        //Reinicializamos tabla
                        cargarTablaCliente();

                        msgAlert(myJson.msg ,"success");
                        setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                        //$('#msgAlert').css("display", "none");
                        $("#btnGuardar").prop('disabled', false);
                        $("#btnGuardar").html('Guardar');

                    }
                    else
                    {
                        $("#btnGuardar").prop('disabled', false);
                        msgAlert2(myJson.msg ,"danger");
                    }
                }
            }); 
        }
    });

    


    function msgAlert2(msg,tipo)
    {
        $('#msgAlert2').css("display", "block");
        $("#msgAlert2").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>