<!-- jQuery -->
<script src="public/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="public/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="public/dist/js/adminlte.min.js"></script>
<!-- Ion Slider -->
<script src="public/plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
<!-- Bootstrap slider -->
<script src="public/plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<script src="<?php echo RUTA_URL; ?>public/librerias/bootbox/bootbox.min.js"></script>
<script src="<?php echo RUTA_URL; ?>public/js/main.js"></script>

<!-- DataTables  & Plugins -->
<script src="public/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="public/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="public/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="public/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="public/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="public/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="public/plugins/jszip/jszip.min.js"></script>
<script src="public/plugins/pdfmake/pdfmake.min.js"></script>
<script src="public/plugins/pdfmake/vfs_fonts.js"></script>
<script src="public/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="public/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="public/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="public/plugins/jsgrid/jsgrid.min.js"></script>
<!-- SweetAlert2 -->
<script src="public/plugins/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="public/plugins/toastr/toastr.min.js"></script>
<!-- Select2 -->
<script src="public/plugins/select2/js/select2.full.min.js"></script>
<!-- Bootstrap4 Duallistbox -->
<script src="public/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<!-- InputMask -->
<script src="public/plugins/moment/moment.min.js"></script>
<script src="public/plugins/inputmask/jquery.inputmask.min.js"></script>
<!-- date-range-picker -->
<script src="public/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="public/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="public/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Bootstrap Switch -->
<script src="public/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<!-- BS-Stepper -->
<script src="public/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<!-- dropzonejs -->
<script src="public/plugins/dropzone/min/dropzone.min.js"></script>
<script type="text/javascript">
    var cveperfil = '<?php echo $_SESSION['cveperfil_usuario']; ?>';
    var nombre_perfil = '<?php echo strtoupper($_SESSION["nombre_perfil"]); ?>';
    var cvesucursal_usuario = '<?php echo strtoupper($_SESSION["cvesucursal_usuario"]); ?>';
    var cve_usuario = '<?php echo strtoupper($_SESSION["cve_usuario"]); ?>';
   

</script>
