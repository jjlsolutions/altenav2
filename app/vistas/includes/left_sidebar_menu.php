<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="home" class="brand-link">
      <img src="public/img/LogoAltena.jpg" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" >
      <span class="brand-text font-weight-light">HELADOS ALTEÑA</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="public/dist/img/user.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a style="font-size: 12px;" class="d-block"><?php echo strtoupper($_SESSION["nombreUsuario"]); ?></a>
          <a class="d-block"><i class="fa fa-circle text-success"></i><?php echo strtoupper($_SESSION["nombre_perfil"]); ?></a>
          <a style="font-size: 14px;"></i><?php echo $_SESSION["nombre_sucursal"]; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               <?php
            foreach ($_SESSION["menu_perfil"] as $valor1)
            {
            ?>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="nav-icon fas <?php echo $valor1[icono]; ?>"></i>
                    <p>
                        <?php echo $valor1[text]; ?>
                        <i class="right fas fa-angle-left"></i>
                    </p>
                    </a>
                    <ul class="nav nav-treeview">
                    <?php
                    foreach ($valor1[opcion] as $valor2)
                    {
                    ?>
                        <li class="nav-item">
                            <a href="<?php echo $valor2[metodo_opcion]; ?>" class="nav-link">
                            <i class="far fa-circle nav-icon"></i>
                            <p><?php echo $valor2[nombre_opcion]; ?></p>
                            </a>
                        </li>
                    <?php
                    }
                    ?>
                    </ul>
                </li>
            <?php
            }
            ?>


        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>