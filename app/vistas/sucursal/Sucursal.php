<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Sucursales</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sucursales
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                <div class="card">
                    <div class="card-header">
                    <button class="btn btn-primary" id="btnMostraFormSucursal">Nueva sucursal</button>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                    <table id="gridSucursal" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Dirección&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Teléfono&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                <th>Tipo</th>
                                <th>Representante</th>
                                <th>Editar</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                    </table>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->

                <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- modales -->
<div class="modal fade" id="modal_formSucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Sucursal</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formSucursal"> 

            </div>
        </div>
    </div>
</div>

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">

    $(document).ready(function () {

        tableSucursales = $('#gridSucursal').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "10%","className": "text-center","targets": 5},
                {"width": "10%","className": "text-center","targets": 6},
            ],

            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });

        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarTablaSucursal();

    });

    function cargarTablaSucursal()
    {
        $.ajax({
            url      : 'sucursal/consultar',
            type     : "POST",
            data    : { ban: 1 },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                tableSucursales.clear().draw();

                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;

                    $(myJson.arrayDatos).each( function(key, val)
                    {

                        if (parseInt(val.estatus_sucursal) == 1)
                        {
                            title = 'Sucursal activa';
                            icon = 'fa fa-circle';
                            color_icon = "color: #4ad129;"
                            accion = "bloquearSucursal('" + val.cve_sucursal + "','0')";
                        }
                        else
                        {
                            title = 'Sucursal bloqueada';
                            icon = 'fa fa-circle';
                            color_icon = "color: #f00;"
                            accion = "bloquearSucursal('" + val.cve_sucursal + "','1')";
                        }

                        var btn_editar = "<i class='fa fa-edit' style='font-size:18px; cursor: pointer;' title='Editar Sucursal' onclick=\"mostrarSucursal('" + val.cve_sucursal + "')\"></i>";
                        var btn_status = "<i class='" + icon + "' style='font-size:14px; " + color_icon + " cursor: pointer;' title='" + title + "' onclick=\"" + accion + "\"></i>";

                        tableSucursales.row.add([
                            val.nombre_sucursal,
                            val.direccion_sucursal,
                            val.telefono_sucursal,
                            val.tipo_sucursal,
                            val.representante_sucursal,
                            btn_editar,
                            btn_status,
                        ]).draw();
                    })

                }
                else
                {
                    tableSucursales = $('#gridSucursal').DataTable();
                    
                }

            }
        });
    }

    $('#btnMostraFormSucursal').click(function (e) {

        $('#modal_formSucursal').modal({
            keyboard: false
        });

        $("#muestra_formSucursal").html('Cargando...');

        $.ajax({
            url: 'sucursal/formSucursal',
            success: function(datos){

                $("#muestra_formSucursal").html(datos);

            }
        });
        return false;
    });

    function mostrarSucursal(cve_sucursal)
    {
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'sucursal/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_sucursal: cve_sucursal 

            },
            beforeSend: function() {
                // setting a timeout

            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                console.log(myJson);

                $('#modal_formSucursal').modal({
                    keyboard: false
                });

                $("#muestra_formSucursal").html('Cargando...');

                $.ajax({
                    url: 'sucursal/formSucursal',
                    success: function(datos){

                        
                        $("#muestra_formSucursal").html(datos);

                        
                        $('#nombre_sucursal').val(myJson.arrayDatos[0].nombre_sucursal);
                        $('#calle_sucursal').val(myJson.arrayDatos[0].calle_sucursal);
                        $('#colonia_sucursal').val(myJson.arrayDatos[0].colonia_sucursal);
                        $('#telefono_sucursal').val(myJson.arrayDatos[0].telefono_sucursal);
                        $('#representante_sucursal').val(myJson.arrayDatos[0].representante_sucursal);
                        document.getElementById("tipo_sucursal").selectedIndex = myJson.arrayDatos[0].tipo_sucursal;
                        $('#cve_sucursal').val(myJson.arrayDatos[0].cve_sucursal);

                        $("#btnGuardar").html('Actualizar Sucursal');
                        
                    }
                });

            }
        });
    }

    function bloquearSucursal(cve_sucursal,bloqueo)
    {

        if (bloqueo == 0)
        {
            var msg = "Esta seguro de bloquear esta sucursal?";
            var ban = 2;
        }else{
            var msg = "Esta seguro de desbloquear esta sucursal?";
            var ban = 3;
        }

        bootbox.confirm({
            message: msg,
            buttons: {
                confirm: {
                    label: 'Si'
                },
                cancel: {
                    label: 'No'
                }
            },
            callback: function (result) {
                if (result == true){

                    $.ajax({
                        url      : 'sucursal/bloquearSucursal',
                        type     : "POST",
                        data     : { 

                                ban: ban, 
                                cve_sucursal: cve_sucursal 

                        },
                        beforeSend: function() {
                            // setting a timeout

                        },
                        success  : function(datos) {

                            var myJson = JSON.parse(datos);
                    
                            if(myJson.status == "success")
                            {

                                //var table = $('#gridSucursal').DataTable();
                                        
                                //table.clear();
                                //table.destroy();

                                //Reinicializamos tabla
                                cargarTablaSucursal();

                                msgAlert(myJson.msg ,"info");
                                setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                            }

                        }
                    });

                }else{
                    //No se hace nada...
                }
            }
        });

    }


    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }

</script>

</body>
</html>
