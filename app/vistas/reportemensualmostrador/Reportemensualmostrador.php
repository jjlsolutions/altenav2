<?php 
if($_SESSION['cveperfil_usuario'] != 1){
    session_start();
    session_unset();
    session_destroy(); 
    header("Location: login");
    exit;
}
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$fecha_serv = date("d/m/Y");

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Concentrad  del día</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
   
   <style>
		#portapdf { width: auto; height: auto; border: 1px solid #484848; margin: 0 auto; }
	</style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
            REPORTE MENSUAL MOSTRADOR 2022
            </h1>

        </section>

        <!-- Main content -->
        <section class="content">
            
        <div class="row">
                <div class="col-12">
                    <!-- /.card-header -->
                    <div class="card-body">
                        <embed src="reportes/concentrado.php#toolbar=1&navpanes=0&scrollbar=0" type="application/pdf" width="100%" height="700px" />  
                    </div>
                    <!-- /.card -->
                </div>
            <!-- /.col -->
            </div>
            
            <div class="container-fluid">
                
            <div class="row" id="divtotalVendedor">
                
            </div>
            <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- /.modal -->

<?php 
include RUTA_APP . 'vistas/includes/script.php'; 
?>

<script type="text/javascript">
  
    $(document).ready(function () {
        
    });


</script>

</body>
</html>
