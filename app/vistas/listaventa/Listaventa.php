<?php 
    $url = explode("/", $_SERVER['REQUEST_URI']);
	$archivo = array_pop($url);
	$menu = $_SESSION["menu_perfil"];

	$archivos_array = array();
    foreach ($_SESSION["menu_perfil"] as $valor1)
    {
        foreach ($valor1["opcion"] as $valor2)
        {
            $archivos_array[] = $valor2['metodo_opcion'];
        }
    }

	$opcion_menu = (in_array($archivo, $archivos_array)) ? 1 : 0;
	if (!isset($_SESSION["cve_usuario"]) || $opcion_menu == 0)
	{
		session_start();
		session_unset();
		session_destroy(); 
		header("Location: login");
		exit;
	}

	$permisos_array = array();
    foreach ($_SESSION["permiso_perfil"] as $permiso)
    {
        foreach ($permiso as $valor2)
        {
            $permisos_array[] = $valor2['cve_permiso'];
        }
    }
	$fecha_serv = date("d/m/Y");
/*
modificaciones en proc 18-01-2022
obtenListaventa
obtenSucursales
generarFolio
*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo NOMBRE_SITIO; ?> | Listaventa</title>
    <?php 
    include RUTA_APP . 'vistas/includes/link.php'; 
    ?>
    <style>
      .custom-control-label {
            position: relative;
            margin-bottom: 0;
            vertical-align: top;
          }
          .custom-control-label::before {
            position: absolute;
            top: 0.34rem;
            left: -1.5rem;
            display: block;
            width: 1.4rem;
            height: 1.4rem;
            pointer-events: none;
            content: "";
            background-color: #dee2e6;
            border: #adb5bd solid 1px;
            box-shadow: inset 0 0.25rem 0.25rem rgba(0, 0, 0, 0.1);
          }
          .custom-control-label::after {
            position: absolute;
            top: 0.50rem;
            left: -1.3rem;
            display: block;
            width: 1rem;
            height: 1rem;
            content: "";
            background: 100% / 100% 100% no-repeat;
          }
          .custom-checkbox .custom-control-label::before{
            border-radius: 1.0rem;
          }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

    <?php 
    include RUTA_APP . 'vistas/includes/header.php';

    include RUTA_APP . 'vistas/includes/left_sidebar_menu.php'; 
    ?>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Lista ventas
            </h1>
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fas fa-cash-register" onclick="ver_total_caja();"></i>
            </a>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="row" id="divBuscarVentas">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Tipo venta:</label>
                                        <select class="form-control" id="cmbTipoVenta" name="cmbTipoVenta">
                                            <option value="3">Todos</option>    
                                            <option value="1">Público</option>
                                            <option value="2">Domicilio</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Estatus:</label>
                                        <select class="form-control" id="cmbEstatus" name="cmbEstatus">
                                            <option value="5">Todos</option>    
                                            <option value="1">Por cobrar</option>
                                            <option value="2">Pendiente cancelación</option>
                                            <option value="3">Cancelado</option>
                                            <option value="4">Pagado</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label>Fecha:</label>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                            <span class="input-group-text">
                                                <i class="far fa-calendar-alt"></i>
                                            </span>
                                            </div>
                                            <input type="text" class="form-control float-right" id="cmbFecha" name="cmbFecha" readonly>
                                        </div>
                                        <!-- /.input group -->
                                    </div>
                                </div>
                            </div>
                            <div class="float-right" id="btnBuscarVentas">
                                <button type="submit" class="btn btn-primary" id="btnBuscarVentas" onclick="buscarVentas();">Buscar</button>
                            </div>
                            <div class="row" id="divRadioBuscarPendientes">
                                <div class="col-12"> 
                                <div class="custom-control custom-checkbox float-left"> 
                                    <input class="custom-control-input" type="checkbox" id="CheckboxMayoreo" onclick="OcultarElementosBuscarVentas();" >
                                    <label for="CheckboxMayoreo" class="custom-control-label">Pendientes de pago</label>
                                </div>
                                </div>
                            </div>
                            <br>
                            <div class="clearfix" style="margin-top: 25px;">
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                            </div>
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                        <table id="gridProducto" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Folio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>Tipo precio&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>Tipo venta&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>Cliente&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>Usuario</th>
                                    <th>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                                    <th>Ticket</th>
                                    <th>Imprimir</th>
                                    <th>Estatus</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->

                    <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <div class="row" id="divtotalVendedor">
                    
                </div>

            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
    
    <?php 
    //include RUTA_APP . 'vistas/includes/footer.php';

    include RUTA_APP . 'vistas/includes/control_sidebar_right.php';
    ?>

</div>
<!-- ./wrapper -->

<!-- modales -->
<div class="modal fade" id="modal_formProducto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dm modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">Venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formListaVenta"> 
                <div class="box-body">
                    <div class="row">
                        <div id="labelTiket">
                        </div>
                        

                    </div>
                    <hr>
                    <div class="bg-primary py-2 px-3 mt-4">
                        <h2 class="mb-0" id="totalventa">
                            
                        </h2>
                        <h2 class="mb-0" id="cobroEnvio">
                            
                        </h2>
                        <h2 class="mb-0" id="precioEspecial">
                            
                        </h2>
                        <h2 class="mb-0" id="totalTotalventa">
                            
                        </h2>
                        <h4 class="mt-0">
                            <small id="folioventa"></small>
                        </h4>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_formPagar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" >
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="folioventaPagar"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="muestra_formListaVenta"> 
                    <input type="hidden" id="cve_ventaPagar" name="cve_ventaPagar">
                    <input type="hidden" id="tipo_ventaPagar" name="tipo_ventaPagar">
                    <div class="row">
                        <div class="col-12">
                            <div style="font-size: 250%;color:black;">Total: $<label id="totalventaPagar" >0</label></div>
                        </div>
                        <h5 class="mt-0">
                            <small id="vendedorPagar"></small>
                        </h5>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label>Pagó con:*</label>
                            <input type="number" min="0" class="form-control form-control-lg" style="border: 0;box-shadow: none;font-size: 38pt;text-align: center; color:black;margin-bottom:5px;" id="cantidad_sabor" name="cantidad_sabor" onkeyup='javascript:this.value=this.value.toUpperCase();sacaTotalProducto(this.value)'>
                            <div class="float-right">
                                <button type="submit" class="btn btn-primary" id="btnPagarVentas" onclick="CompletarVenta();" data-dismiss="modal">PAGAR</button>
                            </div>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Cambio:*</label>
                            <h3 style="border: 0;box-shadow: none;font-size: 28pt;text-align: center;" id="cambioPagar">0</h3>
                        </div>
                    </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalArchivo" data-backdrop="static" data-keyboard="false">
			<div class="modal-dialog" style="width: 730px;">
				<div class="modal-content">
					<div class="modal-header">
						<div id="tipo"><h4 class="modal-title">Documento digitalizado</h4></div>
					</div>
					<div class="modal-body">
						<div id="portapdf" style="height: 400px;">
							<!-- <object data="archivo.pdf" type="" width="100%" height="100%"></object>  -->
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" id="" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->

<?php 
include RUTA_APP . 'vistas/includes/script.php';
?>

<script type="text/javascript">
var totalRegistros = 0;
    $(document).ready(function () {
        if(cveperfil != 1){
            $("#divtotalVendedor").hide();
        }
        var bar = $('.progress-bar');
		bar.width('0%');
        $('#cmbFecha').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            }
        });

        
        tableProductos = $('#gridProducto').DataTable( {    
            "responsive": true,
            "searching" : true,
            "paging"    : true,
            "ordering"  : false,
            "info"      : true,
            "autoWidth": false,
            "columnDefs": [
                {"width": "5%","className": "text-center","targets": 6},
                {"width": "5%","className": "text-center","targets": 7},
                {"width": "5%","className": "text-center","targets": 8},
                {"className": "dt-center", "targets": "_all"}
            ],
            "bJQueryUI":true,"oLanguage": {
                "sEmptyTable":     "No hay datos registrados en la Base de Datos.",
                "sInfo":           "Mostrando desde _START_ hasta _END_ de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando desde 0 hasta 0 de 0 registros",
                "sInfoFiltered":   "(filtrado de _MAX_ registros en total)",
                "sInfoPostFix":    "",
                "sInfoThousands":  ",",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sLoadingRecords": "Cargando...",
                "sProcessing":     "Procesando...",
                "sSearch":         "Buscar:",
                "sZeroRecords":    "No se encontraron resultados",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "Último",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": activar para Ordenar Ascendentemente",
                    "sSortDescending": ": activar para Ordendar Descendentemente"
                }
            }
        });

        //Mandamos llamar la función para mostrar tabla al cargar la página
        cargarTablaVenta();
        sacaTotalRegistrosBD();
        consultaacadarato1();

    });

    function buscarVentas(){
        cargarTablaVenta();
    }

    function cargarTablaVenta()
    {
        $.ajax({
            xhr: function() {
            var xhr = new window.XMLHttpRequest();
            var bar = $('.progress-bar');

            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                //console.log(percentComplete);

                var percentVal = percentComplete+'%';
                bar.width(percentVal);

                }
            }, false);
            return xhr;
            },
            url      : 'listaventa/consultar',
            type     : "POST",
            data    : { 
                ban: 1,
                cve_venta: '',
                tipo_venta: $("#cmbTipoVenta").val(),
                estatus_venta: $("#cmbEstatus").val(),
                fecha_venta: $("#cmbFecha").val()
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);
                tableProductos.clear().draw();
                
                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;
                    var totalDia = 0;

                    $(myJson.arrayDatos).each( function(key, val)
                    {
                        cancelar = '';
                        if (parseInt(val.estatus_venta) == 1)
                        {
                            title = '&nbsp;&nbsp;&nbsp;Pagar&nbsp;&nbsp;&nbsp;';
                            icon = 'badge badge-success';
                            accion = "PagarVenta('" + val.cve_venta + "','" + val.tipo_venta + "')";
                            titleC = 'Cancelar';
                            iconC = 'badge badge-warning';
                            accionC = "bloquearVenta('" + val.cve_venta + "',0,'0',"+val.tipo_venta+")";
                            cancelar = "<br><span class='"+iconC+"' style=\"cursor: pointer;font-size:14px;\" onclick=\"" + accionC + "\">"+titleC+"</span>"
                        }
                        else if(parseInt(val.estatus_venta) == 2)
                        {
                            title = 'Pendiente cancelacion';
                            icon = 'badge badge-warning';
                            accion = "";
                            if(cveperfil == 1 || cveperfil == 2){
                                title = 'Pendiente cancelacion';
                                icon = 'badge badge-warning';
                                if(val.tipo_venta == 1){
                                    accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'')";

                                }else{
                                    if (parseInt(val.encamino_venta) == 2)
                                    {
                                       
                                        accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'si')";
                                    }
                                    else
                                    {
                                       accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'no')";
                                        
                                    }
                                }
                                

                                
                            }
                            
                        }
                        else if(parseInt(val.estatus_venta) == 3){
                            title = 'Cancelado';
                            icon = 'badge badge-danger';
                            accion = "bloquearVenta('" + val.cve_venta + "','2','"+val.motivo_venta+"',"+val.tipo_venta+")";
                        }
                        else if(parseInt(val.estatus_venta) == 4){
                            title = 'Pagado';
                            icon = 'badge badge-info';
                            accion = "";
                            totalDia = totalDia + parseFloat(val.total_venta);
                        }

                        var btn_editar = "<i class='fa fa-eye' style='font-size:18px; cursor: pointer;' title='Ver la venta' onclick=\"mostrarVenta('" + val.cve_venta + "','"+val.tipo_venta+"')\"></i>";

                        if (parseInt(val.encamino_venta) == 1 && val.tipo_venta == 2 && parseInt(val.estatus_venta) != 2 && parseInt(val.estatus_venta) != 3)
                        {
                            accionC = "encaminoVenta(" + val.cve_venta + ")";
                            encamino = "<br><span class='badge badge-primary' style=\"cursor: pointer;font-size:14px;\" onclick=\"" + accionC + "\">¿En camino?</span>"
                        }
                        else
                        {
                            encamino = val.nombre_usuarioencamino != null ? "<br><i cursor: pointer;' title='Aurotizó que va en camino el pedido'>"+val.nombre_usuarioencamino+"</i>" : '';
                            
                        }

                        var btn_print = "<i class='fa fa-print' style='font-size:18px; cursor: pointer;' title='Ver ticket' onclick=\"ticketPrint('" + val.cve_venta + "','" + val.tipo_venta + "')\"></i>"+encamino;
                        <?php if((in_array(13, $permisos_array))){ ?>
                            var btn_status = "<span class='"+icon+"' style=\"cursor: pointer;font-size:14px;margin-bottom: 5px;\" onclick=\"" + accion + "\">"+title+"</span>"+cancelar;
                        <?php }else{ ?>
                            var btn_status = '';
                        <?php } ?>
                        
                        

                        tableProductos.row.add([
                            val.folio_venta,
                            val.tipoprecio_venta == 1 ? 'Menudeo' : 'Mayoreo',
                            val.tipo_venta == 1 ? 'Público' : 'Domicilio',
                            val.tipo_venta == 1 ? 'Motrador' : val.nombre_cliente,
                            val.nombre_usuario+" "+val.apellidop_usuario,
                            val.fechaventa,
                            btn_editar,
                            btn_print,
                            btn_status,
                        ]).draw();
                    });
                    var bar = $('.progress-bar');
					bar.width('0%');
                    
                }
                else
                {
                    
                    tableProductos = $('#gridProducto').DataTable();
                    var bar = $('.progress-bar');
					bar.width('0%');                    
                }

            }
        });
        if(cveperfil == 1){
            cargarTablaVentaVendedores();
        }
    }

    function encaminoVenta(cve_venta){

            bootbox.dialog({ 
                message: '<b>¿El producto va en camino?</b><br>',
                size: 'large',
                buttons: {
                    No: {
                        label: 'No',
                        className: 'btn-danger',
                        callback: function(){
                            /*$.ajax({
                                url      : 'listaventa/bloquearListaventa',
                                type     : "POST",
                                data     : { 
                                    ban: 1, 
                                    cve_venta: cve_venta,
                                    motivo: motivo
                                },
                                success  : function(datos) {

                                    var myJson = JSON.parse(datos);
                            
                                    if(myJson.status == "success")
                                    {

                                        cargarTablaVenta();

                                        msgAlert(myJson.msg ,"info");
                                        setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                                    }

                                }
                            });*/
                        }
                    },
                    Si: {
                        label: 'Si',
                        className: 'btn-primary',
                        callback: function(){
                            $.ajax({
                            url      : 'venta/actializarStockDomicilio',
                            type     : "POST",
                            data     : { 
                                ban: 1, 
                                cve_venta: cve_venta
                            },
                            success  : function(datos) {
                                var myJson = JSON.parse(datos);
                            
                                if(myJson.status == "success")
                                {
                                    if($("#CheckboxMayoreo").prop("checked")){
                                        cargarTablaVentaPendientes();
                                    }else{
                                        cargarTablaVenta();
                                    }
                                }else{
                                    alert(myJson.msg);
                                }
                                
                            }
                        });                
                        }
                    }
                }
            });
    }

    function cargarTablaVentaPendientes()
    {
        $.ajax({
            xhr: function() {
            var xhr = new window.XMLHttpRequest();
            var bar = $('.progress-bar');

            xhr.upload.addEventListener("progress", function(evt) {
                if (evt.lengthComputable) {
                var percentComplete = evt.loaded / evt.total;
                percentComplete = parseInt(percentComplete * 100);
                //console.log(percentComplete);

                var percentVal = percentComplete+'%';
                bar.width(percentVal);

                }
            }, false);
            return xhr;
            },
            url      : 'listaventa/consultar',
            type     : "POST",
            data    : { 
                ban: 5,
                cve_venta: '',
                tipo_venta: 3,
                estatus_venta: 1,
                fecha_venta: ''
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);
                tableProductos.clear().draw();
                
                if(myJson.arrayDatos.length > 0)
                {

                    var title;
                    var icon;
                    var color_icon;
                    var accion;
                    var totalDia = 0;

                    $(myJson.arrayDatos).each( function(key, val)
                    {
                        cancelar = '';
                        if (parseInt(val.estatus_venta) == 1)
                        {
                            title = '&nbsp;&nbsp;&nbsp;Pagar&nbsp;&nbsp;&nbsp;';
                            icon = 'badge badge-success';
                            accion = "PagarVenta('" + val.cve_venta + "','" + val.tipo_venta + "')";
                            titleC = 'Cancelar';
                            iconC = 'badge badge-warning';
                            accionC = "bloquearVenta('" + val.cve_venta + "',0,'0',"+val.tipo_venta+")";
                            cancelar = "<br><span class='"+iconC+"' style=\"cursor: pointer;font-size:14px;\" onclick=\"" + accionC + "\">"+titleC+"</span>"
                        }
                        else if(parseInt(val.estatus_venta) == 2)
                        {
                            title = 'Pendiente cancelacion';
                            icon = 'badge badge-warning';
                            accion = "";
                            if(cveperfil == 1 || cveperfil == 2){
                                title = 'Pendiente cancelacion';
                                icon = 'badge badge-warning';
                                if(val.tipo_venta == 1){
                                    accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'')";

                                }else{
                                    if (parseInt(val.encamino_venta) == 2)
                                    {
                                       
                                        accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'si')";
                                    }
                                    else
                                    {
                                       accion = "bloquearVenta('" + val.cve_venta + "','1','"+val.motivo_venta+"',"+val.tipo_venta+",'no')";
                                        
                                    }
                                }
                            }
                            
                        }
                        else if(parseInt(val.estatus_venta) == 3){
                            title = 'Cancelado';
                            icon = 'badge badge-danger';
                            accion = "bloquearVenta('" + val.cve_venta + "','2','"+val.motivo_venta+"',"+val.tipo_venta+")";
                        }
                        else if(parseInt(val.estatus_venta) == 4){
                            title = 'Pagado';
                            icon = 'badge badge-info';
                            accion = "";
                            totalDia = totalDia + parseFloat(val.total_venta);
                        }

                        if (parseInt(val.encamino_venta) == 1 && val.tipo_venta == 2 && parseInt(val.estatus_venta) != 2 && parseInt(val.estatus_venta) != 3)
                        {
                            accionC = "encaminoVenta(" + val.cve_venta + ")";
                            encamino = "<br><span class='badge badge-primary' style=\"cursor: pointer;font-size:14px;\" onclick=\"" + accionC + "\">¿En camino?</span>"
                        }
                        else
                        {
                            encamino = val.nombre_usuarioencamino != null ? "<br><i cursor: pointer;' title='Aurotizó que va en camino el pedido'>"+val.nombre_usuarioencamino+"</i>" : '';
                            
                        }
                        

                        var btn_editar = "<i class='fa fa-eye' style='font-size:18px; cursor: pointer;' title='Ver la venta' onclick=\"mostrarVenta('" + val.cve_venta + "','"+val.tipo_venta+"')\"></i>";
                        var btn_print = "<i class='fa fa-print' style='font-size:18px; cursor: pointer;' title='Ver ticket' onclick=\"ticketPrint('" + val.cve_venta + "','" + val.tipo_venta + "')\"></i>"+encamino;;
                        if(nombre_perfil != 'CABINA'){
                            var btn_status = "<span class='"+icon+"' style=\"cursor: pointer;font-size:14px;margin-bottom: 5px;\" onclick=\"" + accion + "\">"+title+"</span>"+cancelar;
                        }else{
                            var btn_status = '';
                        }

                        tableProductos.row.add([
                            val.folio_venta,
                            val.tipoprecio_venta == 1 ? 'Menudeo' : 'Mayoreo',
                            val.tipo_venta == 1 ? 'Público' : 'Domicilio',
                            val.tipo_venta == 1 ? 'Motrador' : val.nombre_cliente,
                            val.nombre_usuario+" "+val.apellidop_usuario,
                            val.fechaventa,
                            btn_editar,
                            btn_print,
                            btn_status,
                        ]).draw();
                    });
                    var bar = $('.progress-bar');
					bar.width('0%');
                    
                }
                else
                {
                    
                    tableProductos = $('#gridProducto').DataTable();
                    var bar = $('.progress-bar');
					bar.width('0%');                    
                }

            }
        });
        if(cveperfil == 1){
            cargarTablaVentaVendedores();
        }
    }

    function cargarTablaVentaVendedores()
    {
        divtotalVendedor = $("#divtotalVendedor");
        $("#divtotalVendedor").empty();
        
        
        $.ajax({
            url      : 'sucursal/consultar',
            type     : "POST",
            data    : { ban: 1 },
            success  : function(datos1) {

                var myJson1 = JSON.parse(datos1);


                if(myJson1.arrayDatos.length > 0)
                {

                    $(myJson1.arrayDatos).each( function(key1, val1)
                    {
                        $.ajax({
                            xhr: function() {
                            var xhr = new window.XMLHttpRequest();
                            var bar = $('.progress-bar');

                            xhr.upload.addEventListener("progress", function(evt) {
                                if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total;
                                percentComplete = parseInt(percentComplete * 100);
                                //console.log(percentComplete);

                                var percentVal = percentComplete+'%';
                                bar.width(percentVal);

                                }
                            }, false);
                            return xhr;
                            },
                            url      : 'listaventa/consultar',
                            type     : "POST",
                            data    : { 
                                ban: 4,
                                cve_venta: '',
                                tipo_venta: 0,
                                estatus_venta: 0,
                                fecha_venta: $("#cmbFecha").val(),
                                cve_sucursal : val1.cve_sucursal
                            },
                            success  : function(datos) {
                                var myJson = JSON.parse(datos);
                                var totalVentas = 0;
                                
                                var cadenaTotales = '';
                                var totalGasto = 0;
                                if(myJson.arrayDatos.length > 0)
                                {    
                                     cadenaTotales+='<div class="col-md-4">'+
                                            '<div class="card card-default">'+
                                              '<div class="card-header">'+
                                                '<h3 class="card-title">'+
                                                  '<i class="fas fa-cash-register"></i> '+
                                                  val1.nombre_sucursal+'<br>'+
                                                '</h3>'+
                                              '</div>'+
                                              '<div class="card-body">';    
                                    $(myJson.arrayDatos).each( function(key, val)
                                    { 
                                        cadenaTotalesGasto = "";
                                        if(val.vendedor == 'Total gasto' && val.total_venta > 0){
                                            cadenaTotalesGasto='<div class="col-12">'+
                                                            '<div class="info-box mb-3">'+
                                                                '<span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>'+
                                                                '<div class="info-box-content">'+
                                                                    '<span class="info-box-text">'+val.vendedor+'</span>'+
                                                                    '<span class="info-box-number" id="totalGasto">-'+val.total_venta+'</span>'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>';
                                            totalGasto = val.total_venta;
                                        }else{
                                            cadenaTotales+='<div class="col-12">'+
                                                            '<div class="info-box mb-3">'+
                                                                '<span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>'+
                                                                '<div class="info-box-content">'+
                                                                    '<span class="info-box-text">'+val.vendedor+'</span>'+
                                                                    '<span class="info-box-number" id="totalVendedor">'+val.total_venta+'</span>'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>';
                                            totalVentas = parseFloat(totalVentas) + parseFloat(val.total_venta);
                                        }
                                        
                                        
                                        
                                    });
                                    cadenaTotales+=cadenaTotalesGasto;
                                    cadenaTotalesGasto = '';
                                    totalVentas = totalVentas - totalGasto;
                                    cadenaTotales+='<div class="col-12">'+
                                                            '<div class="info-box mb-3">'+
                                                                '<span class="info-box-icon bg-success elevation-1"><i class="fas fa-shopping-cart"></i></span>'+
                                                                '<div class="info-box-content">'+
                                                                    '<span class="info-box-text">Total venta</span>'+
                                                                    '<span class="info-box-number" id="totalVendedor">'+totalVentas+'</span>'+
                                                                '</div>'+
                                                            '</div>'+
                                                        '</div>';

                                     cadenaTotales+='</div>'+
                                                            '</div>'+
                                                          '</div>';
                                    divtotalVendedor.append(cadenaTotales);
                                    var bar = $('.progress-bar');
                                    bar.width('0%');
                                }
                                else
                                {                    
                                    
                                    var bar = $('.progress-bar');
                                    bar.width('0%');                  
                                }

                            }
                        });
                    })

                }
                else
                {
                  
                    
                }

            }
        });

    }

    function consultaacadarato1()
    {
        $.ajax({
            url      : 'listaventa/consultar',
            type     : "POST",
            data    : { 
                ban: 3,
                cve_venta: '',
                tipo_venta: 0,
                estatus_venta: 0,
                fecha_venta: $("#cmbFecha").val()
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);
                
                if(totalRegistros == myJson.arrayDatos[0].total)
                {
                    setTimeout(function(){ 
                        consultaacadarato1();
                     }, 5000);

                }else{
                    cargarTablaVenta();
                    $.ajax({
                        url      : 'listaventa/consultar',
                        type     : "POST",
                        data    : { 
                            ban: 3,
                            cve_venta: '',
                            tipo_venta: 0,
                            estatus_venta: 0,
                            fecha_venta: $("#cmbFecha").val()
                        },
                        success  : function(datos) {
                            var myJson = JSON.parse(datos);
                            totalRegistros = myJson.arrayDatos[0].total;
                            consultaacadarato1();

                        }
                    });
                    
                    
                }

            }
        });
    }

    function sacaTotalRegistrosBD()
    {
        $.ajax({
            url      : 'listaventa/consultar',
            type     : "POST",
            data    : { 
                ban: 3,
                cve_venta: '',
                tipo_venta: 0,
                estatus_venta: 0,
                fecha_venta: $("#cmbFecha").val()
            },
            success  : function(datos) {
                var myJson = JSON.parse(datos);
                totalRegistros = myJson.arrayDatos[0].total;

            }
        });
    }

    function sacaTotalProducto(valor){
        totalventaPagar = $('#totalventaPagar').text();
        var totalProCuenta = valor - totalventaPagar; 
        if(totalProCuenta >= 0){
            $("#btnPagarVentas").show();
            $('#cambioPagar').text(totalProCuenta);
            return;
        }
        $('#cambioPagar').text("0");
        $("#btnPagarVentas").hide();
        
    }

    function ticketPrint(cve_venta,tipo_venta) 
    {
        var mywindow = window.open("listaventa/mostrarTiket/?cve_venta=" + cve_venta+"&tipo_venta="+tipo_venta, '_blank', 'height=400,width=700').print();
    
    }

    function mostrarVenta(cve_venta, tipoventa)
    {
        $("#labelTiket").empty();
        $('#totalventa').text("TOTAL: $0");
                        $('#folioventa').text("FOLIO: ");
                        
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'listaventa/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_venta: cve_venta,
                    tipo_venta: tipoventa
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                console.log(myJson);

                $('#modal_formProducto').modal({
                    keyboard: false
                });

                        botones = $("#labelTiket");
                        $("#labelTiket").empty();
                        totalglobal = 0;
                        $(myJson.arrayDatos).each( function(key, val)
                        {
                            totalventa = 0;
                            totalventa = val.cantidad_venta * val.precio_venta;
                            if(tipoventa == 1){                                
                                botones.append('<blockquote>'+val.nombre_producto+'<br>Cantidad: '+val.cantidad_venta+'<br>Total: $'+totalventa+'</blockquote>');
                            }
                            else{
                                botones.append('<blockquote>'+val.nombre_producto+' '+val.nombre_sabor+'<br>Cantidad: '+val.cantidad_venta+'<br>Total: $'+totalventa+'</blockquote>');
                            }
                            
                        });
                        if($("#cmbTipoVenta").val() == 1){
                            $('#totalventa').text("TOTAL: $"+myJson.arrayDatos[0].total_venta);
                            $('#folioventa').text("FOLIO: "+myJson.arrayDatos[0].folio_venta);
                        }else{  
                            if(myJson.arrayDatos[0].cobroenvio_venta > 0){ 
                                $('#totalventa').text("SUBTOTAL: $"+myJson.arrayDatos[0].total_venta);                               
                                $('#cobroEnvio').text("ENVÍO: $"+parseFloat(myJson.arrayDatos[0].cobroenvio_venta));
                            }
                            else{
                                $('#totalventa').text("");
                                $('#cobroEnvio').text("");
                            }

                            if(myJson.arrayDatos[0].precioespecial_venta > 0){
                                $('#precioEspecial').text("DESCUENTO: - $"+parseFloat(myJson.arrayDatos[0].precioespecial_venta)); 
                            }
                            else{
                                $('#precioEspecial').text("");
                            }

                            totalglobal = parseFloat(myJson.arrayDatos[0].total_venta) + parseFloat(myJson.arrayDatos[0].cobroenvio_venta) - parseFloat(myJson.arrayDatos[0].precioespecial_venta);
                            $('#totalTotalventa').text("TOTAL: $"+totalglobal);
                            $('#folioventa').text("FOLIO: "+myJson.arrayDatos[0].folio_venta);
                            
                        }
                       
                    
            }
        });
    }

    function PagarVenta(cve_venta, tipo_venta)
    {
        
        $("#btnPagarVentas").hide();
        $("#cve_ventaPagar").val(cve_venta);
        $("#tipo_ventaPagar").val(tipo_venta);
        $("#cantidad_sabor").val("");
        $("#folioventaPagar").text("");
        $("#totalventaPagar").text("");
        $("#cambioPagar").text("0");
        
        
        
        
        $('#msgAlert').css("display", "none");

        $.ajax({
            url      : 'listaventa/consultar',
            type     : "POST",
            data     : { 

                    ban: 2, 
                    cve_venta: cve_venta,
                    tipo_venta: tipo_venta
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                console.log(myJson);

                $('#modal_formPagar').modal({
                    keyboard: false,
                    backdrop: 'static'
                });
                $('#modal_formPagar').on('shown.bs.modal', function () {
                    $('#cantidad_sabor').focus();
                });

                        totalglobal = 0;
                        $(myJson.arrayDatos).each( function(key, val)
                        {
                            totalventa = val.cantidad_venta * val.precio_venta;
                            totalglobal = totalglobal + totalventa;
                        });
                        if(tipo_venta == 2){
                            totalglobal = parseFloat(totalglobal) + parseFloat(myJson.arrayDatos[0].cobroenvio_venta) - parseFloat(myJson.arrayDatos[0].precioespecial_venta);
                        }
                        $('#totalventaPagar').text(totalglobal); 
                        $('#folioventaPagar').text("FOLIO: "+myJson.arrayDatos[0].folio_venta);
                        $('#vendedorPagar').text("VENDEDOR: "+myJson.arrayDatos[0].nombre_usuario+" "+myJson.arrayDatos[0].apellidop_usuario);
                    
            }
        });
    }

    function CompletarVenta(){
        cveventa = $("#cve_ventaPagar").val();
        tipoventa = $("#tipo_ventaPagar").val();
        $.ajax({
            url      : 'listaventa/completarVenta',
            type     : "POST",
            data     : { 
                ban: 1,
                cve_venta: cveventa,
                pagocon_venta: $("#cantidad_sabor").val(),
                cambio_venta: $("#cambioPagar").text()
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);
        
                if(myJson.status == "success")
                {
                    $("#folioventaPagar").text("");
                    $("#totalventaPagar").text("");
                    $("#cantidad_sabor").val("");
                    $("#vendedorPagar").text("");
                    $("#cambioPagar").text("0");
                    ticketPrint(cveventa,tipoventa) ;
                    if($("#CheckboxMayoreo").prop("checked")){
                        cargarTablaVentaPendientes();
                    }else{
                        cargarTablaVenta();
                    }
                    
                    

                    msgAlert(myJson.msg ,"info");
                    setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                }

            }
        });
    }

    
    function mostrarDocumento(cve_venta,tipo_venta)
    {
        $("#portapdf").html('<object data="mostrarTiket?cve_venta='+cve_venta+'&tipo_venta='+tipo_venta+'" type="" width="100%" height="100%"></object>');
	    $("#modalArchivo").modal("show");
    }

    function bloquearVenta(cve_venta,bloqueo,motivo,tipo_venta,encamino)
    {
        if (bloqueo == 0)
        {
            var msg = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;¿Desea cancelar la venta?';
            var ban = 2;

            bootbox.prompt({ 
                title: msg,
                placeholder: 'Escriba el motivo de cancelación',
                callback: function(result){ 
                    
                    if(result !== null && result !== ''){
                        $.ajax({
                            url      : 'listaventa/bloquearListaventa',
                            type     : "POST",
                            data     : { 
                                ban: ban, 
                                cve_venta: cve_venta,
                                motivo: result
                            },
                            success  : function(datos) {

                                var myJson = JSON.parse(datos);
                        
                                if(myJson.status == "success")
                                {

                                    cargarTablaVenta();

                                    msgAlert(myJson.msg ,"info");
                                    setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                                }

                            }
                        });
                    }  
                    else{
                        if(result === ''){
                            bootbox.alert({
                                size: "small",
                                message: "Debe escribir almenos un motivo",
                                callback: function(){ bloquearVenta(cve_venta,bloqueo); }
                            });
                        }
                    }  
                }
            });
        }else if(bloqueo == 1){
            var msg = motivo;
            

            bootbox.dialog({ 
                message: '<b>¿Desea cancelar la venta por el siguiente motivo?</b><br><p>'+motivo+'</p>',
                size: 'large',
                buttons: {
                    No: {
                        label: 'No',
                        className: 'btn-danger',
                        callback: function(){
                            $.ajax({
                            url      : 'listaventa/bloquearListaventa',
                            type     : "POST",
                            data     : { 
                                ban: 1, 
                                cve_venta: cve_venta,
                                motivo: motivo
                            },
                            success  : function(datos) {

                                var myJson = JSON.parse(datos);
                        
                                if(myJson.status == "success")
                                {

                                    cargarTablaVenta();

                                    msgAlert(myJson.msg ,"info");
                                    setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                                }

                            }
                        });
                        }
                    },
                    Si: {
                        label: 'Si',
                        className: 'btn-primary',
                        callback: function(){
                            $.ajax({
                            url      : 'listaventa/bloquearListaventa',
                            type     : "POST",
                            data     : { 
                                ban: 3, 
                                cve_venta: cve_venta,
                                tipo_venta : tipo_venta,
                                encamino : encamino,
                                motivo: motivo
                            },
                            success  : function(datos) {

                                var myJson = JSON.parse(datos);
                        
                                if(myJson.status == "success")
                                {

                                    cargarTablaVenta();

                                    msgAlert(myJson.msg ,"info");
                                    setTimeout(function() { $("#msgAlert").fadeOut(1500); },3000);

                                }

                            }
                        });                
                        }
                    }
                }
            });
        }
        else{
            bootbox.alert("<b>Motivo de cancelación</b><br>"+motivo);
        }
       
    }

    function OcultarElementosBuscarVentas(){
        if($("#CheckboxMayoreo").prop("checked")){
            $("#divBuscarVentas").hide();
            $("#btnBuscarVentas").hide();
            cargarTablaVentaPendientes();
        }
        else{
            $("#divBuscarVentas").show();
            $("#btnBuscarVentas").show();
            buscarVentas();
        }
        
    }

    function ver_total_caja()
    {
        $.ajax({
            url      : 'concentradocajero/consultar',
            type     : "POST",
            data    : { 
                ban: 4,
                fecha_concentradocajero: GetTodayDate() + " - "+ GetTodayDate(),
                sucursal_concentradocajero : cvesucursal_usuario != '0' ? cvesucursal_usuario : '0',
                cveusuario_concentradocajero : cve_usuario != '' ? cve_usuario : '0'
            },
            success  : function(datos) {

                var myJson = JSON.parse(datos);

                bootbox.alert({
                    message: "Total caja: "+myJson.arrayDatos[0].totalcaja_venta,
                    size: 'small'
                });
                
            }
        });
        
    }

    function GetTodayDate() {
        var tdate = new Date();
        var dd = ("0" + (tdate.getDate())).slice(-2); //yields day
        var MM = ("0" + (tdate.getMonth() + 1)).slice(-2); //yields month
        var yyyy = tdate.getFullYear(); //yields year
        var currentDate= yyyy + "-" +( MM) + "-" + dd;

        return currentDate;
    }

    function msgAlert(msg,tipo)
    {
        $('#msgAlert').css("display", "block");
        $("#msgAlert").html("<div class='alert alert-" + tipo + "' role='alert'>" + msg + " <button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button> </div>");
    }
    

</script>

</body>
</html>
