<?php
session_start();
error_reporting(E_ALL);

$model = new ListaventaModelo();
$response = $model->consultar2(2, $_REQUEST["cve_venta"],$_REQUEST["tipo_venta"]);
//print_r($response);
if(empty($response)){
    echo 'Sin datos que mostrar.';
    return;
}
?>
<style type="text/css">
.classImprime{
    height: auto;
    width: 400px;
    /*border: 2px solid;
    border-radius: 0px;*/
    margin: 0px;
    padding: 0px;
    float: left;
    font-family:sans-serif;
    font-size: 10pt;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    color: #000;
}
td{
    padding: 2px;
    font-family:sans-serif;
    font-size: 10pt;
    font-style: normal;
    line-height: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    color: #000;
}
@page{
    size: auto;/* es el valor por defecto */
  margin: 0;
}
</style>
<style>
    .contenedor{
    position: relative;
    display: inline-block;
    text-align: center;
}
.centrado{
    position: absolute;
    top: 70%;
    left: 20%;
    font-size: 50pt;
    -webkit-transform: rotate(-60deg); 
    -moz-transform: rotate(-60deg);
    -o-transform: rotate(-60deg);
    transform: rotate(-60deg);
    height:70px;
    width:70px;
}
</style>

<!--body-->
<body onafterprint="window.close()">

<div align="left" class="classImprime contenedor">
    <div align="center" style="margin: 0px;">
        <b><span style="font-size: 14pt;"><?php echo $response[0]["nombre_sucursal"]; ?></span></b>
        
    </div>
    <table width=390>
        <tr> 
            <td width=60 align=left cellpadding=5 >
                <div align="justify">
                    <img src="<?php echo RUTA_URL; ?>public/img/LogoAltena.jpg" id="borde_negro" style="width: 100%;display: block;margin-bottom: 10px;border-radius:150px;">
                    </div>
            </td>
            <td width=320 align=left colspan=2>
                <div align="justify">
                    <b><span style="font-size: 8pt;">ALONDRA AGLAEL LIZARRAGA ZEPEDA</span><br></b>
                    <div style="font-size: 8pt;margin-top: 5px;">
                    R.F.C: LIZA95808U83 | CURP: LIZA950808MSLZPL07 | <?php echo $response[0]["direccion_sucursal"]; ?> | RÉGIMEN DE INCORPORACIÓN FISCAL.
                    </div>
                </div>
            </td>
        </tr>

    </table>
    ---------------------------------------------------------------------------------
    <?php
        $estatusventa = '';
        if ($response[0]["estatus_venta"] === '1') {
            //$estatusventa = 'NO PAGADO';
        }
        if ($response[0]["estatus_venta"] === '2') {
            $estatusventa = 'CANCELADO';
        }
        if ($response[0]["estatus_venta"] === '3') {
            $estatusventa = 'CANCELADO';
        }
    ?>
  <div class="centrado"><?php echo $estatusventa ?></div>
    <?php
    //////////////////////////////////////////////////////////////   Detalle de la venta, cantidad, productos y precio
    if ($response[0]["tipo_venta"] === '1') {
        $tipo_venta = 'VENTA PUBLICO';
    }
    else{
        $tipo_venta = 'DOMICILIO';
    }
    
    echo "<table width=\"390\">";
        echo"
            <tr> 
                <td width=\"80\" align=\"left\" cellpadding=\"5\" >FECHA:</td>
                <td width=\"300\" align=\"left\" colspan=\"2\"><b>".$response[0]["fechaadd_venta"]."</b></td>
                <!--<td width=\"180\" align=\"left\" colspan=\"2\">&nbsp;</td> -->
             <!-- <td width=\"100\" align=\"right\" ><b>". $response[0]["folio_venta"]."</b></td>--> 
            </tr>
            <tr>
                <td align=\"left\" >FOLIO:</td>
                <td align=\"left\" colspan='2'>".$response[0]["folio_venta"]."</td>
            </tr>
            <tr>
                <td align=\"left\" >SERVICIO:</td>
                <td align=\"left\" colspan='2'>".$tipo_venta."</td>
            </tr>
            <tr>
                <td align=\"left\" >ATENDIO:</td>
                <td align=\"left\" colspan='2'>".$response[0]["nombre_usuario"]."</td>
            </tr>
        ";
    echo "</table>";
    if (count($response)) {
        /*******************PRODUCTOS*************************************/
            echo "<table width=\"390\">";
                echo"<tr><td colspan='4' align=\"center\">****** PRODUCTOS ******</td></tr>
                    <tr> 
                        <td width=\"27\" align=\"left\" cellpadding=\"5\" >CANT</td>
                        <td width=\"310\" align=\"left\" >DESCRIPCI&Oacute;N</td>
                        <td width=\"90\" align=\"left\" >C. UNIT.</td>
                        <td width=\"20\" align=\"center\" >IMPORTE</td> 
                    </tr>";
            $totalServicio = 0;
            $total = 0;
            for($i = 0 ; $i < count($response); $i++) 
            {
                $total = $response[$i]["cantidad_venta"] * $response[$i]["precio_venta"];
                echo "<tr> 
                        <td width=\"27\" >";
                            echo $response[$i]["cantidad_venta"];
                echo "  </td>
                        <td width=\"320\" >";
                            echo htmlspecialchars($response[$i]["tipo_venta"] == 1 ? $response[$i]["nombre_producto"] : $response[$i]["nombre_producto"] . " ". $response[$i]["nombre_sabor"]);
                echo "  </td>
                        <td width=\"80\" >";
                            echo htmlspecialchars('$'.$response[$i]["precio_venta"]);
                echo "  </td>
                        <td width=\"20\" align=\"right\" > $";
                            echo number_format($total, 2, '.', ',');
                echo "</td> 
                    </tr>";     
            }
            
            $totaltotalVenta = $response[0]["total_venta"] + $response[0]["cobroenvio_venta"] - $response[0]["precioespecial_venta"];
            
            echo "</table>";
            echo "<table width=\"390\">";
            echo"<tr>
                    <td colspan='4' align=\"center\"></td>
                </tr>
                <tr> 
                    <td width=\"27\" align=\"left\" cellpadding=\"5\" ></td>
                    <td width=\"200\" align=\"left\" ></td>
                    <td width=\"137\" align=\"left\" ></td>
                    <td width=\"26\" align=\"center\" ></td> 
                </tr>";
            if($response[0]["cobroenvio_venta"] > 0 || $response[0]["cobroenvio_venta"] > 0){
                echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">SUBTOTAL:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>$".number_format($response[0]["total_venta"], 2, '.', ',')."</b></td></tr>";
            }
            
            
            if($response[0]["cobroenvio_venta"] > 0){
                echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">ENVÍO:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>$".number_format($response[0]["cobroenvio_venta"], 2, '.', ',')."</b></td></tr>";
            }
            if($response[0]["precioespecial_venta"] > 0){
                echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">DESCUENTO:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>- $".number_format($response[0]["precioespecial_venta"], 2, '.', ',')."</b></td></tr>";
            }
            
            echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">TOTAL:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>$".number_format($totaltotalVenta, 2, '.', ',')."</b></td></tr>";
            echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">PAGO CON:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>$".number_format($response[0]["pagocon_venta"], 2, '.', ',')."</b></td></tr>";
            echo " <tr><td></td><td></td><td width=\"200\" align=\"right\" style=\"font-size:10pt;\">CAMBIO:</td><td width=\"65\" align=\"right\" style=\"font-size:10pt;\"><b>$".number_format($response[0]["cambio_venta"], 2, '.', ',')."</b></td></tr>";
            echo "</table>";
        }

        if ($response[0]["tipo_venta"] !== '1') {
            echo "<table width=\"390\">";
                echo"
                <tr><td colspan='4' align=\"center\">****** DOMICILIO ******</td></tr>
                    <tr> 
                        <td width=\"80\" align=\"left\" cellpadding=\"5\" >CLIENTE:</td>
                        <td width=\"300\" align=\"left\" colspan=\"2\"><b>".htmlspecialchars($response[0]["nombre_cliente"])."</b></td>
                    </tr>
                    <tr>
                        <td align=\"left\" >DIRECCI&Oacute;N:</td>
                        <td align=\"left\" colspan='2'>".htmlspecialchars($response[0]["direccion_cliente"])."</td>
                    </tr>
                    <tr>
                        <td align=\"left\" >TEL&Eacute;FONO:</td>
                        <td align=\"left\" colspan='2'>".$response[0]["celular_cliente"]."</td>
                    </tr>
                    <tr>
                        <td align=\"left\" >COMENTARIOS:</td>
                        <td align=\"left\" colspan='2'>".$response[0]["comentario_cliente"]."</td>
                    </tr>
                    <tr>
                        <td align=\"left\" >OBSERVACIONES:</td>
                        <td align=\"left\" colspan='2'>".$response[0]["observaciones_venta"]."</td>
                    </tr>
                ";
            echo "</table>";
        }
        
    ?>

    <div align="center">
    ---------------------------------------------------------------------------------
        <div style="display: block;text-align: center;font-weight: bold;font-size: 9pt;">
            <img src="<?php echo RUTA_URL; ?>public/img/telefono_negro.png" style="width: 3%;margin-right: 3px;">(669) 215 8585<br>
        </div>
        <div style="display: block;text-align: center;font-weight: bold;font-size: 9pt;">
            <img src="<?php echo RUTA_URL; ?>public/img/facebook_negro.png" style="width: 4%;margin-right: 3px;">www.facebook.com/Helados-Alteña-1560283674282715/<br>
        </div>   
        <br>
        .     
    </div>
</div>
</body>