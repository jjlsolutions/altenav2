<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{
	
	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Concentrado extends Controlador
	{
		
		public function __construct()
		{

			$this->concentradoModelo = $this->modelo('ConcentradoModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('concentrado/Concentrado');
		}



		public function consultar()
		{
			$data = $this->concentradoModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function consultarCaConcentrado()
		{
			$data = $this->concentradoModelo->consultarCaConcentrado($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}


		public function formConcentrado()
		{

			$this->vista('concentrado/formConcentrado', $datos);
		}



		public function guardarConcentrado()
		{
			$datosCompletos = $this->validarDatosVaciosConcentradoGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{

				$rutaArchivo = "../public/img/concentrado";
				
				$fe = date('Ymd');
				$ho = date('His');
				$nomDoc = str_replace(' ', '', substr($_FILES['file']['name'],0,-(strlen(strrchr($_FILES['file']['name'],".")))).'_'.$fe.$ho);
			
				$path_parts = pathinfo($_FILES['file']['name']);
				$extension = $path_parts['extension'];
			
				$_FILES['file']['name'] = $nomDoc.'.'.$extension;
				$file = $_FILES['file'];
				$response = new StdClass();
				
				if (!is_dir($rutaArchivo))
					mkdir($rutaArchivo);
				
				opendir($rutaArchivo);
				$fileupload =  $file['name'];
				$filedestino = $rutaArchivo."/".$fileupload;
				$extension = explode(".", $filedestino);

				if (!file_exists($filedestino))
				{
					copy($file['tmp_name'], $filedestino);
					
					$response->success = true;
					$response->archivo = $fileupload;
				}
				else
				{
					$response->success = false;
					$response->error = 3;
					//'El archivo ya existe, favor de verificar.'
				}
			
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_deconcentrado= (int) (!empty($_POST['cve_deconcentrado']) && $_POST['cve_deconcentrado']!=null) ? $_POST['cve_deconcentrado']:'0';

				$datosConcentrado =  array (
									ban                    => 1,
									cve_deconcentrado           => $cve_deconcentrado,
									descripcion_deconcentrado         => $_POST["descripcion_deconcentrado"],
									imagen_deconcentrado      => $_FILES['file']['name'],
									total_deconcentrado        => $_POST["total_deconcentrado"],
									cvecaconcentrado_deconcentrado        => $_POST["cvecaconcentrado_deconcentrado"],
									cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->concentradoModelo->guardarConcentrado($datosConcentrado);

				
				if ($respuesta == true)
				{
					$msg = "Concentrado guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function guardarCaconcentrado()
		{
			//Preparamos en un array los datos que enviaremos a la BD
			
			$datosSabor =  array (
								ban                    => 1,
								cve_caconcentrado         => $_POST["cve_caconcentrado"],
								nombre_caconcentrado         => $_POST["nombre_caconcentrado"],
								cveusuario_accion      => $_SESSION["cve_usuario"]
								);
			
			$respuesta = $this->concentradoModelo->guardarCaconcentrado($datosSabor);

			
			if ($respuesta == true)
			{
				$msg = "Sabor guardado con Éxito.";
				$status = "success";
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}
				

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function validarDatosVaciosConcentradoGuardar($dataPost)
		{
			if(empty($dataPost["descripcion_deconcentrado"]) || !trim($dataPost["descripcion_deconcentrado"])){ $status = "vacio"; }
			elseif(empty($dataPost["total_deconcentrado"]) || !trim($dataPost["total_deconcentrado"])){ $status = "vacio"; }
			elseif(empty($dataPost["cvecaconcentrado_deconcentrado"]) || !trim($dataPost["cvecaconcentrado_deconcentrado"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function eliminarConcentrado()
		{
			$datosConcentrado =  array (
					cve_deconcentrado         => $_POST["cve_deconcentrado"]
						     );

			$respuesta = $this->concentradoModelo->eliminarConcentrado($datosConcentrado);

			if ($respuesta == true)
			{
				$rutaArchivo = "../public/img/concentrado";
				$ruta = $rutaArchivo."/".$_POST["imagen_deconcentrado"];
				unlink($ruta);
				
				$msg = "Concentrado eliminado.";
				
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>