<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Producto extends Controlador
	{
		
		public function __construct()
		{

			$this->productoModelo = $this->modelo('ProductoModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('producto/Producto');
		}



		public function consultar()
		{
			$data = $this->productoModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function consultarHistorialStock()
		{
			$data = $this->productoModelo->consultarHistorialStock($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function formProducto()
		{

			$this->vista('producto/formProducto', $datos);
		}



		public function guardarProducto()
		{
			$datosCompletos = $this->validarDatosVaciosProductoGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				
			//Preparamos en un array los datos que enviaremos a la BD
			    $cve_producto= (int) (!empty($_POST['cve_producto']) && $_POST['cve_producto']!=null) ? $_POST['cve_producto']:'0';
				//$cve_producto = (empty($cve_producto)) ? $_POST["cve_producto"] : 0 ;

				$datosProducto =  array (
									ban                    => 1,
									nombre_producto         => $_POST["nombre_producto"],
									preciomenudeo_producto      => $_POST["preciomenudeo_producto"],
									preciomayoreo_producto        => $_POST["preciomayoreo_producto"],
									cve_producto           => $cve_producto,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->productoModelo->guardarProducto($datosProducto);

				
				if ($respuesta == true)
				{
					$msg = "Producto guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function actualizarStockProducto()
		{
			//Preparamos en un array los datos que enviaremos a la BD
			    $cve_producto= (int) (!empty($_POST['cve_producto']) && $_POST['cve_producto']!=null) ? $_POST['cve_producto']:'0';
				//$cve_producto = (empty($cve_producto)) ? $_POST["cve_producto"] : 0 ;

				$datosProducto =  array (
									ban                    => 2,
									preciomayoreo_producto        => $_POST["cantidad_producto"],
									cve_producto           => $cve_producto,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->productoModelo->actualizarStockProducto($datosProducto);

				
				if ($respuesta == true)
				{
					$msg = "Producto guardado con Éxito.";
					$status = "success";
					$stock = $respuesta['stock'];
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}

			
			$envioDatos["status"] = $status;
			$envioDatos["stock"] = $stock;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function consultarStock()
		{
			//Preparamos en un array los datos que enviaremos a la BD
			    $cve_producto= (int) (!empty($_POST['cve_producto']) && $_POST['cve_producto']!=null) ? $_POST['cve_producto']:'0';
				//$cve_producto = (empty($cve_producto)) ? $_POST["cve_producto"] : 0 ;

				$datosProducto =  array (
									cve_producto           => $cve_producto,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->productoModelo->consultarStock($datosProducto);

				
				if ($respuesta == true)
				{
					$status = "success";
					$stock = $respuesta['stock'];
				}
				else
				{
					$status = "error";
				}

			
			$envioDatos["status"] = $status;
			$envioDatos["stock"] = $stock;
			echo json_encode($envioDatos);
			
		}

		public function validarDatosVaciosProductoGuardar($dataPost)
		{
			if(empty($dataPost["nombre_producto"]) || !trim($dataPost["nombre_producto"])){ $status = "vacio"; }
			elseif(empty($dataPost["preciomenudeo_producto"]) || !trim($dataPost["preciomenudeo_producto"])){ $status = "vacio"; }
			elseif(empty($dataPost["preciomayoreo_producto"]) || !trim($dataPost["preciomayoreo_producto"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearProducto()
		{
			$datosProducto =  array (
								ban                 => $_POST["ban"],
								cve_producto         => $_POST["cve_producto"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->productoModelo->bloquearProducto($datosProducto);

			if ($respuesta == true)
			{
				if ($datosProducto['ban'] == 2)
				{
					$msg = "Producto bloqueada.";
				}else{
					$msg = "Producto desbloqueada.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>