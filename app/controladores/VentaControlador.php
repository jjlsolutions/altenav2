<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Venta extends Controlador
	{
		
		public function __construct()
		{

			$this->ventaModelo = $this->modelo('VentaModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('venta/Venta');
		}



		public function consultar()
		{
			$data = $this->ventaModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function obtenerFolio()
		{
			$data = $this->ventaModelo->obtenerFolio($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function formVenta()
		{

			$this->vista('venta/formVenta', $datos);
		}



		public function guardarVenta()
		{
			
			$datos =  array (
				ban					=> 1,
				folo_venta			=> '',
				total_venta			=> $_POST["total_venta"],
				tipo_venta			=> $_POST["tipo_venta"],
				tipoprecio_venta	=> $_POST["tipoprecio_venta"],
				cvecliente_venta	=> $_POST["cvecliente_venta"],
				cveusuario_accion	=> $_SESSION["cve_usuario"],
				listaProductos		=> json_decode($_POST["listaProductos"]),
				cobroenvio_venta	=> $_POST["cobroenvio_venta"],
				observaciones		=>  $_POST["observaciones"],
				precioespecial_venta		=>  $_POST["precioespecial_venta"]
			 );
			
			$respuesta = $this->ventaModelo->guardarVenta($datos);
			
			if ($respuesta == true)
			{
				$msg = "Venta guardado con Éxito.";
				$status = "success";
				$cve_venta = $respuesta['cve_venta'];
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}
			

			$envioDatos["status"] = $status;
			$envioDatos["cve_venta"] = $cve_venta;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);

		}

		public function actializarStockDomicilio(){
			
			$datos =  array (
				ban					=> 1,
				cve_venta			=> $_POST["cve_venta"]
			 );
			$respuesta = $this->ventaModelo->actializarStockDomicilio($datos);
	
			$arrayrespuesta = explode("|", $respuesta);
			if ($arrayrespuesta[0] == "true")
			{
				$msg = "Éxito.";
				$status = "success";
			}
			else
			{
				$msg = "No hay suficientes ".$arrayrespuesta[1].".";
			}
			

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);

		}

		public function validarDatosVaciosVentaGuardar($dataPost)
		{
			if(empty($dataPost["nombre_venta"]) || !trim($dataPost["nombre_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["cveproducto_venta"]) || !trim($dataPost["cveproducto_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["clave_venta"]) || !trim($dataPost["clave_venta"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearVenta()
		{
			$datosVenta =  array (
								ban                 => $_POST["ban"],
								cve_venta         => $_POST["cve_venta"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->ventaModelo->bloquearVenta($datosVenta);

			if ($respuesta == true)
			{
				if ($datosVenta['ban'] == 2)
				{
					$msg = "Venta bloqueado.";
				}else{
					$msg = "Venta desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>