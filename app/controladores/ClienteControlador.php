<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Cliente extends Controlador
	{
		
		public function __construct()
		{

			$this->clienteModelo = $this->modelo('ClienteModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('cliente/Cliente');
		}



		public function consultar()
		{
			$data = $this->clienteModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formCliente()
		{

			$this->vista('cliente/formCliente', $datos);
		}



		public function guardarCliente()
		{
			$datosCompletos = $this->validarDatosVaciosClienteGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_cliente= (int) (!empty($_POST['cve_cliente']) && $_POST['cve_cliente']!=null) ? $_POST['cve_cliente']:'0';
				
				$datosCliente =  array (
									ban                    => 1,
									nombre_cliente         => $_POST["nombre_cliente"],
									direccion_cliente      => $_POST["direccion_cliente"],
									comentario_cliente        => $_POST["comentario_cliente"],
									celular_cliente       => $_POST["celular_cliente"],
									cve_cliente           => $cve_cliente,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->clienteModelo->guardarCliente($datosCliente);

				
				if ($respuesta == true)
				{
					$msg = "Cliente guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosClienteGuardar($dataPost)
		{
			if(empty($dataPost["nombre_cliente"]) || !trim($dataPost["nombre_cliente"])){ $status = "vacio"; }
			elseif(empty($dataPost["direccion_cliente"]) || !trim($dataPost["direccion_cliente"])){ $status = "vacio"; }
			elseif(empty($dataPost["comentario_cliente"]) || !trim($dataPost["comentario_cliente"])){ $status = "vacio"; }
			elseif(empty($dataPost["celular_cliente"]) || !trim($dataPost["celular_cliente"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearCliente()
		{
			$datosCliente =  array (
								ban                 => $_POST["ban"],
								cve_cliente         => $_POST["cve_cliente"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->clienteModelo->bloquearCliente($datosCliente);

			if ($respuesta == true)
			{
				if ($datosCliente['ban'] == 2)
				{
					$msg = "Cliente bloqueado.";
				}else{
					$msg = "Cliente desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>