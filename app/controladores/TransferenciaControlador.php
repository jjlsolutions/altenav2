<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Transferencia extends Controlador
	{
		
		public function __construct()
		{

			$this->transferenciaModelo = $this->modelo('TransferenciaModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('transferencia/Transferencia');
		}



		public function consultar()
		{
			$data = $this->transferenciaModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formVenta()
		{

			$this->vista('venta/formVenta', $datos);
		}



		public function guardarTransferencia()
		{
			$rutaArchivo = "../public/img/transferencia";
				
				$fe = date('Ymd');
				$ho = date('His');
				$nomDoc = str_replace(' ', '', substr($_FILES['file']['name'],0,-(strlen(strrchr($_FILES['file']['name'],".")))).'_'.$fe.$ho);
			
				$path_parts = pathinfo($_FILES['file']['name']);
				$extension = $path_parts['extension'];
			
				$_FILES['file']['name'] = $nomDoc.'.'.$extension;
				$file = $_FILES['file'];
				$response = new StdClass();
				
				if (!is_dir($rutaArchivo))
					mkdir($rutaArchivo);
				
				opendir($rutaArchivo);
				$fileupload =  $file['name'];
				$filedestino = $rutaArchivo."/".$fileupload;
				$extension = explode(".", $filedestino);

				if (!file_exists($filedestino))
				{
					copy($file['tmp_name'], $filedestino);
					
					$response->success = true;
					$response->archivo = $fileupload;
				}
				else
				{
					$response->success = false;
					$response->error = 3;
					//'El archivo ya existe, favor de verificar.'
				}
			
			$datos =  array (
				ban					=> 1,
				cvesucursalventa_transferencia	=> $_POST["cvesucursalventa_transferencia"],
				cveusuario_accion	=> $_SESSION["cve_usuario"],
				listaProductos		=> json_decode($_POST["listaProductos"]),
				observaciones_transferencia		=>  $_POST["observaciones_transferencia"],
				imagen      => $_FILES['file']['name']
			 );
			
			$respuesta = $this->transferenciaModelo->guardarTransferencia($datos);
			
			if ($respuesta > 0)
			{
				$msg = "Venta guardado con Éxito.";
				$status = "success";
				$cve_venta = $respuesta['cve_venta'];
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}
			

			$envioDatos["status"] = $status;
			$envioDatos["cve_venta"] = $cve_venta;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);

		}



		public function validarDatosVaciosVentaGuardar($dataPost)
		{
			if(empty($dataPost["nombre_venta"]) || !trim($dataPost["nombre_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["cveproducto_venta"]) || !trim($dataPost["cveproducto_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["clave_venta"]) || !trim($dataPost["clave_venta"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearVenta()
		{
			$datosVenta =  array (
								ban                 => $_POST["ban"],
								cve_venta         => $_POST["cve_venta"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->ventaModelo->bloquearVenta($datosVenta);

			if ($respuesta == true)
			{
				if ($datosVenta['ban'] == 2)
				{
					$msg = "Venta bloqueado.";
				}else{
					$msg = "Venta desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>