<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Permiso extends Controlador
	{
		
		public function __construct()
		{

			$this->permisoModelo = $this->modelo('PermisoModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('permiso/Permiso');
		}



		public function consultar()
		{
			$data = $this->permisoModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formPermiso()
		{

			$cve_perfil = (empty($cve_perfil)) ? $_POST["cve_perfil"] : 0 ;
			$datosPerfil =  array (
									cvePerfil => $cve_perfil
							     );

			$datos = $this->permisoModelo->obtenerOpcionesMenu($datosPerfil);

			$this->vista('permiso/formPermiso', $datos);
		}



		public function guardarPermiso()
		{
			$datosCompletos = $this->validarDatosVaciosPerfilGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_perfil= (int) (!empty($_POST['txtcvePerfil']) && $_POST['txtcvePerfil']!=null) ? $_POST['txtcvePerfil']:'0';

				$datosPerfil[0] =  array (
									ban               => 1,
									nombrePerfil      => $_POST["txtNombrePerfil"],
									descipcionPerfil  => $_POST["txtDescipcionPerfil"],
									cvePerfil         => $cve_perfil,
							     	cveusuario_accion => $_SESSION["cve_usuario"]
							     );

				$datosPerfil[1] = array (
                                    chk2_2              => $_POST["chk2_2"],
                                    chk2_3               => $_POST["chk2_3"],
                                    chk2_4             => $_POST["chk2_4"],
                                    chk2_5               => $_POST["chk2_5"],
                                    chk2_6             => $_POST["chk2_6"],
                                    chk2_7             => $_POST["chk2_7"],
                                    chk2_8             => $_POST["chk2_8"],
									chk2_9             => $_POST["chk2_9"],
									chk2_10             => $_POST["chk2_10"],
									chk2_12             => $_POST["chk2_12"],
									chk2_13             => $_POST["chk2_13"],
									chk2_14            => $_POST["chk2_14"],
									chk2_15            => $_POST["chk2_15"],
									chk2_16            => $_POST["chk2_16"],
									chk2_17            => $_POST["chk2_17"],
									chk2_18            => $_POST["chk2_18"]
							     );

				//print_r($datosPerfil[1]);
				
				$respuesta = $this->permisoModelo->guardarPermiso($datosPerfil);

				
				if ($respuesta == true)
				{
					$msg = "Perfil guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosPerfilGuardar($dataPost)
		{
			if(empty($dataPost["txtNombrePerfil"]) || !trim($dataPost["txtNombrePerfil"])){ $status = "vacio"; }
			elseif(empty($dataPost["txtDescipcionPerfil"]) || !trim($dataPost["txtDescipcionPerfil"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}
		
	}

}


?>