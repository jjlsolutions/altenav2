<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{
	
	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Concentradocajero extends Controlador
	{
		
		public function __construct()
		{

			$this->concentradocajeroModelo = $this->modelo('ConcentradocajeroModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('concentradocajero/Concentradocajero');
		}



		public function consultar()
		{
			$data = $this->concentradocajeroModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function consultarCaConcentrado()
		{
			$data = $this->concentradocajeroModelo->consultarCaConcentrado($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}


		public function formConcentradocajero()
		{

			$this->vista('concentradocajero/formConcentradocajero', $datos);
		}



		public function guardarConcentradocajero()
		{
			$datosCompletos = $this->validarDatosVaciosConcentradocajeroGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{

				$rutaArchivo = "../public/img/concentradocajero";
				
				$fe = date('Ymd');
				$ho = date('His');
				$nomDoc = str_replace(' ', '', substr($_FILES['file']['name'],0,-(strlen(strrchr($_FILES['file']['name'],".")))).'_'.$fe.$ho);
			
				$path_parts = pathinfo($_FILES['file']['name']);
				$extension = $path_parts['extension'];
			
				$_FILES['file']['name'] = $nomDoc.'.'.$extension;
				$file = $_FILES['file'];
				$response = new StdClass();
				
				if (!is_dir($rutaArchivo))
					mkdir($rutaArchivo);
				
				opendir($rutaArchivo);
				$fileupload =  $file['name'];
				$filedestino = $rutaArchivo."/".$fileupload;
				$extension = explode(".", $filedestino);

				if (!file_exists($filedestino))
				{
					copy($file['tmp_name'], $filedestino);
					
					$response->success = true;
					$response->archivo = $fileupload;
				}
				else
				{
					$response->success = false;
					$response->error = 3;
					//'El archivo ya existe, favor de verificar.'
				}
			
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_deconcentradocajero= (int) (!empty($_POST['cve_deconcentradocajero']) && $_POST['cve_deconcentradocajero']!=null) ? $_POST['cve_deconcentradocajero']:'0';

				$datosConcentradocajero =  array (
									ban                    => 1,
									cve_deconcentradocajero           => $cve_deconcentradocajero,
									descripcion_deconcentradocajero         => $_POST["descripcion_deconcentradocajero"],
									imagen_deconcentradocajero      => $_FILES['file']['name'],
									total_deconcentradocajero        => $_POST["total_deconcentradocajero"],
									cvecaconcentradocajero_deconcentradocajero        => $_POST["cvecaconcentradocajero_deconcentradocajero"],
									cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->concentradocajeroModelo->guardarConcentradocajero($datosConcentradocajero);

				
				if ($respuesta == true)
				{
					$msg = "Concentradocajero guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function guardarCaconcentradocajero()
		{
			//Preparamos en un array los datos que enviaremos a la BD
			
			$datosSabor =  array (
								ban                    => 1,
								cve_caconcentradocajero         => $_POST["cve_caconcentradocajero"],
								nombre_caconcentradocajero         => $_POST["nombre_caconcentradocajero"],
								cveusuario_accion      => $_SESSION["cve_usuario"]
								);
			
			$respuesta = $this->concentradocajeroModelo->guardarCaconcentradocajero($datosSabor);

			
			if ($respuesta == true)
			{
				$msg = "Sabor guardado con Éxito.";
				$status = "success";
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}
				

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function validarDatosVaciosConcentradocajeroGuardar($dataPost)
		{
			if(empty($dataPost["descripcion_deconcentradocajero"]) || !trim($dataPost["descripcion_deconcentradocajero"])){ $status = "vacio"; }
			elseif(empty($dataPost["total_deconcentradocajero"]) || !trim($dataPost["total_deconcentradocajero"])){ $status = "vacio"; }
			elseif(empty($dataPost["cvecaconcentradocajero_deconcentradocajero"]) || !trim($dataPost["cvecaconcentradocajero_deconcentradocajero"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function eliminarConcentradocajero()
		{
			$datosConcentradocajero =  array (
					cve_deconcentradocajero         => $_POST["cve_deconcentradocajero"]
						     );

			$respuesta = $this->concentradocajeroModelo->eliminarConcentradocajero($datosConcentradocajero);

			if ($respuesta == true)
			{
				$rutaArchivo = "../public/img/concentradocajero";
				$ruta = $rutaArchivo."/".$_POST["imagen_deconcentradocajero"];
				unlink($ruta);
				
				$msg = "Concentradocajero eliminado.";
				
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>