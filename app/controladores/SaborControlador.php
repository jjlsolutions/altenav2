<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Sabor extends Controlador
	{
		
		public function __construct()
		{

			$this->saborModelo = $this->modelo('SaborModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('sabor/Sabor');
		}



		public function consultar()
		{
			$data = $this->saborModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formSabor()
		{

			$this->vista('sabor/formSabor', $datos);
		}



		public function guardarSabor()
		{
			$datosCompletos = $this->validarDatosVaciosSaborGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_sabor= (int) (!empty($_POST['cve_sabor']) && $_POST['cve_sabor']!=null) ? $_POST['cve_sabor']:'0';

				$datosSabor =  array (
									ban                    => 1,
									nombre_sabor         => $_POST["nombre_sabor"],
									cveproducto_sabor      => $_POST["cveproducto_sabor"],
									clave_sabor        => $_POST["clave_sabor"],
									cve_sabor           => $cve_sabor,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->saborModelo->guardarSabor($datosSabor);

				
				if ($respuesta == true)
				{
					$msg = "Sabor guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosSaborGuardar($dataPost)
		{
			if(empty($dataPost["nombre_sabor"]) || !trim($dataPost["nombre_sabor"])){ $status = "vacio"; }
			elseif(empty($dataPost["cveproducto_sabor"]) || !trim($dataPost["cveproducto_sabor"])){ $status = "vacio"; }
			elseif(empty($dataPost["clave_sabor"]) || !trim($dataPost["clave_sabor"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearSabor()
		{
			$datosSabor =  array (
								ban                 => $_POST["ban"],
								cve_sabor         => $_POST["cve_sabor"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->saborModelo->bloquearSabor($datosSabor);

			if ($respuesta == true)
			{
				if ($datosSabor['ban'] == 2)
				{
					$msg = "Sabor bloqueado.";
				}else{
					$msg = "Sabor desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>