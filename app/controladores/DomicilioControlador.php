<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Domicilio extends Controlador
	{
		
		public function __construct()
		{

			$this->domicilioModelo = $this->modelo('DomicilioModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('domicilio/Domicilio');
		}



		public function consultar()
		{
			$data = $this->ventaModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}



		public function formVenta()
		{

			$this->vista('venta/formVenta', $datos);
		}



		public function guardarVenta()
		{
			$datosCompletos = $this->validarDatosVaciosVentaGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_venta= (int) (!empty($_POST['cve_venta']) && $_POST['cve_venta']!=null) ? $_POST['cve_venta']:'0';

				$datosVenta =  array (
									ban                    => 1,
									nombre_venta         => $_POST["nombre_venta"],
									cveproducto_venta      => $_POST["cveproducto_venta"],
									clave_venta        => $_POST["clave_venta"],
									cve_venta           => $cve_venta,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->ventaModelo->guardarVenta($datosVenta);

				
				if ($respuesta == true)
				{
					$msg = "Venta guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosVentaGuardar($dataPost)
		{
			if(empty($dataPost["nombre_venta"]) || !trim($dataPost["nombre_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["cveproducto_venta"]) || !trim($dataPost["cveproducto_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["clave_venta"]) || !trim($dataPost["clave_venta"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function bloquearVenta()
		{
			$datosVenta =  array (
								ban                 => $_POST["ban"],
								cve_venta         => $_POST["cve_venta"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->ventaModelo->bloquearVenta($datosVenta);

			if ($respuesta == true)
			{
				if ($datosVenta['ban'] == 2)
				{
					$msg = "Venta bloqueado.";
				}else{
					$msg = "Venta desbloqueado.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>