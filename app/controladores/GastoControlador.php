<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{
	
	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Gasto extends Controlador
	{
		
		public function __construct()
		{

			$this->gastoModelo = $this->modelo('GastoModelo');

		}



		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('gasto/Gasto');
		}



		public function consultar()
		{
			$data = $this->gastoModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function consultarCaGasto()
		{
			$data = $this->gastoModelo->consultarCaGasto($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}


		public function formGasto()
		{

			$this->vista('gasto/formGasto', $datos);
		}



		public function guardarGasto()
		{
			$datosCompletos = $this->validarDatosVaciosGastoGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{

				$rutaArchivo = "../public/img/gasto";
				
				$fe = date('Ymd');
				$ho = date('His');
				$nomDoc = str_replace(' ', '', substr($_FILES['file']['name'],0,-(strlen(strrchr($_FILES['file']['name'],".")))).'_'.$fe.$ho);
			
				$path_parts = pathinfo($_FILES['file']['name']);
				$extension = $path_parts['extension'];
			
				$_FILES['file']['name'] = $nomDoc.'.'.$extension;
				$file = $_FILES['file'];
				$response = new StdClass();
				
				if (!is_dir($rutaArchivo))
					mkdir($rutaArchivo);
				
				opendir($rutaArchivo);
				$fileupload =  $file['name'];
				$filedestino = $rutaArchivo."/".$fileupload;
				$extension = explode(".", $filedestino);

				if (!file_exists($filedestino))
				{
					copy($file['tmp_name'], $filedestino);
					
					$response->success = true;
					$response->archivo = $fileupload;
				}
				else
				{
					$response->success = false;
					$response->error = 3;
					//'El archivo ya existe, favor de verificar.'
				}
			
				//Preparamos en un array los datos que enviaremos a la BD
				$cve_degasto= (int) (!empty($_POST['cve_degasto']) && $_POST['cve_degasto']!=null) ? $_POST['cve_degasto']:'0';

				$datosGasto =  array (
									ban                    => 1,
									cve_degasto           => $cve_degasto,
									descripcion_degasto         => $_POST["descripcion_degasto"],
									imagen_degasto      => $_FILES['file']['name'],
									total_degasto        => $_POST["total_degasto"],
									cvecagasto_degasto        => $_POST["cvecagasto_degasto"],
									cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->gastoModelo->guardarGasto($datosGasto);

				
				if ($respuesta == true)
				{
					$msg = "Gasto guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function guardarCagasto()
		{
			//Preparamos en un array los datos que enviaremos a la BD
			
			$datosSabor =  array (
								ban                    => 1,
								cve_cagasto         => $_POST["cve_cagasto"],
								nombre_cagasto         => $_POST["nombre_cagasto"],
								cveusuario_accion      => $_SESSION["cve_usuario"]
								);
			
			$respuesta = $this->gastoModelo->guardarCagasto($datosSabor);

			
			if ($respuesta == true)
			{
				$msg = "Sabor guardado con Éxito.";
				$status = "success";
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}
				

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}

		public function validarDatosVaciosGastoGuardar($dataPost)
		{
			if(empty($dataPost["descripcion_degasto"]) || !trim($dataPost["descripcion_degasto"])){ $status = "vacio"; }
			elseif(empty($dataPost["total_degasto"]) || !trim($dataPost["total_degasto"])){ $status = "vacio"; }
			elseif(empty($dataPost["cvecagasto_degasto"]) || !trim($dataPost["cvecagasto_degasto"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}



		public function eliminarGasto()
		{
			$datosGasto =  array (
					cve_degasto         => $_POST["cve_degasto"]
						     );

			$respuesta = $this->gastoModelo->eliminarGasto($datosGasto);

			if ($respuesta == true)
			{
				$rutaArchivo = "../public/img/gasto";
				$ruta = $rutaArchivo."/".$_POST["imagen_degasto"];
				unlink($ruta);
				
				$msg = "Gasto eliminado.";
				
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>