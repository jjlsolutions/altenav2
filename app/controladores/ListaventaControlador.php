<?php
session_start();

if ($_SESSION["cve_usuario"] == "")
{
	header("Location:Login");
}
else
{

	//Heredamos Controlador para poder tener acceso al método modelo y método vista
	class Listaventa extends Controlador
	{
		
		public function __construct()
		{

			$this->listaventaModelo = $this->modelo('ListaventaModelo');

		}

		//Todo controlador debe tener un metodo index
		public function index()
		{
			$this->vista('listaventa/Listaventa');
		}

		public function mostrarTiket()
		{

			$this->vista('listaventa/Ticket', $datos);
		}

		public function consultar()
		{
			$data = $this->listaventaModelo->consultar($_POST);

			$envioDatos["arrayDatos"] = $data;

			echo json_encode($envioDatos);
		}

		public function guardarListaventa()
		{
			$datosCompletos = $this->validarDatosVaciosListaventaGuardar($_POST);

			if ($datosCompletos == "vacio")
			{
				$status = "error";
				$msg = "Favor de revisar el formulario, hay campos requeridos vacios.";
			}
			else
			{
				
			//Preparamos en un array los datos que enviaremos a la BD
			    $cve_venta= (int) (!empty($_POST['cve_venta']) && $_POST['cve_venta']!=null) ? $_POST['cve_venta']:'0';
				//$cve_venta = (empty($cve_venta)) ? $_POST["cve_venta"] : 0 ;

				$datosListaventa =  array (
									ban                    => 1,
									nombre_venta         => $_POST["nombre_venta"],
									preciomenudeo_venta      => $_POST["preciomenudeo_venta"],
									preciomayoreo_venta        => $_POST["preciomayoreo_venta"],
									cve_venta           => $cve_venta,
							     	cveusuario_accion      => $_SESSION["cve_usuario"]
							     );
				
				$respuesta = $this->listaventaModelo->guardarListaventa($datosListaventa);

				
				if ($respuesta == true)
				{
					$msg = "Listaventa guardado con Éxito.";
					$status = "success";
				}
				else
				{
					$msg = "Hubo un error al guardar el registro.";
					$status = "error";
				}
				
			}

			
			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
			
		}



		public function validarDatosVaciosListaventaGuardar($dataPost)
		{
			if(empty($dataPost["nombre_venta"]) || !trim($dataPost["nombre_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["preciomenudeo_venta"]) || !trim($dataPost["preciomenudeo_venta"])){ $status = "vacio"; }
			elseif(empty($dataPost["preciomayoreo_venta"]) || !trim($dataPost["preciomayoreo_venta"])){ $status = "vacio"; }
			else{
				$status = "completo";
			}

			return $status;
		}

		public function bloquearListaventa()
		{
			$datosListaventa =  array (
								ban                 => $_POST["ban"],
								cve_venta         => $_POST["cve_venta"],
								tipo_venta         => $_POST["tipo_venta"],
								encamino         => $_POST["encamino"],
								motivo         => $_POST["motivo"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->listaventaModelo->bloquearListaventa($datosListaventa);

			if ($respuesta == true)
			{
				if ($datosListaventa['ban'] == 2)
				{
					$msg = "Listaventa bloqueada.";
				}else{
					$msg = "Listaventa desbloqueada.";
				}
				$status = "success";
			}
			else
			{
				//Este error se presenta por un error en el query
				$msg = "Hubo un error al bloquear el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}

		public function completarVenta()
		{
			$datosListaventa =  array (
								ban                 => $_POST["ban"],
								cve_venta         => $_POST["cve_venta"],
								pagocon_venta         => $_POST["pagocon_venta"],
								cambio_venta         => $_POST["cambio_venta"],
								cveusuario_accion   => $_SESSION["cve_usuario"]
						     );

			$respuesta = $this->listaventaModelo->completarVenta($datosListaventa);

			if ($respuesta == true)
			{
				$msg = "Listaventa guardado con Éxito.";
				$status = "success";
			}
			else
			{
				$msg = "Hubo un error al guardar el registro.";
				$status = "error";
			}

			$envioDatos["status"] = $status;
			$envioDatos["msg"] = $msg;
			echo json_encode($envioDatos);
		}
		
	}

}


?>