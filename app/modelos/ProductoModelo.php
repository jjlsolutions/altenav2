<?php

class ProductoModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_producto = (!empty($datosFiltrados['cve_producto']) || $datosFiltrados['cve_producto']!=null) ? $datosFiltrados['cve_producto'] : '0';
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL obtenProducto('$ban','$cve_producto','$cvesucursal_usuario')";
        //echo $query;

        $c_producto = $this->conexion->query($query);
        $r_producto = $this->conexion->consulta_array($c_producto);

        return $r_producto;
    }

    public function consultarHistorialStock($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $filtro = $_SESSION["cvesucursal_usuario"] > 0 ? ' where cu.cvesucursal_usuario = '.$_SESSION["cvesucursal_usuario"].' '  : '';
        
        $query = "SELECT
                    case when hs.cantidad_historialstock >= 0 then 
                        concat('El usuario: ',concat(cu.nombre_usuario, ' ', cu.apellidop_usuario, ' ', cu.apellidom_usuario ),' agregó ',hs.cantidad_historialstock,' ',cp.nombre_producto,if(hs.tipo_historialstock is null, '', concat(' de tipo ',hs.tipo_historialstock)))
                    else 
                        concat('El usuario: ',concat(cu.nombre_usuario, ' ', cu.apellidop_usuario, ' ', cu.apellidom_usuario ),' disminuyó ',hs.cantidad_historialstock,' ',cp.nombre_producto,if(hs.tipo_historialstock is null, '', concat(' de tipo ',hs.tipo_historialstock)))
                        end as comentario,
                    hs.*,
                    cp.*,
                    cu.*,
                    if(cs.nombre_sucursal is null , 'SYSADMIN', cs.nombre_sucursal) as nombre_sucursal 
                from
                    historial_stock hs
                inner join ca_productos cp on
                    hs.cveproducto_historialstock = cp.cve_producto
                inner join ca_usuario cu on
                    hs.cveusuariomod_historialstock = cu.cve_usuario
                left join ca_sucursales cs on cu.cvesucursal_usuario = cs.cve_sucursal
                $filtro 
                order by hs.cve_historialstock desc ;";
        //echo $query;

        $c_producto = $this->conexion->query($query);
        $r_producto = $this->conexion->consulta_array($c_producto);

        return $r_producto;
    }

    public function guardarProducto($datosProducto)
    {

        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $ban                    = $datosFiltrados['ban'];
        $nombre_producto         = $datosFiltrados['nombre_producto'];
        $preciomenudeo_producto          = $datosFiltrados['preciomenudeo_producto'];
        $preciomayoreo_producto          = $datosFiltrados['preciomayoreo_producto'];
        $cveProducto            = $datosFiltrados['cve_producto'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL guardarProducto(
                                    '$ban',
                                    '$cveProducto',
                                    '$nombre_producto',
                                    '$preciomayoreo_producto',
                                    '$preciomenudeo_producto',
                                    '$cvesucursal_usuario',
                                    '$cveusuario_accion',
                                    ''
                                    )";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }

    public function actualizarStockProducto($datosProducto)
    {

        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $ban                    = $datosFiltrados['ban'];
        $preciomayoreo_producto          = $datosFiltrados['preciomayoreo_producto'];
        $cveProducto            = $datosFiltrados['cve_producto'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL guardarProducto(
                                    '$ban',
                                    '$cveProducto',
                                    '',
                                    '$preciomayoreo_producto',
                                    '',
                                    '$cvesucursal_usuario',
                                    '$cveusuario_accion',
                                    'INVENTARIO'
                                    )";

        $c_perfil = $this->conexion->query($query) or die ($this->conexion->error());
        $r_perfil = $this->conexion->consulta_assoc($c_perfil);

        
        
        $this->conexion->close_conexion();
        
        return $r_perfil;

    }

    public function consultarStock($datosProducto)
    {

        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $cveProducto            = $datosFiltrados['cve_producto'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "select * from ca_productos where cve_producto = $cveProducto and cvesucursal_producto = $cvesucursal_usuario;";

        $c_perfil = $this->conexion->query($query) or die ($this->conexion->error());
        $r_perfil = $this->conexion->consulta_assoc($c_perfil);

        
        
        $this->conexion->close_conexion();
        
        return $r_perfil;

    }

    public function bloquearProducto($datosProducto)
    {
        $datosFiltrados = $this->filtrarDatos($datosProducto);

        $ban               = $datosFiltrados['ban'];
        $cve_producto        = $datosFiltrados['cve_producto'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarProducto('$ban','$cve_producto','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>