<?php
date_default_timezone_set("America/Mazatlan");
class TransferenciaModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_transferencia = (!empty($datosFiltrados['cve_transferencia']) || $datosFiltrados['cve_transferencia']!=null) ? $datosFiltrados['cve_transferencia'] : '0';
        $estatus_transferencia = (!empty($datosFiltrados['estatus_transferencia']) || $datosFiltrados['estatus_transferencia']!=null) ? $datosFiltrados['estatus_transferencia'] : '';
         $responseconsultar = new StdClass();
        if($ban == 1){
            if($_SESSION["cvesucursal_usuario"] == 0){
                $query = "SELECT *,
    cu.nombre_sucursal as nombre_sucursaldestino,
    cs.nombre_sucursal as nombre_sucursalorigen from ma_transferencias mi 
            inner join ca_sucursales cu on cu.cve_sucursal = mi.cvesucursalventa_transferencia 
            inner join ca_sucursales cs on cs.cve_sucursal = mi.cvesucursal_transferencia 
            inner join ca_usuario cu2 on mi.cveusuarioadd_transferencia = cu2.cve_usuario
            order by cve_transferencia desc;";
            }else if($_SESSION["cvesucursal_usuario"] == 1){
                $query = "SELECT *,
    cu.nombre_sucursal as nombre_sucursaldestino,
    cs.nombre_sucursal as nombre_sucursalorigen from ma_transferencias mi 
            inner join ca_sucursales cu on cu.cve_sucursal = mi.cvesucursalventa_transferencia 
            inner join ca_sucursales cs on cs.cve_sucursal = mi.cvesucursal_transferencia 
            inner join ca_usuario cu2 on mi.cveusuarioadd_transferencia = cu2.cve_usuario
            where cs.cve_sucursal = ".$_SESSION["cvesucursal_usuario"]." order by cve_transferencia desc;";
            }else{
                $query = "";
            }
            //echo $query;
    
            $c_venta = $this->conexion->query($query);
            $responseconsultar = $this->conexion->consulta_array($c_venta);
        }
        else if($ban == 2){
               
            $result = false;

            
            $query = "select * from ma_transferencias mt 
                    inner join de_transferencia dt on mt.cve_transferencia = dt.cvetransferencia_detransferencia 
                    inner join ca_productos cp on  dt.cvesp_detransferencia = cp.cve_producto 
                    where dt.cvetransferencia_detransferencia =".$cve_transferencia.";";

            $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
            $this->conexion->next_result();

            $query2 = "select sum(di.cantidad_detransferencia) as total_cantidad, sum(cp.stock) as total_stock from de_transferencia di
                        inner join ca_productos cp on cp.cve_producto = di.cvesp_detransferencia
                        where cvetransferencia_detransferencia = ".$cve_transferencia.";";

            $respuestatotal = $this->conexion->query($query2) or die ($this->conexion->error());
            $total_productos = $respuestatotal->fetch_object();

            if($total_productos->total_cantidad <= $total_productos->total_stock){
                while ($valor = $respuesta->fetch_object()) {
                    $cveusuario_accion = $_SESSION["cve_usuario"];
                    $this->conexion->next_result();
                    $queryS =  "select (select stock  from ca_productos where nombre_producto = '".$valor->nombre_producto."' and cvesucursal_producto = ".$valor->cvesucursalventa_transferencia.") + ".$valor->cantidad_detransferencia."  as stock;";
                    
                    $respuestaStock = $this->conexion->query($queryS) or die ($this->conexion->error());
                    $stock_productos = $respuestaStock->fetch_object();

                     $this->conexion->next_result();

                    $queryS =  "select (select cve_producto  from ca_productos where nombre_producto = '".$valor->nombre_producto."' and cvesucursal_producto = ".$valor->cvesucursalventa_transferencia.")  as cve_producto;";    
                   
                    $respuestacve_producto = $this->conexion->query($queryS) or die ($this->conexion->error());
                    $cve_producto_productos = $respuestacve_producto->fetch_object();

                    $this->conexion->next_result();

                    if($stock_productos->stock == null || $cve_producto_productos->cve_producto == null){
                        
                        $responseconsultar->success = false;
                        $responseconsultar->msj = 'Uno de los productos no existe en esa sucursal';
                        return $responseconsultar;
                    }
                }

                $query = "select * from ma_transferencias mt 
                    inner join de_transferencia dt on mt.cve_transferencia = dt.cvetransferencia_detransferencia 
                    inner join ca_productos cp on  dt.cvesp_detransferencia = cp.cve_producto 
                    where dt.cvetransferencia_detransferencia =".$cve_transferencia.";";

                $respuesta2 = $this->conexion->query($query) or die ($this->conexion->error());
                $this->conexion->next_result();

                while ($valor2 = $respuesta2->fetch_object()) {
                
                    $cveusuario_accion = $_SESSION["cve_usuario"];

                    $queryS =  'select (select stock  from ca_productos where cve_producto = '.$valor2->cvesp_detransferencia.') - '.$valor2->cantidad_detransferencia.'  as stock;';
                    $respuestaStock = $this->conexion->query($queryS) or die ($this->conexion->error());
                    $this->conexion->next_result();

                    $stock_productos = $respuestaStock->fetch_object();
                    $query = "UPDATE ca_productos 
                            SET 
                            stock = $stock_productos->stock  
                        WHERE cve_producto = $valor2->cvesp_detransferencia;";
                    $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
                    $respuestas = $this->conexion->query($query);
                    $this->conexion->next_result();
                    /////////////////////
                    $queryS =  "select (select stock  from ca_productos where nombre_producto = '".$valor2->nombre_producto."' and cvesucursal_producto = ".$valor2->cvesucursalventa_transferencia.") + ".$valor2->cantidad_detransferencia."  as stock;";
                    
                    $respuestaStock = $this->conexion->query($queryS) or die ($this->conexion->error());
                    $stock_productos = $respuestaStock->fetch_object();

                     $this->conexion->next_result();

                    $queryS =  "select (select cve_producto  from ca_productos where nombre_producto = '".$valor2->nombre_producto."' and cvesucursal_producto = ".$valor2->cvesucursalventa_transferencia.")  as cve_producto;";                    
                    $respuestacve_producto = $this->conexion->query($queryS) or die ($this->conexion->error());
                    $cve_producto_productos = $respuestacve_producto->fetch_object();
                    $this->conexion->next_result();
                    $query = "UPDATE ca_productos 
                            SET 
                            stock = $stock_productos->stock  
                        WHERE cve_producto = $cve_producto_productos->cve_producto ;";
                    $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
                  
                    $respuestas = $this->conexion->query($query);
                    $this->conexion->next_result();

                }

                $query = "UPDATE ma_transferencias 
                SET 
                    estatus_transferencia = ".$estatus_transferencia."
                WHERE cve_transferencia = ".$cve_transferencia.";";
                $c_venta = $this->conexion->query($query);
                $r_venta = $this->conexion->consulta_array($c_venta);

              
                $responseconsultar->success = true;
            }

        }
        else if($ban == 3){
            $query = "DELETE from ma_transferencias WHERE cve_transferencia = ".$cve_transferencia.";";

            $c_venta = $this->conexion->query($query);
            $responseconsultar = $this->conexion->consulta_array($c_venta);
        }else if($ban == 4){
            if($_SESSION["cvesucursal_usuario"] == 0){
                $query = "SELECT * from ma_transferencias mi 
            inner join de_transferencia di on di.cvetransferencia_detransferencia = mi.cve_transferencia 
            inner join ca_productos cp on cp.cve_producto = di.cvesp_detransferencia 
            where mi.cve_transferencia = ".$cve_transferencia." order by cve_transferencia desc;";
            }else{
                $query = "SELECT * from ma_transferencias mi 
            inner join de_transferencia di on di.cvetransferencia_detransferencia = mi.cve_transferencia 
            inner join ca_productos cp on cp.cve_producto = di.cvesp_detransferencia 
            where mi.cvesucursal_transferencia = ".$_SESSION["cvesucursal_usuario"]." and mi.cve_transferencia = ".$cve_transferencia." order by cve_transferencia desc;";
            }
            //echo $query;
        
            $c_venta = $this->conexion->query($query);
            $responseconsultar = $this->conexion->consulta_array($c_venta);
        }

        return $responseconsultar;
    }



    public function guardarTransferencia($datosVenta)
    {

        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban                = $datosFiltrados['ban'];
        $cvesucursalventa_transferencia   = $datosFiltrados['cvesucursalventa_transferencia'];
        $cveusuario_accion  = $datosFiltrados['cveusuario_accion'];
        $listaProductos     = $datosVenta['listaProductos'];
        $observaciones_transferencia     = $datosVenta['observaciones_transferencia'];
        $imagen   = $datosFiltrados['imagen'];
        $fecha              = date("Y-m-d H:i:s");
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "INSERT INTO ma_transferencias (
                                    cvesucursalventa_transferencia,
                                    cvesucursal_transferencia,
                                    observaciones_transferencia,
                                    imagen_transferencia,
                                    cveusuarioadd_transferencia
                                ) VALUES (
                                    $cvesucursalventa_transferencia,
                                    $cvesucursal_usuario,
                                    '$observaciones_transferencia',
                                    '$imagen',
                                    $cveusuario_accion
                                );";
       // echo $query;                 

        $c_perfil = $this->conexion->query($query) or die ($this->conexion->error());
        $insert_id = $this->conexion->mysqli_insert_id();
        $ultima_cve = $insert_id;
        
        $this->conexion->next_result();
        foreach($listaProductos as $valor){
                
                $query = "INSERT INTO de_transferencia
                            (
                            cvetransferencia_detransferencia,
                            cvesp_detransferencia,
                            cantidad_detransferencia
                            ) VALUES (
                                $ultima_cve,
                                $valor->cvesp_venta,
                                $valor->cantidad_venta
                            );";
                //echo $query;
                $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
                $this->conexion->next_result();
            
        }

        $this->conexion->close_conexion();
        return $insert_id;
    }



    public function bloquearVenta($datosVenta)
    {
        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarVenta('$ban','$cve_venta','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>