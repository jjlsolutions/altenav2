<?php
date_default_timezone_set("America/Mazatlan");
//date("Y-m-d H:i:s");
class VentaModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_venta = (!empty($datosFiltrados['cve_venta']) || $datosFiltrados['cve_venta']!=null) ? $datosFiltrados['cve_venta'] : '0';

        $query = "CALL obtenVentaes('$ban','$cve_venta')";
        //echo $query;

        $c_venta = $this->conexion->query($query);
        $r_venta = $this->conexion->consulta_array($c_venta);

        return $r_venta;
    }

    public function obtenerFolio($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_venta = (!empty($datosFiltrados['cve_venta']) || $datosFiltrados['cve_venta']!=null) ? $datosFiltrados['cve_venta'] : '0';

        $query = "CALL obtenerFolio('$ban','$cve_venta')";
        //echo $query;

        $c_venta = $this->conexion->query($query);
        $r_venta = $this->conexion->consulta_array($c_venta);

        return $r_venta;
    }

    public function guardarVenta($datosVenta)
    {

        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban                = $datosFiltrados['ban'];
        $folo_venta         = $datosFiltrados['folo_venta'];
        $total_venta        = $datosFiltrados['total_venta'];
        $tipo_venta         = $datosFiltrados['tipo_venta'];
        $tipoprecio_venta   = $datosFiltrados['tipoprecio_venta'];
        $cvecliente_venta   = $datosFiltrados['cvecliente_venta'];
        $cveusuario_accion  = $datosFiltrados['cveusuario_accion'];
        $listaProductos     = $datosVenta['listaProductos'];
        $cobroenvio_venta     = $datosVenta['cobroenvio_venta'];
        $observaciones     = $datosVenta['observaciones'];
        $precioespecial_venta = $datosVenta['precioespecial_venta'];
        $fecha              = date("Y-m-d H:i:s");
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL generarFolio(
                                    '$ban',
                                    '$folo_venta',
                                    '$total_venta',
                                    '$tipo_venta',
                                    '$tipoprecio_venta',
                                    '$cvecliente_venta',
                                    '$cobroenvio_venta',
                                    '$cvesucursal_usuario',
                                    '$observaciones',
                                    '$precioespecial_venta',
                                    '$fecha',
                                    '$cveusuario_accion'
                                    )";
                                    

        $c_perfil = $this->conexion->query($query) or die ($this->conexion->error());
        $r_perfil = $this->conexion->consulta_assoc($c_perfil);

        $ultima_cve = $r_perfil['cve_venta'];
        
        $this->conexion->next_result();
        $ban = 0;
        if($tipo_venta == 1){
            $ban = 2; //venta
        }
        else{
            $ban = 3; // domicilio
        }
        foreach($listaProductos as $valor){
            $v_negativo = -$valor->cantidad_venta;

            if($ban == 2){
                $query = "CALL guardarProducto(
                    '2',
                    '$valor->cvesp_venta',
                    '',
                    '$v_negativo',
                    '',
                    '0',
                    '$cveusuario_accion',
                    'VENTA'
                    )";
    
                $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
                    $this->conexion->next_result();
            }
            
                $query = "CALL guardarDetalleVenta(
                    '1',
                    '$ultima_cve',
                    '$valor->cvesp_venta',
                    '$valor->cantidad_venta',
                    '$valor->precio_venta'
                    )";
                $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
                $this->conexion->next_result();
            
        }

        $this->conexion->close_conexion();
        return $r_perfil;
    }

    public function actializarStockDomicilio($datosVenta)
    {
        $datosFiltrados = $this->filtrarDatos($datosVenta);
        $result = false;
        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];

            
        $query = "Select * from de_venta where cveventa_venta = ".$cve_venta.";";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        $this->conexion->next_result();

        $query2 = "select
                        sum(dv.cantidad_venta) as total_cantidad,
                        cp.stock as total_stock,
                        cp.nombre_producto,
                        case
                            when sum(dv.cantidad_venta) < cp.stock then '1'
                            else '0'
                        end as resultado
                    from
                        de_venta dv
                    inner join ca_sabores cs on
                        cs.cve_sabor = dv.cvesp_venta
                    inner join ca_productos cp on
                        cp.cve_producto = cs.cveproducto_sabor
                    where
                        cveventa_venta = ".$cve_venta."
                    group by
                        cp.cve_producto having resultado < 1;";

        $respuestatotal = $this->conexion->query($query2) or die ($this->conexion->error());
        $total_productos = $respuestatotal->fetch_object();
        $countador = count($total_productos);

        if($countador == 0){
            while ($valor = $respuesta->fetch_object()) {
                $v_negativo = gmp_neg($valor->cantidad_venta);
                $cveusuario_accion = $_SESSION["cve_usuario"];
                $query = "CALL guardarProducto(
                    '3',
                    '$valor->cvesp_venta',
                    '',
                    '$v_negativo',
                    '',
                    '0',
                    '$cveusuario_accion',
                    'DOMICILIO'
                    )";
    
                $respuestas = $this->conexion->query($query);
                $this->conexion->next_result();
            }

            $query = "UPDATE ma_ventas 
                        SET 
                        encamino_venta = 2,
                        cveusuarioencamino_venta = $cveusuario_accion,
                        fechaaddencamino_venta = NOW()
                    WHERE cve_venta = $cve_venta;";
            $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
            $result = "true|0";
        }
        else{
            $valor = array();
            $valor[] = $total_productos->nombre_producto;
            while ($valor_nombre = $respuestatotal->fetch_object()) {
                $valor[] = $valor_nombre->nombre_producto;
            } 
            $result = "false|".implode( ',', $valor );
        }

        //$this->conexion->next_result();
        

        return $result;
    }

    public function bloquearVenta($datosVenta)
    {
        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarVenta('$ban','$cve_venta','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>