<?php

class ConcentradocajeroModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $fecha_concentradocajero = (!empty($datosFiltrados['fecha_concentradocajero']) || $datosFiltrados['fecha_concentradocajero']!=null) ? $datosFiltrados['fecha_concentradocajero'] : '0';
        $sucursal_concentradocajero = (!empty($datosFiltrados['sucursal_concentradocajero']) || $datosFiltrados['sucursal_concentradocajero']!=null) ? $datosFiltrados['sucursal_concentradocajero'] : '0';
        $fecha_concentradocajero = explode(" - ",$fecha_concentradocajero);
        $fecha1 = $fecha_concentradocajero[0];
        $fecha2 = $fecha_concentradocajero[1];
        $cveusuario_concentradocajero = (!empty($datosFiltrados['cveusuario_concentradocajero']) || $datosFiltrados['cveusuario_concentradocajero']!=null) ? $datosFiltrados['cveusuario_concentradocajero'] : '0';

        $filtro = '';

        if($cveusuario_concentradocajero !=0){
            $filtro = 'and mv.cveusuariomod_venta = '.$cveusuario_concentradocajero;
        }
        $query = "select
                    concat(cu.nombre_usuario, ' ', cu.apellidop_usuario, ' ', cu.apellidom_usuario) as nombrecompleto_venta,
                    sum(mv.total_venta) as total_venta,
                    sum(mv.total_venta - mv.precioespecial_venta) as totalcaja_venta,
                    sum(mv.precioespecial_venta) as precioespecial_venta,
                    COUNT(IF(mv.tipo_venta  = 1, 1, NULL)) as  cantidad_venta_mostrador,
                    COUNT(IF(mv.tipo_venta  = 2, 1, NULL)) as  cantidad_venta_domicilio, 
                    COUNT(IF(mv.tipo_venta  = 1  and mv.tipoprecio_venta = 1 , 1, NULL)) as  cantidad_mostrador_menudeo,
                    sum(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  total_mostrador_menudeo,
                    COUNT(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  cantidad_mostrador_mayoreo,
                    sum(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  total_mostrador_mayoreo,
                    COUNT(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  cantidad_domicilio_menudeo,
                    sum(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  total_domicilio_menudeo,
                    COUNT(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  cantidad_domicilio_mayoreo,
                    sum(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  total_domicilio_mayoreo,
                    cs.nombre_sucursal,
                    cp.nombre_perfil 
                from
                    ma_ventas mv
                inner join ca_usuario cu on
                    mv.cveusuariomod_venta = cu.cve_usuario
                inner join ca_sucursales cs on
                    cu.cvesucursal_usuario = cs.cve_sucursal
                inner join ca_perfil cp on
                    cu.cveperfil_usuario = cp.cve_perfil
                where
                    mv.cvesucursal_venta = $sucursal_concentradocajero
                    and mv.estatus_venta = 4 $filtro and date_format(mv.fechamod_venta , '%Y-%m-%d') between date_format('$fecha1' , '%Y-%m-%d') and date_format('$fecha2' , '%Y-%m-%d')
                group by
                    mv.cveusuariomod_venta
                order by cs.nombre_sucursal desc, cu.nombre_usuario asc";
        //echo $query;

        $c_concentradocajero = $this->conexion->query($query);
        $r_concentradocajero = $this->conexion->consulta_array($c_concentradocajero);

        return $r_concentradocajero;
    }

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>