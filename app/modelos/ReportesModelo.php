<?php

class ReportesModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }

	public function consultarVentasMesMostrador($mes_numero)
	{
		$query = "set lc_time_names = 'es_MX';";

        $c_puesto = $this->conexion->query($query);
		$this->conexion->next_result();

        $query = "select
					MONTH(fechamod_venta) as mes_numero,
					UPPER(mes) as mes,
					cp.nombre_producto,
					sum(cantidad_venta) as cantidad_venta,
					sum(total) as total
				from
					(
					SELECT
						mv.fechamod_venta,
						MONTHNAME(mv.fechamod_venta) as mes,
						dv.*,
						(dv.cantidad_venta * dv.precio_venta) as total
					FROM
						ma_ventas mv
					inner join de_venta dv on
						mv.cve_venta = dv.cveventa_venta
					WHERE
						mv.total_venta > 0
						and year(mv.fechamod_venta) = '2022'
						and mv.estatus_venta = 4
						and mv.cvesucursal_venta = 1
						and mv.tipo_venta = 1) as tbl
						INNER JOIN ca_productos cp on tbl.cvesp_venta = cp.cve_producto
				group by
					mes,
					cvesp_venta having mes_numero =".$mes_numero.";";

        $c_puesto = $this->conexion->query($query);
        $r_puesto = $this->conexion->consulta_array($c_puesto);

        return $r_puesto;
	}
	
}

?>