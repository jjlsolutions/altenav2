<?php

class ConcentradoModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $fecha_concentrado = (!empty($datosFiltrados['fecha_concentrado']) || $datosFiltrados['fecha_concentrado']!=null) ? $datosFiltrados['fecha_concentrado'] : '0';
        $sucursal_concentrado = (!empty($datosFiltrados['sucursal_concentrado']) || $datosFiltrados['sucursal_concentrado']!=null) ? $datosFiltrados['sucursal_concentrado'] : '0';
        $fecha_concentrado = explode(" - ",$fecha_concentrado);
        $fecha1 = $fecha_concentrado[0];
        $fecha2 = $fecha_concentrado[1];
        $cveusuario_concentrado = (!empty($datosFiltrados['cveusuario_concentrado']) || $datosFiltrados['cveusuario_concentrado']!=null) ? $datosFiltrados['cveusuario_concentrado'] : '0';

        $filtro = '';

        if($cveusuario_concentrado !=0){
            $filtro = 'and mv.cveusuarioadd_venta = '.$cveusuario_concentrado;
        }
        $query = "select
                    concat(cu.nombre_usuario, ' ', cu.apellidop_usuario, ' ', cu.apellidom_usuario) as nombrecompleto_venta,
                    sum(mv.total_venta) as total_venta,
                    sum(mv.total_venta - mv.precioespecial_venta) as totalcaja_venta,
                    sum(mv.precioespecial_venta) as precioespecial_venta,
                    COUNT(IF(mv.tipo_venta  = 1, 1, NULL)) as  cantidad_venta_mostrador,
                    COUNT(IF(mv.tipo_venta  = 2, 1, NULL)) as  cantidad_venta_domicilio, 
                    COUNT(IF(mv.tipo_venta  = 1  and mv.tipoprecio_venta = 1 , 1, NULL)) as  cantidad_mostrador_menudeo,
                    sum(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  total_mostrador_menudeo,
                    COUNT(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  cantidad_mostrador_mayoreo,
                    sum(IF(mv.tipo_venta  = 1 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  total_mostrador_mayoreo,
                    COUNT(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  cantidad_domicilio_menudeo,
                    sum(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 1, mv.total_venta, NULL)) as  total_domicilio_menudeo,
                    COUNT(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  cantidad_domicilio_mayoreo,
                    sum(IF(mv.tipo_venta  = 2 and mv.tipoprecio_venta = 2, mv.total_venta, NULL)) as  total_domicilio_mayoreo,
                    cs.nombre_sucursal,
                    cp.nombre_perfil 
                from
                    ma_ventas mv
                inner join ca_usuario cu on
                    mv.cveusuarioadd_venta = cu.cve_usuario
                inner join ca_sucursales cs on
                    cu.cvesucursal_usuario = cs.cve_sucursal
                inner join ca_perfil cp on
                    cu.cveperfil_usuario = cp.cve_perfil
                where
                    mv.cvesucursal_venta = $sucursal_concentrado
                    and mv.estatus_venta = 4 $filtro and date_format(mv.fechamod_venta , '%Y-%m-%d') between date_format('$fecha1' , '%Y-%m-%d') and date_format('$fecha2' , '%Y-%m-%d')
                group by
                    mv.cveusuarioadd_venta
                order by cs.nombre_sucursal desc, cu.nombre_usuario asc";
        //echo $query;

        $c_concentrado = $this->conexion->query($query);
        $r_concentrado = $this->conexion->consulta_array($c_concentrado);

        return $r_concentrado;
    }

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>