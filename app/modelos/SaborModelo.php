<?php

class SaborModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_sabor = (!empty($datosFiltrados['cve_sabor']) || $datosFiltrados['cve_sabor']!=null) ? $datosFiltrados['cve_sabor'] : '0';
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL obtenSabores('$ban','$cve_sabor','$cvesucursal_usuario')";
        //echo $query;

        $c_sabor = $this->conexion->query($query);
        $r_sabor = $this->conexion->consulta_array($c_sabor);

        return $r_sabor;
    }



    public function guardarSabor($datosSabor)
    {

        $datosFiltrados = $this->filtrarDatos($datosSabor);

        $ban                    = $datosFiltrados['ban'];
        $nombre_sabor         = $datosFiltrados['nombre_sabor'];
        $cveproducto_sabor      = $datosFiltrados['cveproducto_sabor'];
        $clave_sabor        = $datosFiltrados['clave_sabor'];
        $cve_sabor            = $datosFiltrados['cve_sabor'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        
        $query = "CALL guardarSabor(
                                    '$ban',
                                    '$cve_sabor',
                                    '$nombre_sabor',
                                    '$cveproducto_sabor',
                                    '$clave_sabor',
                                    '$cveusuario_accion'
                                    )";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }



    public function bloquearSabor($datosSabor)
    {
        $datosFiltrados = $this->filtrarDatos($datosSabor);

        $ban               = $datosFiltrados['ban'];
        $cve_sabor        = $datosFiltrados['cve_sabor'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarSabor('$ban','$cve_sabor','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>