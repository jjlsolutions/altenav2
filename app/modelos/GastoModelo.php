<?php

class GastoModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_degasto = (!empty($datosFiltrados['cve_degasto']) || $datosFiltrados['cve_degasto']!=null) ? $datosFiltrados['cve_degasto'] : '0';
        $fecha_venta = (!empty($datosFiltrados['fecha_venta']) || $datosFiltrados['fecha_venta']!=null) ? $datosFiltrados['fecha_venta'] : '0';
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];

        $fecha_venta = explode(" - ",$fecha_venta);
        $fecha1 = $fecha_venta[0];
        $fecha2 = $fecha_venta[1];

        $query = "CALL obtenGasto('$ban','$cve_degasto','$cvesucursal_usuario','$fecha1','$fecha2')";
        //echo $query;

        $c_gasto = $this->conexion->query($query);
        $r_gasto = $this->conexion->consulta_array($c_gasto);

        return $r_gasto;
    }

    public function consultarCaGasto($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_gasto = (!empty($datosFiltrados['cve_gasto']) || $datosFiltrados['cve_gasto']!=null) ? $datosFiltrados['cve_gasto'] : '0';
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL obtenCaGasto('$ban','$cve_gasto','$cvesucursal_usuario')";
        

        $c_gasto = $this->conexion->query($query);
        $r_gasto = $this->conexion->consulta_array($c_gasto);

        return $r_gasto;
    }

    public function guardarGasto($datosGasto)
    {

        $datosFiltrados = $this->filtrarDatos($datosGasto);

        $ban                    = $datosFiltrados['ban'];
        $cve_degasto         = $datosFiltrados['cve_degasto'];
        $descripcion_degasto      = $datosFiltrados['descripcion_degasto'];
        $imagen_degasto        = $datosFiltrados['imagen_degasto'];
        $total_degasto            = $datosFiltrados['total_degasto'];
        $cvecagasto_degasto            = $datosFiltrados['cvecagasto_degasto'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        
        $query = "CALL guardarGasto(
                                    '$ban',
                                    '$cve_degasto',
                                    '$descripcion_degasto',
                                    '$imagen_degasto',
                                    '$total_degasto',
                                    '$cvecagasto_degasto',
                                    '$cvesucursal_usuario',
                                    '$cveusuario_accion'
                                    )";
            
        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }

    public function guardarCagasto($datosSabor)
    {

        $datosFiltrados = $this->filtrarDatos($datosSabor);

        $ban                    = $datosFiltrados['ban'];
        $cve_cagasto         = $datosFiltrados['cve_cagasto'];
        $nombre_cagasto         = $datosFiltrados['nombre_cagasto'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];

        $query = "CALL guardarCaGasto(
                                    '$ban',
                                    '$cve_cagasto',
                                    '$nombre_cagasto',
                                    '$cvesucursal_usuario',
                                    '$cveusuario_accion'
                                    )";
                                    
        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }

    public function eliminarGasto($datosGasto)
    {
        $datosFiltrados = $this->filtrarDatos($datosGasto);

        $cve_degasto        = $datosFiltrados['cve_degasto'];
        $query = "CALL guardarGasto(
                                '1',
                                '$cve_degasto',
                                '',
                                '',
                                '',
                                '',
                                '',
                                ''
                                )";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>