<?php
date_default_timezone_set("America/Mazatlan");
class DomicilioModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_venta = (!empty($datosFiltrados['cve_venta']) || $datosFiltrados['cve_venta']!=null) ? $datosFiltrados['cve_venta'] : '0';

        $query = "CALL obtenVentaes('$ban','$cve_venta')";
        //echo $query;

        $c_venta = $this->conexion->query($query);
        $r_venta = $this->conexion->consulta_array($c_venta);

        return $r_venta;
    }



    public function guardarVenta($datosVenta)
    {

        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban                    = $datosFiltrados['ban'];
        $nombre_venta         = $datosFiltrados['nombre_venta'];
        $cveproducto_venta      = $datosFiltrados['cveproducto_venta'];
        $clave_venta        = $datosFiltrados['clave_venta'];
        $cve_venta            = $datosFiltrados['cve_venta'];
        $fecha              = date("Y-m-d H:i:s");
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];

        $query = "CALL guardarVenta(
                                    '$ban',
                                    '$cve_venta',
                                    '$nombre_venta',
                                    '$cveproducto_venta',
                                    '$clave_venta',
                                    '$fecha',
                                    '$cveusuario_accion'
                                    )";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }



    public function bloquearVenta($datosVenta)
    {
        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarVenta('$ban','$cve_venta','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>