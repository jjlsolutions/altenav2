<?php
date_default_timezone_set("America/Mazatlan");
class IncidenteModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_incidente = (!empty($datosFiltrados['cve_incidente']) || $datosFiltrados['cve_incidente']!=null) ? $datosFiltrados['cve_incidente'] : '0';
        $estatus_incidente = (!empty($datosFiltrados['estatus_incidente']) || $datosFiltrados['estatus_incidente']!=null) ? $datosFiltrados['estatus_incidente'] : '';

        if($ban == 1){
            if($_SESSION["cvesucursal_usuario"] == 0){
                $query = "SELECT * from ma_incidentes mi 
            inner join ca_usuario cu on cu.cve_usuario = mi.cvecliente_incidente 
            inner join ca_sucursales cs on cs.cve_sucursal = cu.cvesucursal_usuario 
            order by cve_incidente desc;";
            }else{
                $query = "SELECT * from ma_incidentes mi 
            inner join ca_usuario cu on cu.cve_usuario = mi.cvecliente_incidente 
            inner join ca_sucursales cs on cs.cve_sucursal = cu.cvesucursal_usuario 
            where cs.cve_sucursal = ".$_SESSION["cvesucursal_usuario"]." order by cve_incidente desc;";
            }
            
    
            $c_venta = $this->conexion->query($query);
            $r_venta = $this->conexion->consulta_array($c_venta);
        }
        else if($ban == 2){
            $query = "UPDATE ma_incidentes 
            SET 
                estatus_incidente = ".$estatus_incidente."
            WHERE cve_incidente = ".$cve_incidente.";";
            $c_venta = $this->conexion->query($query);
            $r_venta = $this->conexion->consulta_array($c_venta);

            $this->conexion->next_result();     
        $$result = false;

            
        $query = "Select * from de_incidente where cveincidente_deincidente = ".$cve_incidente.";";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        $this->conexion->next_result();

        $query2 = "select sum(di.cantidad_deincidente) as total_cantidad, sum(cp.stock) as total_stock from de_incidente di
                    inner join ca_productos cp on cp.cve_producto = di.cvesp_deincidente
                    where cveincidente_deincidente = ".$cve_incidente.";";

        $respuestatotal = $this->conexion->query($query2) or die ($this->conexion->error());
        $total_productos = $respuestatotal->fetch_object();

        if($total_productos->total_cantidad <= $total_productos->total_stock){
            while ($valor = $respuesta->fetch_object()) {
                $cveusuario_accion = $_SESSION["cve_usuario"];

                $queryS =  'select (select stock  from ca_productos where cve_producto = '.$valor->cvesp_deincidente.') - '.$valor->cantidad_deincidente.'  as stock;';
                $respuestaStock = $this->conexion->query($queryS) or die ($this->conexion->error());
                $this->conexion->next_result();

                $stock_productos = $respuestaStock->fetch_object();
            $query = "UPDATE ca_productos 
                        SET 
                        stock = $stock_productos->stock  
                    WHERE cve_producto = $valor->cvesp_deincidente;";
            $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
    
                $respuestas = $this->conexion->query($query);
                $this->conexion->next_result();

            $query = "INSERT INTO historial_stock  
                    (
                    cveproducto_historialstock,
                    cantidad_historialstock,
                    tipo_historialstock,
                    cveusuariomod_historialstock
                    ) VALUES (
                                        $valor->cvesp_deincidente,
                                        -$valor->cantidad_deincidente,
                                        'INCIDENTE',
                                        ".$_SESSION["cve_usuario"]."
                                     );";
            $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
    
                $respuestas = $this->conexion->query($query);
                $this->conexion->next_result();
            }

            $result = true;
        }

        }
        else if($ban == 3){
            $query = "DELETE from ma_incidentes WHERE cve_incidente = ".$cve_incidente.";";

            $c_venta = $this->conexion->query($query);
            $r_venta = $this->conexion->consulta_array($c_venta);

            $query = "DELETE from de_incidentes WHERE cve_deincidente = ".$cve_incidente.";";

            $c_venta = $this->conexion->query($query);
            $r_venta = $this->conexion->consulta_array($c_venta);
        }else if($ban == 4){
            if($_SESSION["cvesucursal_usuario"] == 0){
                $query = "SELECT * from ma_incidentes mi 
            inner join de_incidente di on di.cveincidente_deincidente = mi.cve_incidente 
            inner join ca_productos cp on cp.cve_producto = di.cvesp_deincidente 
            where mi.cve_incidente = ".$cve_incidente." order by cve_incidente desc;";
            }else{
                $query = "SELECT * from ma_incidentes mi 
            inner join de_incidente di on di.cveincidente_deincidente = mi.cve_incidente 
            inner join ca_productos cp on cp.cve_producto = di.cvesp_deincidente 
            where mi.cvesucursal_incidente = ".$_SESSION["cvesucursal_usuario"]." and mi.cve_incidente = ".$cve_incidente." order by cve_incidente desc;";
            }
            
        
            $c_venta = $this->conexion->query($query);
            $r_venta = $this->conexion->consulta_array($c_venta);
        }

        return $r_venta;
    }



    public function guardarIncidente($datosVenta)
    {

        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban                = $datosFiltrados['ban'];
        $cvecliente_venta   = $datosFiltrados['cvecliente_venta'];
        $cveusuario_accion  = $datosFiltrados['cveusuario_accion'];
        $listaProductos     = $datosVenta['listaProductos'];
        $observaciones     = $datosVenta['observaciones'];
        $imagen   = $datosFiltrados['imagen'];
        $fecha              = date("Y-m-d H:i:s");
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL guardarIncidente(
                                    '$ban',
                                    '$cvecliente_venta',
                                    '$cvesucursal_usuario',
                                    '$observaciones',
                                    '$imagen',
                                    '$fecha',
                                    '$cveusuario_accion'
                                    )";
                                    

        $c_perfil = $this->conexion->query($query) or die ($this->conexion->error());
        $r_perfil = $this->conexion->consulta_assoc($c_perfil);

        $ultima_cve = $r_perfil['cve_incidente'];
        
        $this->conexion->next_result();
        foreach($listaProductos as $valor){
                
                $query = "CALL guardarDetalleIncidente(
                    '1',
                    '$ultima_cve',
                    '$valor->cvesp_venta',
                    '$valor->cantidad_venta'
                    )";
                $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
                $this->conexion->next_result();
            
        }

        $this->conexion->close_conexion();
        return $r_perfil;
    }



    public function bloquearVenta($datosVenta)
    {
        $datosFiltrados = $this->filtrarDatos($datosVenta);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL eliminarVenta('$ban','$cve_venta','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>