<?php

class ListaventaModelo
{

	//creamos la variable donde se instanciará la clase "conectar"
    public $conexion;

    public function __construct() {

    	//inicializamos la clase para conectarnos a la bd
        $this->conexion = new ConexionBD(); //instanciamos la clase

    }



    public function consultar($datos)
    {
        $datosFiltrados = $this->filtrarDatos($datos);

        $ban  = $datosFiltrados['ban'];
        $cve_venta = (!empty($datosFiltrados['cve_venta']) || $datosFiltrados['cve_venta']!=null) ? $datosFiltrados['cve_venta'] : '0';
        $tipo_venta = (!empty($datosFiltrados['tipo_venta']) || $datosFiltrados['tipo_venta']!=null) ? $datosFiltrados['tipo_venta'] : '0';
        $estatus_venta = (!empty($datosFiltrados['estatus_venta']) || $datosFiltrados['estatus_venta']!=null) ? $datosFiltrados['estatus_venta'] : '0';
        $fecha_venta = (!empty($datosFiltrados['fecha_venta']) || $datosFiltrados['fecha_venta']!=null) ? $datosFiltrados['fecha_venta'] : '0';
        $cvesucursal_usuario = ($_SESSION["cvesucursal_usuario"] == 0) ? $datosFiltrados['cve_sucursal'] : $_SESSION["cvesucursal_usuario"];
 
        $fecha_venta = explode(" - ",$fecha_venta);
        $fecha1 = $fecha_venta[0];
        $fecha2 = $fecha_venta[1];
        $query = "CALL obtenListaventa('$ban','$cve_venta','$tipo_venta','$estatus_venta','$fecha1','$fecha2','$cvesucursal_usuario')";
        //echo $query;
        $c_venta = $this->conexion->query($query);
        $r_venta = $this->conexion->consulta_array($c_venta);

        return $r_venta;
    }

    public function consultar2($ban,$cve_venta,$tipo_venta)
    {
       
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
 
        $query = "CALL obtenListaventa('$ban','$cve_venta','$tipo_venta','0','0','0','$cvesucursal_usuario')";
      
        $c_venta = $this->conexion->query($query);
        $r_venta = $this->conexion->consulta_array($c_venta);

        return $r_venta;
    }

    public function guardarListaventa($datosListaventa)
    {

        $datosFiltrados = $this->filtrarDatos($datosListaventa);

        $ban                    = $datosFiltrados['ban'];
        $nombre_venta         = $datosFiltrados['nombre_venta'];
        $preciomenudeo_venta          = $datosFiltrados['preciomenudeo_venta'];
        $preciomayoreo_venta          = $datosFiltrados['preciomayoreo_venta'];
        $cveListaventa            = $datosFiltrados['cve_venta'];
        $cveusuario_accion      = $datosFiltrados['cveusuario_accion'];
        $cvesucursal_usuario = $_SESSION["cvesucursal_usuario"];
        $query = "CALL guardarListaventa(
                                    '$ban',
                                    '$cveListaventa',
                                    '$nombre_venta',
                                    '$preciomayoreo_venta',
                                    '$preciomenudeo_venta',
                                    '$cvesucursal_usuario',
                                    '$cveusuario_accion'
                                    )";

        $respuesta = $this->conexion->query($query) or die ($this->conexion->error());
        
        $this->conexion->close_conexion();
        
        return $respuesta;

    }



    public function bloquearListaventa($datosListaventa)
    {
        $datosFiltrados = $this->filtrarDatos($datosListaventa);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $tipo_venta        = $datosFiltrados['tipo_venta'];
        $encamino        = $datosFiltrados['encamino'];
        
        $motivo        = $datosFiltrados['motivo'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];
        if($ban == 1 || $ban == 2){
            $query = "CALL eliminarListaventa('$ban','$cve_venta','$motivo','$cveusuario_accion')";

            $respuesta = $this->conexion->query($query);
        }
        else{
            file_put_contents('pruebasQueryguardarProducto.txt',print_r( array($encamino),true)."\r\n", FILE_APPEND | LOCK_EX);
            if($encamino == 'no' && $tipo_venta == 2){
                file_put_contents('pruebasQueryguardarProducto.txt',print_r( array('no va n camino y no se descuenta nada, solo se cancela'),true)."\r\n", FILE_APPEND | LOCK_EX);
            }else{
                $query1 = "CALL obtenListaventa('2','$cve_venta',$tipo_venta,'0','0','','1')";
                file_put_contents('pruebasQueryguardarProducto.txt',print_r( array($query1),true)."\r\n", FILE_APPEND | LOCK_EX);
                $respuesta2 = $this->conexion->query($query1);
                $this->conexion->next_result();
                while ($valor = $respuesta2->fetch_object()) {
                    $v_positivo = $valor->cantidad_venta;
                    $cveusuario_accion = $_SESSION["cve_usuario"];
                    $tipocaso = '';
                    if($tipo_venta == 1){
                        $tipocaso = 2;
                    }
                    if($tipo_venta == 2){
                        $tipocaso = 3;
                    }
                    $query = "CALL guardarProducto(
                        '$tipocaso',
                        '$valor->cvesp_venta',
                        '',
                        '$v_positivo',
                        '',
                        '0',
                        '$cveusuario_accion',
                        'DOMICILIO CANCELADO'
                        )";
                        file_put_contents('pruebasQueryguardarProducto.txt',print_r( array($query),true)."\r\n", FILE_APPEND | LOCK_EX);

                    $respuestas = $this->conexion->query($query);
                    $this->conexion->next_result();
                }
            }
            

            $query = "CALL eliminarListaventa('$ban','$cve_venta','$motivo','$cveusuario_accion')";

            $respuesta = $this->conexion->query($query);
        }
        

        return $respuesta;
    }

    public function completarVenta($datosListaventa)
    {
        $datosFiltrados = $this->filtrarDatos($datosListaventa);

        $ban               = $datosFiltrados['ban'];
        $cve_venta        = $datosFiltrados['cve_venta'];
        $pagocon_venta        = $datosFiltrados['pagocon_venta'];
        $cambio_venta        = $datosFiltrados['cambio_venta'];
        $cveusuario_accion = $datosFiltrados['cveusuario_accion'];

        $query = "CALL completarVenta('$ban','$cve_venta','$pagocon_venta','$cambio_venta','$cveusuario_accion')";

        $respuesta = $this->conexion->query($query);

        return $respuesta;
    }

    public function filtrarDatos($datosFiltrar){

        foreach ($datosFiltrar as $indice => $valor) {
            $datosFiltrarr[$indice] = $this->conexion->real_escape_string($valor);
        }

        return $datosFiltrarr;

    }
	
}

?>