<?php  
	function renameFile($nombre) //Regresa normbre de archivo
	{
		$GLOBALS['normalizeChars'] = array(
		'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 
		'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 
		'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 
		'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 
		'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 
		'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 
		'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f'
		);
	
		$texto = strtr($nombre, $GLOBALS['normalizeChars']);
	
		$safe_filename = preg_replace( 
						 array("/\s+/", "/[^-\.\w]+/"), 
						 array("_", ""), 
						 trim($texto)); 
	
		return $safe_filename;
	}
	
	function cargarArchivo($ruta,$file)
	{
		$response = new StdClass();
		
		if (!is_dir($ruta))
			mkdir($ruta);
		
		if ($dh = opendir($ruta))
		{
			$fileupload =  $file['name'];
			$filedestino = $ruta."/".$fileupload;
			
			$extension = explode(".", $filedestino);

			$countArray = count($extension);
			
			if (!file_exists($filedestino))
			{
				copy($file['tmp_name'], $filedestino);
				
				$response->success = true;
				$response->archivo = $fileupload;
			}
			else
			{
				$response->success = false;
				$response->error = 3;
				//'El archivo ya existe, favor de verificar.'
			}
		}
		else {
			$response->success = false;
			$response->error = 1;
			//'La ruta del archivo no es valida, favor de mostrar este mensaje al administrador del sistema.'
		}
        return $response;
	}
	
	function cargarArchivoFIEL($ruta,$file)
	{
		$response = new StdClass();
		
		if (!is_dir($ruta))
			mkdir($ruta);
		
		if ($dh = opendir($ruta))
		{
			$fileupload =  renameFile($file['name']);
			$filedestino = $ruta."/".$fileupload;
			
			$extension = explode(".", $filedestino);
			$countArray = count($extension);
			
			if($countArray > 0 && (strtolower($extension[$countArray-1]) == 'cer' || strtolower($extension[$countArray-1]) == 'key' ))
			{
				if (!file_exists($filedestino))
				{
					copy($file['tmp_name'], $filedestino);
					
					$response->success = true;
					$response->archivo = $fileupload;
				}
				else
				{
					$response->success = false;
					$response->error = 3;
					//'El archivo ya existe, favor de verificar.'
				}
			}
			else
			{
				$response->success = false;
				$response->error = 2;
				//'El tipo de archivo no es valido, favor de verificar.'
			}
		}
		else {
			$response->success = false;
			$response->error = 1;
			//'La ruta del archivo no es valida, favor de mostrar este mensaje al administrador del sistema.'
		}
        return $response;
	}
	
	function eliminarDirSubDirArchivo($ruta,$tipo,$archivo)
	{
		/*file_put_contents('eliminarArchivo.txt',$ruta."\r\n", FILE_APPEND | LOCK_EX);
		file_put_contents('eliminarArchivo.txt',$tipo."\r\n", FILE_APPEND | LOCK_EX);
		file_put_contents('eliminarArchivo.txt',$archivo."\r\n", FILE_APPEND | LOCK_EX);*/
		if ($dh = opendir($ruta))
		{
			if($tipo==1) //eliminar directorio, subdirectorios y archivos de un directorio
			{
				foreach( glob($ruta."/*") as $archivos_carpeta)
				{
					if (is_dir($archivos_carpeta))
					{
						foreach( glob($archivos_carpeta."/*") as $archivos_carpeta_2)
						{
							if (is_dir($archivos_carpeta_2))
							{
								foreach( glob($archivos_carpeta_2."/*") as $archivos_carpeta_3)
								{
									unlink($archivos_carpeta_3);			// Eliminamos todos los archivos de la carpeta hasta dejarla vacia  
								}
							}
							else
								unlink($archivos_carpeta_2);			// Eliminamos todos los archivos de la carpeta hasta dejarla vacia  
						}
						rmdir($archivos_carpeta);					// Eliminamos la carpeta vacia
					}
					else
						unlink($archivos_carpeta);			// Eliminamos todos los archivos de la carpeta hasta dejarla vacia  
				}
				rmdir($ruta);					// Eliminamos la carpeta vacia
			}
			else if($tipo==2)//eliminar subdirectorios y archivos de un subdirectorio
			{
				foreach(glob($ruta."/*.*") as $archivos_carpeta)
				{
					unlink($archivos_carpeta);     // Eliminamos todos los archivos de la carpeta hasta dejarla vacia  
				}
				
				rmdir($ruta);         // Eliminamos la carpeta vacia	
			}
			else if($tipo==3)//Eliminar archivo
			{
				$ruta = $ruta."/".$archivo;
				unlink($ruta);
			}
			return 1;
		}
		else {
			//return "{success: false, 'msg': 'La ruta del archivo no es valida, favor de mostrar este mensaje al administrador del sistema.'}";
			return -1;
		}
	}
?>